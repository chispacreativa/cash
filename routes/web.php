<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Route::get('/', 'IndexController@index');

Route::get('/inicio', 'IndexController@index');


Route::get('/home', 'IndexController@index');



Route::get('/test', 'TestController@index');



Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']); //Vista
Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']); //Vista

Route::middleware(['verified'])
    ->group(function () {
        Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
        Route::get('/dashboard/perfil', ['as' => 'dashboard.perfil', 'uses' => 'DashboardController@perfil']);
        Route::match(['GET', 'POST'], '/dashboard/edit-profile', ['as' => 'dashboard.edit-profile', 'uses' => 'DashboardController@editprofile']);
        Route::get('/dashboard/activity', ['as' => 'dashboard.activity', 'uses' => 'DashboardController@activity']);
        Route::match(['GET', 'POST'],'/dashboard/cuentas', ['as' => 'dashboard.cuentas', 'uses' => 'DashboardController@cuentas']);
        Route::match(['GET', 'POST'],'/dashboard/cuentas/{id}', ['as' => 'dashboard.cuentas.edit', 'uses' => 'DashboardController@cuentasEdit']);
        
        //Route::get('/submit', ['as' => 'submit', 'uses' => 'DashboardController@submit']);
        
        Route::get('/intercambio', ['as' => 'intercambio', 'uses' => 'IntercambioController@index']);
        Route::post('/intercambio', ['as' => 'intercambio', 'uses' => 'IntercambioController@store']);
        
        Route::get('/intercambio/view/{id}', ['as' => 'intercambio.view', 'uses' => 'IntercambioController@view']);
        Route::post('/intercambio/view/{id}', ['as' => 'intercambio.view.update', 'uses' => 'IntercambioController@update']);
        Route::post('/intercambio/view/{id}/images', ['as' => 'intercambio.images', 'uses' => 'IntercambioController@images']);
        
        Route::get('/intercambio/bankaccounts', 'IntercambioController@getBankAccounts');
        Route::post('/intercambio/bankaccounts', 'IntercambioController@submitBankAccounts')->name("add_accounts");
        
        Route::post('/intercambio/tipomoneda', 'IntercambioController@getTipoMoneda');
        Route::post('/intercambio/bank', 'IntercambioController@getBank');
    });


Route::get('/contacto', 'ContactoController@contacto')->name('contacto');
Route::get('/nosotros', 'ContactoController@nosotros')->name('nosotros');
Route::get('/preguntas-frecuentes', 'ContactoController@preguntas')->name('faq');
Route::get('/terminos-y-condiciones', 'ContactoController@terminos')->name('terminos');
Route::get('/privacidad', 'ContactoController@privacidad')->name('privacidad');


/*use Illuminate\Mail\Markdown;
Route::get('/mail-test', function () {
    $markdown = new Markdown(view(), config('mail.markdown'));

    //return $markdown->render('vendor.mail_test');
    return $markdown->render('emails.intercambio.nuevo');
});*/

Route::get('/mail-intercambio', '\App\Mail\NuevoIntercambio@build');

/* Registro completo de usuarios */
// Route::get('/register-complete', 'Auth\RegisterController@register_complete_data');
Route::post('register-complete', ['as' => 'register-complete', 'uses' => 'Auth\RegisterController@register_complete_data']); //Vista