class IntercambioModal {
    constructor() {

        this.btn_add_account = null;
        this.btn_mdl_add = null;
        this.modal_add_account = null;
        this.__init();
    }

    __init() {
        var self = this;
        self.intercambio_container = $('#intercambio-layout');

        if (self.btn_add_account == null)
            self.btn_add_account = self.intercambio_container.find('.btn-add-account-send');
           
            
        if (self.modal_add_account == null)
            self.modal_add_account = self.intercambio_container.find('#modal_add_account_send');
            

        if (self.btn_mdl_add == null)
            self.btn_mdl_add = self.modal_add_account.find('#btn-mdl-add-send');

        self.btn_mdl_add.on('click', function(event) {
           
            self.__addBankAccount();
        });
        
        self.btn_add_account.on('click', function(e) {
            var fruitCount = $(this).attr('data-type');
            self.modal_add_account.find('#type-account').val(fruitCount);
            self.modal_add_account.modal();
        });
    }

    __addBankAccount() {
        var self = this;
        var __entidad_bancaria = self.modal_add_account.find('#entidad-bancaria');
        var __tipo_cuenta = self.modal_add_account.find('#tipo-cuenta');
        var __tipo_moneda = self.modal_add_account.find('#tipo-moneda');
        var __numero_cuenta = self.modal_add_account.find('#numero-cuenta');
        var __nombre_cuenta = self.modal_add_account.find('#nombre-cuenta');
        var __apellido_cuenta = self.modal_add_account.find('#apellido-cuenta');
        var __cedula_cuenta = self.modal_add_account.find('#cedula-cuenta');
        var __email_cuenta = self.modal_add_account.find('#email-cuenta');
        var __casa_cuenta = self.modal_add_account.find('#casa-cuenta');
        var __pais_cuenta = self.modal_add_account.find('#pais');
        var __type_account_tran = $("#type-account").val();
         //   console.log(__type_account_tran);
        var __correcto = true;
        if (__entidad_bancaria.val() == '') {
            __correcto = false;
        }
        if (__tipo_cuenta.val() == '') {
            __correcto = false;
        }
        if (__tipo_moneda.val() == '') {
            __correcto = false;
        }
        if (__numero_cuenta.val() == '') {
            __correcto = false;
        }
        if (__nombre_cuenta.val() == '') {
            __correcto = false;
        }

        if (__apellido_cuenta.val() == '') {
            __correcto = false;
        }

        if (__cedula_cuenta.val() == '') {
            __correcto = false;
        }

        if (__email_cuenta.val() == '') {
            __correcto = false;
        }

        if (__casa_cuenta.val() == '') {
            __correcto = false;
        }

        
        if (__pais_cuenta.val() == '') {
            __correcto = false;
        }


        if (__correcto) {
            axios.post(
                '/intercambio/bankaccounts',
                {
                    'entidad_bancaria' : __entidad_bancaria.val(),
                    'tipo_cuenta' : __tipo_cuenta.val(),
                    'tipo_moneda' : __tipo_moneda.val(),
                    'numero_cuenta': __numero_cuenta.val(),
                    'nombre_cuenta' : __nombre_cuenta.val(),
                    'apellido_cuenta' : __apellido_cuenta.val(),
                    'cedula' : __cedula_cuenta.val(),
                    'email' : __email_cuenta.val(),
                    'casa' : __casa_cuenta.val(),
                    'pais_cuenta': __pais_cuenta.val(),
                    'type_account_tran': __type_account_tran
                }
            ).then(function (response) {
                if (response.status == 200) {
                    if (response.data.error) {
                        
                    } else {
                        self.modal_add_account.modal('hide');
                        self.__reloadBankAccounts(__type_account_tran);
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }
}

$(function() {
    if ($('#intercambio-layout').length > 0) {
        var $__intercambio = new Intercambio();
    }
});
