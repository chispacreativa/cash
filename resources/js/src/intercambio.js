class Intercambio {
    constructor() {

        this.btn_add_account = null;
        this.btn_mdl_add = null;
        this.modal_add_account = null;
        this.__init();
    }

    __init() {
        var self = this;
        self.intercambio_container = $('#intercambio-layout');

        if (self.btn_add_account == null)
            self.btn_add_account = self.intercambio_container.find('.btn-add-account');

        if (self.modal_add_account == null)
            self.modal_add_account = self.intercambio_container.find('#modal_add_account');


        if (self.btn_mdl_add == null)
            self.btn_mdl_add = self.modal_add_account.find('#btn-mdl-add');

        self.btn_mdl_add.on('click', function(event) {
            self.__addBankAccount();
        });
        
        self.btn_add_account.on('click', function() {
            self.modal_add_account.modal();
        });
    }

    __addBankAccount() {
        var self = this;
        var __entidad_bancaria = self.modal_add_account.find('#entidad-bancaria');
        var __tipo_cuenta = self.modal_add_account.find('#tipo-cuenta');
        var __tipo_moneda = self.modal_add_account.find('#tipo-moneda');
        var __numero_cuenta = self.modal_add_account.find('#numero-cuenta');
        var __nombre_cuenta = self.modal_add_account.find('#nombre-cuenta');
        var __correcto = true;
        if (__entidad_bancaria.val() == '') {
            __correcto = false;
        }
        if (__tipo_cuenta.val() == '') {
            __correcto = false;
        }
        if (__tipo_moneda.val() == '') {
            __correcto = false;
        }
        if (__numero_cuenta.val() == '') {
            __correcto = false;
        }
        if (__nombre_cuenta.val() == '') {
            __correcto = false;
        }

        if (__correcto) {
            axios.post(
                '/intercambio/bankaccounts',
                {
                    'entidad_bancaria' : __entidad_bancaria.val(),
                    'tipo_cuenta' : __tipo_cuenta.val(),
                    'tipo_moneda' : __tipo_moneda.val(),
                    'numero_cuenta': __numero_cuenta.val(),
                    'nombre_cuenta' : __nombre_cuenta.val()
                }
            ).then(function (response) {
                if (response.status == 200) {
                    if (response.data.error) {
                        
                    } else {
                        self.modal_add_account.modal('hide');
                        self.__reloadBankAccounts();
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    __reloadBankAccounts() {
        var __select_list = $('#bank-accounts');
        __select_list.empty();
        __select_list.append('<option value="" selected>Selecciona una cuenta...</option>');
        axios.get('/intercambio/bankaccounts')
            .then((response) => {
                if (response.status == 200 && response.data.count > 0) {
                    var $count = 0;
                    var __data =  response.data.data;
                    __data.forEach(element => {
                        var __selected = '';
                        if ($count >= (response.data.count - 1)) __selected = 'selected';
                        __select_list.append(`<option value="${element.id}" ${__selected}>${element.name}</option>`);
                        $count++;
                    });
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }
}

$(function() {
    if ($('#intercambio-layout').length > 0) {
        var $__intercambio = new Intercambio();
    }
});