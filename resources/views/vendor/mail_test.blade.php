
<div style="padding:10px 0px;background-color:#eeeffe">
    <div style="width:100%;background-color:#eeeffe">
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;font-family:sans-serif">
            Ya enviamos los US$&nbsp;5,000.00 a tu cuenta.
        </div>
        <div
            style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;font-family:sans-serif">
            &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;<wbr>&nbsp;&zwnj;&nbsp;&zwnj;&nbsp; </div>
            <table align="center" cellspacing="0" cellpadding="0" border="0" width="600"
                style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
                <tbody>
                    <tr>
                        <td colspan="2" align="center">
                            <img width="120" height="35" alt="logo_ftb" border="0"
                                style="height:auto;background:#ffffff;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555"
                                src="{{asset('mail/img/logo.png')}}"
                                class="CToWUd">
                        </td>
                    </tr>
                    <tr style="background-color:#397afa">
                        <td colspan="2"
                            style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                            <h3
                                style="font-family: 'Rubik', sans-serif;font-size:20px;font-weight:500;color:#ffffff;line-height:1;margin:0px">
                                ¡Hemos enviado los fondos de tu operación #20190709C0003!</h3>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table align="center" cellspacing="0" cellpadding="0" border="0" width="600"
                style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
                <tbody>
                <!--tr style="background-color:#ffffff"-->
                <tr background="{{asset('mail/img/bg.svg')}}" style="background-repeat: no-repeat" >
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top" width="260" style="vertical-align:middle">
                            &nbsp; &nbsp;
                            <table
                                style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:0px!important">
                                <tbody>
                                    <tr>
                                        <td style="font-family: 'Rubik', sans-serif" >Ya enviamos los 
                                            <span style="color:#673ab7;font-family: 'Rubik', sans-serif;">US$ 5,000.00</span> a tu cuenta.
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td style="font-family: 'Rubik', sans-serif" >Además, adjuntamos el comprobante de la operación para su registro en
                                            contabilidad, el mismo que también puedes descargar en nuestra plataforma.
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td>¡Gracias por la operación! </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top" width="300"
                            style="vertical-align:middle;text-align:center;padding-top:40px;padding-bottom:40px">
                            <img width="300" height="233" alt="img_fondos_enviados" border="0"
                                style="height:auto;background:#ede7f6;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555"
                                src="{{asset('mail/img/business.svg')}}"
                                class="CToWUd a6T" tabindex="0">
                            <div class="a6S" dir="ltr" style="opacity: 0.01; left: 752px; top: 427px;">
                                <div id=":ob" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Descargar" role="button"
                                    tabindex="0" aria-label="Descargar el archivo adjunto " data-tooltip-class="a1V">
                                    <div class="aSK J-J5-Ji aYr"></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table align="center" cellspacing="0" cellpadding="0" border="0" width="600"
                style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#FFF">
                <tbody>
                    <tr>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td width="60"><img width="50px" alt="icono_referir"
                                style="margin:5px 0px;border-radius:50%;background-color:#ffffff"
                                src="{{asset('mail/img/verified-user.svg')}}"
                                class="CToWUd">
                        </td>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <table
                                style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:0px!important">
                                <tbody>
                                    <tr>
                                        <td style="color:#77838f;font-size:15px;padding-top:20px;font-family: 'Rubik', sans-serif;">
                                            ¡COMPARTE EL MEJOR TIPO DE CAMBIO CON TUS AMIGOS! CADA VEZ SOMOS MÁS
                                            <span style="font-style:italic">#ForgetTheBank</span></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:14px"><a
                                                href="https://api.whatsapp.com/send?text=Yo%20ya%20realizo%20mis%20operaciones%20de%20cambio%20de%20dólares%20con%20ftb.pe.%20¡Te%20invito%20a%20conocerlos!%20https%3A%2F%2Fhaztemiembro.ftb.pe%20#ElMejorTipoDeCambio%20%23ForgetTheBank"
                                                style="text-decoration:none" target="_blank"
                                                data-saferedirecturl="https://www.google.com/url?q=https://api.whatsapp.com/send?text%3DYo%2520ya%2520realizo%2520mis%2520operaciones%2520de%2520cambio%2520de%2520d%C3%B3lares%2520con%2520ftb.pe.%2520%C2%A1Te%2520invito%2520a%2520conocerlos!%2520https%253A%252F%252Fhaztemiembro.ftb.pe%2520%23ElMejorTipoDeCambio%2520%2523ForgetTheBank&amp;source=gmail&amp;ust=1569468013757000&amp;usg=AFQjCNGjIlOk1r2oFnYyHqHeAjJUYpld1g">
                                                <div
                                                    style="color:#ffffff;background-color:#25d366;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                    <img width="20px" alt="icono_whatsapp" style="margin-top:5px"
                                                        src="https://ci5.googleusercontent.com/proxy/igNynrSU7e9PdYmvJghrNMSglI3aO-xIDfHoNvQbaO09KNiJMfGtVQj8Ju-sE-jHlDZpF15LINSwHfkYH9NyvXZzIO6aaC3djJ-Z=s0-d-e1-ft#https://ftb.pe/assets/img/email/icons/whatsapp_white.png"
                                                        class="CToWUd">
                                                </div>
                                            </a>
                                            <a href="https://www.facebook.com/dialog/share?app_id=318344762169514&amp;display=popup&amp;href=https%3A%2F%2Fhaztemiembro.ftb.pe"
                                                style="text-decoration:none" target="_blank"
                                                data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/dialog/share?app_id%3D318344762169514%26display%3Dpopup%26href%3Dhttps%253A%252F%252Fhaztemiembro.ftb.pe&amp;source=gmail&amp;ust=1569468013757000&amp;usg=AFQjCNHK0QMkUahFIJMuA0ypJgWdUiqWIg">
                                                <div style="color:#ffffff;background-color:#3b5999;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                    <img width="20px" alt="icono_facebook" style="margin-top:5px"
                                                        src="https://ci3.googleusercontent.com/proxy/xyjnjQOztz4TxlXicztEHF3QpD0dvEWwXgLfwlxXW3MEDboAFzG4kfj2YzPdVG9hzDrrWf1yjk0fHWpj8VYcqQYNYlq7Dv6XCrxD=s0-d-e1-ft#https://ftb.pe/assets/img/email/icons/facebook_white.png"
                                                        class="CToWUd">
                                                </div>
                                            </a>
                                            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fhaztemiembro.ftb.pe&amp;title=ftb.pe+El+mejor+cambio&amp;summary=Te+ofrecemos+el+mejor+tipo+de+cambio+del+mercado+de+manera+sencilla,+transparente+y+segura,+logrando+así+generar+ahorro+a+los+negocios+de+nuestros+clientes+desde+la+primera+operación.&amp;source=haztemiembro.ftb.pe"
                                                style="text-decoration:none" target="_blank"
                                                data-saferedirecturl="https://www.google.com/url?q=https://www.linkedin.com/shareArticle?mini%3Dtrue%26url%3Dhttps%253A%252F%252Fhaztemiembro.ftb.pe%26title%3Dftb.pe%2BEl%2Bmejor%2Bcambio%26summary%3DTe%2Bofrecemos%2Bel%2Bmejor%2Btipo%2Bde%2Bcambio%2Bdel%2Bmercado%2Bde%2Bmanera%2Bsencilla,%2Btransparente%2By%2Bsegura,%2Blogrando%2Bas%C3%AD%2Bgenerar%2Bahorro%2Ba%2Blos%2Bnegocios%2Bde%2Bnuestros%2Bclientes%2Bdesde%2Bla%2Bprimera%2Boperaci%C3%B3n.%26source%3Dhaztemiembro.ftb.pe&amp;source=gmail&amp;ust=1569468013758000&amp;usg=AFQjCNFsOr2uWZbAV_lABXCz2Lneyi1mww">
                                                <div
                                                    style="color:#ffffff;background-color:#0077b5;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                    <img width="20px" alt="icono_linkedin" style="margin-top:5px"
                                                        src="https://ci5.googleusercontent.com/proxy/m3HMwAWR1NcPmAyOJlyj4bZfpfpbu-lnEF_E_qRsi8RktEJCVfE-X2xIxv10WOEDREinipnCC8mySzim6FHmXow1OFZ-9EOJOA3i=s0-d-e1-ft#https://ftb.pe/assets/img/email/icons/linkedin_white.png"
                                                        class="CToWUd">
                                                </div>
                                            </a>
                                            <a href="mailto:?subject=Te+invito+a+conocer+ftb.pe+para+tus+cambios+de+dólares&amp;body=Hola,+yo+ya+realizo+mis+operaciones+de+cambio+de+dólares+con+ftb.pe.+¡Te+invito+a+conocerlos!+https://haztemiembro.ftb.pe+#ElMejorTipoDeCambio+%23ForgetTheBank"
                                                style="text-decoration:none" target="_blank">
                                                <div
                                                    style="color:#ffffff;background-color:#212121;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                    <img width="20px" alt="icono_email" style="margin-top:5px"
                                                        src="https://ci4.googleusercontent.com/proxy/jjI6SaGgJ6IaC64hIF5WBFP_5lpA_5HbAgbqu2Qb0oMYxpp6FERvtH0harOCi0m7Ys_TudLuoYQhjV3eLy8Z2OMZDl6Ufvk1=s0-d-e1-ft#https://ftb.pe/assets/img/email/icons/email_white.png"
                                                        class="CToWUd">
                                                </div>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <table align="center" cellspacing="0" cellpadding="0" border="0" width="600"
            style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#397afa">
            <tbody>
                <tr>
                    <td width="130"
                        style="width:160px;font-family:sans-serif;font-size:12px;text-align:center;color:#888888">
                        <div
                            style="width:150px;height:150px;margin:10px;border-radius:50%;background-color:#e7f9f6;width:120px;height:120px">
                            <img alt="logo_ftb" width="120" height="35" border="0"
                                style="width:120px;height:35px;font-family:sans-serif;font-size:15px;color:#555555;margin-top:42px"
                                src="{{asset('mail/img/icon-23.svg')}}"
                                class="CToWUd">
                        </div>
                    </td>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="font-family:sans-serif;font-size:12px;text-align:left;color:#ffffff;padding-top:20px">
                        <h3 style="padding-top:0px;padding-bottom:0px;font-size:20px!important;margin:0px!important;font-family: 'Rubik', sans-serif;color: #FFF">
                            ¿Deseas mayor información?</h3>
                        <p style="padding-top:0px;padding-bottom:0px;font-size:14px!important;font-family: 'Rubik', sans-serif;color:#FFF">Nos puedes escribir a
                            <span style="font-weight:600;text-decoration:underline">info&shy;@&shy;ftb&shy;.pe</span> o
                            llamar al
                            <span style="font-weight:600;font-family: 'Rubik', sans-serif">9927-29850</span> (Whatsapp)</p>
                        <p style="padding-top:0px;padding-bottom:0px;font-size:14px!important;font-family: 'Rubik', sans-serif;color:#FFF">Síguenos en nuestras
                            redes sociales:</p>
                        <table
                            style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin-left:0px!important">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="float:left;height:30px;width:30px;border-radius:50%;background-color:#ffffff;margin-right:10px;border:5px solid #ffffff;text-align: center;">
                                            <a href="#" style="text-decoration:none"
                                                target="_blank"
                                                data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/ftb.pe/&amp;source=gmail&amp;ust=1569468013758000&amp;usg=AFQjCNFB7mlHlNLVxptKdHHyt71A1RogOw"><img
                                                    width="16" height="16" alt="logo_facebook" border="0"
                                                    style=""
                                                    src="https://ci6.googleusercontent.com/proxy/2Q5VM72-YZGFzV5U0eyCdSFoqRnvqaAeD3UrOAkE5lQtJB9g9w6nnaJwkpY0vzlc95zV-A2EE6HaZxDm_2lW5DEb1bGP=s0-d-e1-ft#https://ftb.pe/assets/img/email/icons/facebook.png"
                                                    class="CToWUd">
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="float:left;height:30px;width:30px;border-radius:50%;background-color:#ffffff;border:5px solid #ffffff;text-align: center;">
                                            <a href="#"
                                                style="text-decoration:none" target="_blank"
                                                data-saferedirecturl="https://www.google.com/url?q=https://www.linkedin.com/company/ftb-pe&amp;source=gmail&amp;ust=1569468013758000&amp;usg=AFQjCNGXt7e8MherRI9M80fBZXpZxxc9gg"><img
                                                    width="16" height="16" alt="logo_linkedin" border="0"
                                                    style=""
                                                    src="https://ci4.googleusercontent.com/proxy/84J4NBVBGL9s-Axri4e1j7AnAcTyS3zjt2CPN3xaCYd2OBKbi6Y1-m6ja7-m1GY7XfdhJ7gQvcKjEO36kRMHhbQdGXqL=s0-d-e1-ft#https://ftb.pe/assets/img/email/icons/linkedin.png"
                                                    class="CToWUd">
                                            </a></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <div class="yj6qo"></div>
        <div class="adL">
        </div>
    </div>