
<div class="modal fade" id="modal_promo" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <form action="{{ route("admin.sendPromotion") }}" method="post">
                {{ csrf_field() }}
                    <h4 id="modal-label-3" class="modal-title text-center">Agregar cuenta bancaria</h4>
                    <p class="text-center">Añade la cuenta bancaria de donde envías tu cambio seguro esta</p>
                    <div class="mb-2 row">
                        <div class="form-group col-md-6" style="position: initial">
                            <label for="nombre-cuenta" class="xs-only">Asunto</label>
                            <input type="text" name="asunto" class="form-control" id="asunto" placeholder="Asunto" required>
                        </div>

                        <div class="form-group col-md-6" style="position: initial">
                            <label for="usuario" class="xs-only">Usuario</label>
                            <select name="usuario" id="usuario" class="form-control">
                                <option value="" selected>Selecciona un usuario...</option>
                                @foreach ($users as $key => $value)
                                    <option value="{{ $value->id }}" {{ $value->id == 169 ? 'selected': '' }}>{{ $value->name }} - {{ $value->email }} </option>
                                @endforeach
                            </select>
                            <small>Dejar en blanco si desea enviar a todos, solo se envia por usuario al selecionar</small>
                        </div>
                    </div>

                    <div class="form-row mb-2">
                        <textarea class="form-control" name="cuerpo" id="cuerpo" cols="30" rows="4" placeholder="Cuerpo del mensaje"></textarea>
                    </div>

                    <div class="form-row mb-2">
                    </div>

                    

                    <div class="float-right">
                        <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
                        <button class="btn btn-primary btn-b" type="submit">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>