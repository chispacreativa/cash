@if(Session::has('success_mail'))
<div class="alert alert-success">
    {{ Session::get('success_mail') }}
</div>
@endif

<div class="text-center darken-grey-text mb-4">
    <h1 class="font-bold mt-4 mb-3 h5">Crear promocion para usuarios</h1>
    <a class="btn btn-danger btn-md" href="#" data-toggle="modal" data-target="#modal_promo">Crear promocion <i class="fa fa-mail-forward pl-2"></i></a>
</div>

@include('admin.promocion.promocion')