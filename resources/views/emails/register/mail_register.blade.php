{{-- @component('mail::message')

Nuevo Intercambio.<br>

@component('mail::table')
| Envias        |          | Recibes  |
| ------------- |:--------:| --------:|
| S/ 10         | -->      | $10      |
@endcomponent

@component('mail::button', ['url' => route('intercambio.view', ['id' => $intercambio->id])])
Ver Intercambio
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent

--}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div style="padding:10px 0px;background-color:#eeeffe">
    <div style="width:100%;background-color:#eeeffe">>
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;font-family:sans-serif">
            &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;<wbr>&nbsp;&zwnj;&nbsp;&zwnj;&nbsp; </div>
        <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
            <tbody>
                <tr>
                    <td colspan="2" align="center">
                        <img width="120" height="35" alt="logo_ftb" border="0" style="height:auto;background:#ffffff;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555" src="{{asset('mail/img/logo.png')}}" class="CToWUd">
                    </td>
                </tr>
                <tr style="background-color:#397afa">
                    <td colspan="2" align="center" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                        <h3 style="font-family: 'Rubik', sans-serif;font-size:20px;font-weight:500;color:#ffffff;line-height:1;margin:0px">
                            {{$title}} </h3>
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
            <tbody>
                <!--tr style="background-color:#ffffff"-->
                <tr background="{{asset('mail/img/bg.png')}}" style="background-repeat: no-repeat">
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top" width="260" style="vertical-align:middle">
                        &nbsp; &nbsp;
                        <table style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:0px!important">
                            <tbody>
                                <tr>
                                    <td style="font-family: 'Rubik', sans-serif">
                                        <span style="color:#673ab7;font-family: 'Rubik', sans-serif;">Tenemos el mejor servicio de cambio para ti</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top" width="300" style="vertical-align:middle;text-align:center;padding-top:40px;padding-bottom:40px">
                        <img width="300" height="233" alt="img_fondos_enviados" border="0" style="height:auto;background:#ede7f6;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555" src="{{asset('mail/img/business.png')}}" class="CToWUd a6T" tabindex="0">
                        <div class="a6S" dir="ltr" style="opacity: 0.01; left: 752px; top: 427px;">
                            <div id=":ob" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Descargar" role="button" tabindex="0" aria-label="Descargar el archivo adjunto " data-tooltip-class="a1V">
                                <div class="aSK J-J5-Ji aYr"></div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="yj6qo"></div>
    <div class="adL">
    </div>
</div>
</body>
</html>