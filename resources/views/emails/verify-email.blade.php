@component('mail::message')
#Hola


Haga clic en el botón de abajo para verificar su dirección de correo electrónico.

@component('mail::button', ['url' => $url])
Confirme su dirección de correo electrónico
@endcomponent

Gracias,<br>
{{ config('app.name') }}


@slot('subcopy')
@lang(
    "Si tiene problemas para hacer clic en \":actionText\" ˈbotón, copia y pega la URL a continuación\n".
    'en su navegador web: [:actionURL](:actionURL)',
    [
        'actionText' => "Confirme su dirección de correo electrónico",
        'actionURL' => $url,
    ]
)
@endslot

@endcomponent

