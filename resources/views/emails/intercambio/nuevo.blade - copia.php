{{-- @component('mail::message')

Nuevo Intercambio.<br>

@component('mail::table')
| Envias        |          | Recibes  |
| ------------- |:--------:| --------:|
| S/ 10         | -->      | $10      |
@endcomponent

@component('mail::button', ['url' => route('intercambio.view', ['id' => $intercambio->id])])
Ver Intercambio
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent

--}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div style="padding:10px 0px;background-color:#eeeffe">
    <div style="width:100%;background-color:#eeeffe">
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;font-family:sans-serif">
            Ya enviamos los US$&nbsp;8,000 a tu cuenta.
        </div>
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;font-family:sans-serif">
            &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;<wbr>&nbsp;&zwnj;&nbsp;&zwnj;&nbsp; </div>
        <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
            <tbody>
                <tr>
                    <td colspan="2" align="center">
                        <img width="120" height="35" alt="logo_ftb" border="0" style="height:auto;background:#ffffff;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555" src="{{asset('mail/img/logo.png')}}" class="CToWUd">
                    </td>
                </tr>
                <tr style="background-color:#397afa">
                    <td colspan="2" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                        <h3 style="font-family: 'Rubik', sans-serif;font-size:20px;font-weight:500;color:#ffffff;line-height:1;margin:0px">
                            ¡Hemos enviado los fondos de tu operación #20190709C0003!</h3>
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
            <tbody>
                <!--tr style="background-color:#ffffff"-->
                <tr background="{{asset('mail/img/bg.png')}}" style="background-repeat: no-repeat">
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top" width="260" style="vertical-align:middle">
                        &nbsp; &nbsp;
                        <table style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:0px!important">
                            <tbody>
                                <tr>
                                    <td style="font-family: 'Rubik', sans-serif">Ya enviamos los
                                        <span style="color:#673ab7;font-family: 'Rubik', sans-serif;">US$ 5,000.00</span> a tu cuenta.
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td style="font-family: 'Rubik', sans-serif">Además, adjuntamos el comprobante de la operación para su registro en
                                        contabilidad, el mismo que también puedes descargar en nuestra plataforma.
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td>¡Gracias por la operación! </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top" width="300" style="vertical-align:middle;text-align:center;padding-top:40px;padding-bottom:40px">
                        <img width="300" height="233" alt="img_fondos_enviados" border="0" style="height:auto;background:#ede7f6;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555" src="{{asset('mail/img/business.png')}}" class="CToWUd a6T" tabindex="0">
                        <div class="a6S" dir="ltr" style="opacity: 0.01; left: 752px; top: 427px;">
                            <div id=":ob" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Descargar" role="button" tabindex="0" aria-label="Descargar el archivo adjunto " data-tooltip-class="a1V">
                                <div class="aSK J-J5-Ji aYr"></div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#FFF">
            <tbody>
                <tr>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td width="60"><img width="50px" alt="icono_referir" style="margin:5px 0px;border-radius:50%;background-color:#ffffff" src="{{asset('mail/img/verified-user.png')}}" class="CToWUd">
                    </td>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <table style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:0px!important">
                            <tbody>
                                <tr>
                                    <td style="color:#77838f;font-size:15px;padding-top:20px;font-family: 'Rubik', sans-serif;">
                                        ¡COMPARTE EL MEJOR TIPO DE CAMBIO CON TUS AMIGOS! CADA VEZ SOMOS MÁS
                                        <span style="font-style:italic">#abc</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px"><a href="#">
                                            <div style="color:#ffffff;background-color:#25d366;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                <img width="20px" alt="icono_whatsapp" style="margin-top:5px" src="#" class="CToWUd">
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div style="color:#ffffff;background-color:#3b5999;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                <img width="20px" alt="icono_facebook" style="margin-top:5px" src="#" class="CToWUd">
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div style="color:#ffffff;background-color:#0077b5;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                <img width="20px" alt="icono_linkedin" style="margin-top:5px" src="#" class="CToWUd">
                                            </div>
                                        </a>
                                        <a href="#" style="text-decoration:none" target="_blank">
                                            <div style="color:#ffffff;background-color:#212121;border-radius:50%;width:30px;height:30px;text-align:center;display:inline-block;margin-right:10px;margin-bottom:20px">
                                                <img width="20px" alt="icono_email" style="margin-top:5px" src="#" class="CToWUd">
                                            </div>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#397afa">
        <tbody>
            <tr>
                <td width="130" style="width:160px;font-family:sans-serif;font-size:12px;text-align:center;color:#888888">
                    <div style="width:150px;height:150px;margin:10px;border-radius:50%;background-color:#e7f9f6;width:120px;height:120px">
                        <img alt="logo_ftb" width="120" height="35" border="0" style="width:120px;height:35px;font-family:sans-serif;font-size:15px;color:#555555;margin-top:42px" src="{{asset('mail/img/icon-23.png')}}" class="CToWUd">
                    </div>
                </td>
                <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td style="font-family:sans-serif;font-size:12px;text-align:left;color:#ffffff;padding-top:20px">
                    <h3 style="padding-top:0px;padding-bottom:0px;font-size:20px!important;margin:0px!important;font-family: 'Rubik', sans-serif;color: #FFF">¿Deseas mayor información?</h3>
                    <p style="padding-top:0px;padding-bottom:0px;font-size:14px!important;font-family: 'Rubik', sans-serif;color:#FFF">Nos puedes escribir a
                        <span style="font-weight:600;text-decoration:underline">...</span> o llamar al
                        <span style="font-weight:600;font-family: 'Rubik', sans-serif">9999-9999</span> (Whatsapp)</p>
                    <p style="padding-top:0px;padding-bottom:0px;font-size:14px!important;font-family: 'Rubik', sans-serif;color:#FFF">Síguenos en nuestras redes sociales:</p>
                    <table style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin-left:0px!important">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="float:left;height:30px;width:30px;border-radius:50%;background-color:#ffffff;margin-right:10px;border:5px solid #ffffff;text-align: center;">
                                        <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="#"><img width="16" height="16" alt="logo_facebook" border="0" style="" src="#" class="CToWUd">
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div style="float:left;height:30px;width:30px;border-radius:50%;background-color:#ffffff;border:5px solid #ffffff;text-align: center;">
                                        <a href="#" style="text-decoration:none" target="_blank" data-saferedirecturl="#"><img width="16" height="16" alt="logo_linkedin" border="0" style="" src="#" class="CToWUd">
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <div class="yj6qo"></div>
    <div class="adL">
    </div>
</div>
</body>
</html>