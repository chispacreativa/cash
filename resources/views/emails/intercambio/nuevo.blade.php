<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Intercambio</title>
</head>
<body>
    <div style="padding:10px 0px;background-color:#eeeffe">
        <div style="width:100%;background-color:#eeeffe">
            <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
                <tbody>
                    <tr>
                        <td colspan="2" align="center">
                            <img width="120" height="35" alt="logo_ftb" border="0" style="height:auto;background:#ffffff;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555" src="{{asset('mail/img/logo.png')}}" class="CToWUd">
                        </td>
                    </tr>
                    <tr style="background-color:#397afa">
                        <td colspan="2" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                            <h3 style="font-family: 'Helvetica Nue', sans-serif;font-size:20px;font-weight:500;color:#ffffff;line-height:1;margin:0px;text-align: center;">{{ $title }}</h3>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#ffffff">
                <tbody>
                    <tr background="{{asset('mail/img/bg.png')}}" style="background-repeat: no-repeat">
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top" width="260" style="vertical-align:middle">
                            @if ($intercambio->status == 0)
                            @php
                                $__exchange_type = $intercambio->getTypeExchange();
                                $__rate = $intercambio->getMoneyRate();
                            @endphp
                            <table style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important">
                                <tbody>
                                    <tr>
                                        <td>Has registrado una orden de <span style="font-weight:600;color:#673ab7">compra</span> de
                                            <span style="font-weight:600;color:#673ab7">{{ $__exchange_type['before_symbol'] }}&nbsp;{{$intercambio->send_amount}}</span> a un tipo de cambio de
                                            <span style="font-weight:600;color:#673ab7">{{ $__rate['simbolo'] }} {{ $__rate['rate'] }}</span> por un contravalor de <span style="font-weight:600;color:#673ab7">{{ $__exchange_type['after_symbol'] }}&nbsp;{{$intercambio->receive_amount}}</span>.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            @endif
                        </td>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top" width="300" style="vertical-align:middle;text-align:center;padding-top:40px;padding-bottom:40px">
                            <img width="300" height="233" alt="img_fondos_enviados" border="0" style="height:auto;background:#ede7f6;font-family:sans-serif;font-size:15px;line-height:15px;color:#555555" src="{{asset('mail/img/business.png')}}" class="CToWUd a6T" tabindex="0">
                        </td>
                    </tr>
                </tbody>
            </table>
            <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#FFF">
                <tbody>
                    <tr>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td width="60"><img width="50px" alt="icono_referir" style="margin:5px 0px;border-radius:50%;background-color:#ffffff" src="{{asset('mail/img/verified-user.png')}}" class="CToWUd">
                        </td>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <table style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:0px!important">
                                <tbody>
                                    <tr>
                                        <td style="color:#77838f;font-size:15px;padding-top:20px;font-family: 'Helvetica Nue', sans-serif;padding-bottom: 20px;">
                                            ¡COMPARTE EL MEJOR TIPO DE CAMBIO CON TUS AMIGOS! CADA VEZ SOMOS MÁS
                                            <span style="font-style:italic">#abc</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" style="border-spacing:0px!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0px auto!important;margin:auto;background-color:#397afa">
            <tbody>
                <tr>
                    <td width="130" style="width:160px;font-family:sans-serif;font-size:12px;text-align:center;color:#888888">
                        <div style="width:150px;height:150px;margin:10px;border-radius:50%;background-color:#e7f9f6;width:120px;height:120px">
                            <img alt="logo_ftb" width="120" height="35" border="0" style="width:60px;height:auto;font-family:sans-serif;font-size:15px;color:#555555;margin-top: 30px;" src="{{asset('mail/img/icon-23.png')}}" class="CToWUd">
                        </div>
                    </td>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="font-family:sans-serif;font-size:12px;text-align:left;color:#ffffff;padding-top:20px">
                        <h3 style="padding-top:0px;padding-bottom:0px;font-size:20px!important;margin:0px!important;font-family: 'Helvetica Nue', sans-serif;color: #FFF">¿Deseas mayor información?</h3>
                        <p style="padding-top:0px;padding-bottom:0px;font-size:14px!important;font-family: 'Helvetica Nue', sans-serif;color:#FFF">Nos puedes escribir a
                            <span style="font-weight:600;text-decoration:underline">...</span> o llamar al
                            <span style="font-weight:600;font-family: 'Helvetica Nue', sans-serif">9999-9999</span> (Whatsapp)</p>
                        <p style="padding-top:0px;padding-bottom:0px;font-size:14px!important;font-family: 'Helvetica Nue', sans-serif;color:#FFF">Síguenos en nuestras redes sociales:</p>

                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>