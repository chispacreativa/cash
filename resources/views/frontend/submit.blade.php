<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>CambiCash</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{{ asset('css/stepper.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/style.css')  }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/blue.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/scroll.css') }}" type="text/css" />
</head>
<body class="">
    <div class="page-content" style="background-image: url('images/wizard-v3.jpg')">
        <!--Modal large  benficiario-->
        <div class="modal fade" id="modal-3" tabindex="-1" role="modal" aria-labelledby="modal-label-3"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-label-3" class="modal-title">Nuevo Beneficiario</h4>
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class=" col-md-12 heading-text heading-line text-center">
                                <h4>¿Cómo deseas agregar tu beneficiario?</h4>
                            </div>
                        </div>
                        <div class="optionBoxes ">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="box active icon-box effect medium center process">
                                        <div class="icon"> <a href="#"><i class="fa fa-clipboard-list"></i></a> </div>
                                        <h3>Tengo los datos</h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box  icon-box effect medium center process">
                                        <div class="icon"> <a href="#"><i class="fa fa-user-edit"></i></a> </div>
                                        <h3>Tengo su codigo</h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box  icon-box effect medium center process">
                                        <div class="icon"> <a href="#"><i class="fa fa-paper-plane"></i></a> </div>
                                        <h3>Se los pedié vía email</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-b" type="button">Continuar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Modal large -->

        <div class="wizard-v3-content">
            <div class="wizard-form">
                <div class="wizard-header">

                </div>
                <form class="form-register" action="#" method="post">
                    <div id="form-total">
                        <!-- SECTION 1 -->
                        <h2>
                            <span class="step-icon"><i class="fa fa-donate"></i></span>
                            <span class="step-text">Monto</span>
                        </h2>
                        <section>
                            <div class="inner">
                                <form>
                                    <h3 class="text-center">Cuánto dinero vas a enviar?</h3>
                                    <div class="cotizador mx-auto">
                                        <div class="form-group">
                                            <label>Tu envias</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-append ">
                                                    <input class="form-control" type="number" value="42"
                                                        id="example-number-input">
                                                </div>
                                                <select class="form-control color-celeste text-light" id="type">
                                                    <option value="Dolar">Dolar</option>
                                                    <option value="Soles">Soles</option>
                                                    <option value="Soles">Soles</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Tu destinatario recibe</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <input class="form-control" type="number" value="42"
                                                        id="example-number-input">
                                                </div>
                                                <select class="form-control color-celeste text-light" id="type">
                                                    <option value="line">Dolar</option>
                                                    <option value="bar">Soles</option>
                                                    <option value="bar">Bitcoin</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span>Ahorrarás hasta:</span><span><b>30.000 $</b></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <button class="detalle  float-right"><span
                                                            class="text-right">Detalles <i
                                                                class="fa fa-caret-right"></i></span></button>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="card text-center bg-light float-fix">
                                                    <div class="card-body">

                                                        <span>Tipo de Cambio : <span><b> 1 USD = 682,50
                                                                    CLP</b></span></span><br>
                                                        <span>Debería llegar en máximo: : <span><b> 3 Días(s)
                                                                    Hábil(es)</b></span></span><br>
                                                        <span>Costo Total (Ya incluido): : <span><b> 4
                                                                    PEN</b></span></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>


                                </form>
                            </div>
                        </section>
                        <!-- SECTION 2 -->
                        <h2>
                            <span class="step-icon"><i class="fa fa-hand-holding-usd"></i></span>
                            <span class="step-text">Beneficiario</span>
                        </h2>
                        <section class="transferSteps">
                            <div class="inner destinatarios">
                                <h3 class="text-center">¿A quién le enviarás dinero?</h3>
                                <div class="list">
                                    <div class="text-center mb-5 mt-2 mt-md-0">
                                        <h2> ¿A quién le enviarás dinero ? </h2>
                                    </div>
                                    <div class="">
                                        <div class="">
                                            <form class="site-search">
                                                <div class="form-group clearfix">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input class="form-control" type="search"
                                                                placeholder="Buscar...">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="mt-2 float-right"><a class="btn "
                                                                    data-target="#modal-3" data-toggle="modal" href="#">
                                                                    + Agregar Nuevo</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- -->
                                    <!-- -->
                                    <span class="messageNotFound"> ¡Debes seleccionar un beneficiario para enviar
                                        dinero! </span>
                                    <div class="scrollContent">
                                        <div>
                                            <div class="item transfer cursor-pointer">
                                                <div class="radio">
                                                    <div class="md-radio md-primary md-theme-default">
                                                        <div class="md-radio-container">
                                                            <div class="md-ripple">
                                                                <input class="form-check-input" type="radio">
                                                            </div>
                                                        </div>
                                                        <!-- -->
                                                    </div>
                                                </div>
                                                <!-- -->
                                                <div class="detail">
                                                    <div class="name">
                                                        <label> Nombre </label>
                                                        <p>Gloria Castro</p>
                                                    </div>
                                                    <div class="account_number"><label>Banco Falabella</label>
                                                        <p> 4919258912350874 </p>
                                                    </div>
                                                    <div class="country">
                                                        <label> País </label>
                                                        <p>Colombia COP</p>
                                                    </div>
                                                    <div class="account_type"><label>Tipo de Cuenta</label>
                                                        <p> ahorro </p>
                                                    </div>
                                                    <!-- -->
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="item transfer cursor-pointer">
                                                <div class="radio">
                                                    <div class="md-radio md-primary md-theme-default">
                                                        <div class="md-radio-container">
                                                            <div class="md-ripple">
                                                                <input class="form-check-input" type="radio">
                                                            </div>
                                                        </div>
                                                        <!-- -->
                                                    </div>
                                                </div>
                                                <!-- -->
                                                <div class="detail">
                                                    <div class="name">
                                                        <label> Nombre </label>
                                                        <p>Gloria Castro</p>
                                                    </div>
                                                    <div class="account_number"><label>Banco Falabella</label>
                                                        <p> 4919258912350874 </p>
                                                    </div>
                                                    <div class="country">
                                                        <label> País </label>
                                                        <p>Colombia COP</p>
                                                    </div>
                                                    <div class="account_type"><label>Tipo de Cuenta</label>
                                                        <p> ahorro </p>
                                                    </div>
                                                    <!-- -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- SECTION 2 -->
                        <h2>
                            <span class="step-icon"><i class="fa fa-clipboard-list"></i></span>
                            <span class="step-text">Verificacion</span>
                        </h2>
                        <section>
                            <div class="inner confirmationData">
                                <h3 class="text-center">Verifica esta información</h3>
                                <form>
                                    <div class="confirmationData">
                                        <div class="top">
                                            <div>
                                                <div class="text-center">
                                                    <h5>Transacción</h5>
                                                </div>
                                                <div class="wrapperDetail left">
                                                    <div><span>Monto a Enviar</span><span class="double"><span><img
                                                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAANCAYAAACQN/8FAAAAxUlEQVQokaWQPw5BQRCHv/e8QuIQChdwARKFA2ynkC1EIXQanc0cgERCpZCsSrUlhYiLiCuI2r9min2Ixq+ZmW++YjKJiBAnWHcAMuOlHvP0TVoCDaAWrFt/FYN1E6Ab7drBunlODNaNgKGyC3DVfhCsGwMk1TN9YKGLJjADSkAP2CofprFkvOyBClA2XnZ6L8A0BVpAVyWARM8pGC9HoAO0M+NlQz4PrQXgbrysPt7zlmc8/BJz+Ussak1imH0RTyrfYvgCew8xhTmJ5zoAAAAASUVORK5CYII=">
                                                                1.000 PEN </span></span></div>
                                                    <!---->
                                                    <!---->
                                                    <div><span>Monto a Recibir</span><span><img
                                                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAANCAYAAACQN/8FAAAAvklEQVQoka2QMQ4BQRiFP7I9cQRuoFJxBtFRKR1AobDjTeMKSpXSMRQOoFK4gKh3hV9hNhlM6SWTmf+9b5J5U5NErNy5M5ABbS89Kz/jV50oKyuzngCLsFtspsCk/gLW4iHLnRsCDS9tv4AnQO7cGLhjYGH1JWFQGFg4d6scg3kEDwxOBheDnkEZ/CXh5iqCbwZXg0eY15LeZby0AjbhbU2gFYpuvbT4aO2lGbCLiu69NE1+j5cmwAE4emkUZy8X+1WXgFcs7QAAAABJRU5ErkJggg==">
                                                            981.469 COP</span></div>
                                                    <div><span>Tipo de Cambio</span><span>1 PEN = 1014,33 COP</span>
                                                    </div>
                                                    <div><span>Costo Total (Ya incluido)</span><span>32 PEN</span></div>
                                                    <div><span>Fecha Inicio</span><span>20/8/2019 22:23:19</span></div>
                                                    <div><span>Fecha Vencimiento</span><span>21/8/2019 22:23:19</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="text-center">
                                                    <h5>Beneficiario</h5>
                                                </div>
                                                <div class="wrapperDetail right">
                                                    <div><span>Nombre</span><span>dddd ddddd</span></div>
                                                    <div><span>País</span><span>Colombia</span></div>
                                                    <div><span>Banco</span><span>CONFIAR</span></div>
                                                    <div><span>N° de Cuenta</span><span>5346847612323132</span></div>
                                                    <div><span>Tipo de Routing</span><span></span></div>
                                                    <div><span>N° de Routing</span><span></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bottom">
                                            <div class="wrap mx-auto">
                                                <div>
                                                    <div class="md-field md-theme-default customSelect"><label
                                                            class="mdc-floating-label">Propósito</label><select
                                                            name="purpose" id="proposito" aria-required="true"
                                                            aria-invalid="false">
                                                            <option disabled="disabled" class="notranslate">Selecciona
                                                                uno</option>
                                                            <option value="[object Object]">
                                                                <div>😎</div> Propia Cuenta
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>👪</div> Remesas
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>🚀</div> Viajes
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>💙</div> Donación
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>🚑</div> Tratamiento Médico
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>🏨</div> Hotel
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>🤓</div> Estudios
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>💸</div> Préstamo
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>🔌</div> Servicios
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>🚢</div> Importación
                                                            </option>
                                                            <option value="[object Object]">
                                                                <div>👽</div> Otros
                                                            </option>
                                                        </select></div><small class="" style="display: none;"><i
                                                            class="fa fa-warning help is-danger"></i>
                                                    </small>
                                                </div>
                                                <div>
                                                    <div class="md-field md-theme-default"><label
                                                            for="md-input-lesdriqbd">Comentario para
                                                            Beneficiario</label><input type="text"
                                                            id="md-input-lesdriqbd" class="md-input"><span
                                                            class="tooltip-field"><svg aria-hidden="true"
                                                                focusable="false" data-prefix="fas"
                                                                data-icon="question-circle" role="img"
                                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                                                class="text-info svg-inline--fa fa-question-circle fa-w-16">
                                                                <path fill="currentColor"
                                                                    d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"
                                                                    class=""></path>
                                                            </svg></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                        <!-- SECTION 3 -->
                        <h2>
                            <span class="step-icon"><i class="fa fa-credit-card"></i></span>
                            <span class="step-text">Pago</span>
                        </h2>
                        <section>
                            <div class="inner successStep">
                                <h3 class="text-center">Verifica esta información</h3>
                                <div class="rightSide">
                                    <div class="selectPayMethod">
                                        <div class="optionBoxes">
                                            <div class="box active"><img
                                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAA3CAYAAACb4M1PAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RkZDMjNGRDExRDgzMTFFOUEzQjdEODUxOThFMTA4Q0QiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RkZDMjNGRDAxRDgzMTFFOUEzQjdEODUxOThFMTA4Q0QiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTA5NDNFQjAxRDgxMTFFOTkwQ0FBNjk1REM2Rjk2RjUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTA5NDNFQjExRDgxMTFFOTkwQ0FBNjk1REM2Rjk2RjUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6Lv8umAAAGvUlEQVR42uxaCWxURRh+u71RBCFoARGMaEWjggeC2lRBiaiI94W3YogKATWKREQxKkIEbYJHxAMkiIYqXiQKahWlhsghCGgFrYpNUSgtxRba3X1+436T/hm79M12cdMwf/Jl5s28mTfzzz//tRvyfd9zlDyFHQscAx0DHQMdAx05BjoGOgY6BjpyDHQMdAx0DHTkGOgY6Bh4wFCm2RAqrlBtg4DO6lF0Vfrj+qxCfy/U+wG78PwNngeiPprvhjnnXtYzWFdtWcAe1jNZz2K9gfUMtudwPlXPBWKcJw9oAjoBKhNcwzKX/TGO1d/PZj3Cdr2eLLEGVS7HXl5LhoEhMyMNhhShKE3wfj6wFDiRzwcDLwI3tHdJAgNDKZFA0E/AXJ6qnlSf9m7geeB8oBZopDQoKgbWclyEEjydY8cBf1MiglIUqKeU+JSaQyhF/9k/2/O45kYBRRN46M8C67kO1dcNeIrf8lIigdYTFFesQDEY6KmuuejqDVQAXwNnp1nAxgOzgInA00afOuyMZCUwFUYkwrKv0f4uyzcs51MHcQewAKiidPnUXe8BDwEDLedcwnIa0EO05ySQaLsrDCnqj+JUir/PUinkckrQUOBIg+l/4tTeF9ekWsz7OjAA+A14KeBa1EYmA1OM9joaji7AJYRHBj8CbAkwdznXdAvwJddWx0PxDWNpf4XBwH3d43xKQkukjMhs4GagPxf1MjCE/ScAGwOuZSf1pqJ3KDWfUg1oaRlO3CnGXQO8HfAbPwAFwB/Uy18Bvyv9CmEIJy2BoJuAIp5GjEpWKdbVlCylQ45he4Qntp0nqDe9UhgJtcBrAzIvj4aonsway42ZpL61mHgcmMFvvMU55gb41unAImAYUCIM0M50GhHl1pzHxypa6Rm0nq2RsqqbyfABLfQr6bqXOm8HsBCYqtQH+yfSimrmfBtw2crtGgOcxeftkMBu6WKgKs4EViQx/EPgIkpcodG3kAxs6aqfA6zj81TqzjrhYAelo4Cf1a0CA7NS5UiriUbRaPjCx1J+3zx8qBrvjOLHQ5S6akYTSyy+fTywgSpBzbVV9E0CnqAauZ06MZ/6tYhqRlrPVcApwH3ATFsfOtWOtNJpicKaDYxE5ifoL6DFC0IPs5xiMK8zmdfE+X4R1lhJ3hdAV2OuW4HvgMcsGZiT8liYxuEyXkttmWKUNH1Nb6Rnn8GNdQC+FxYzCI1k+YqxoUmsLyfzMoWv6VECTVpHJ74H9eVKSwlsmxvTRh14KSVgpMWwfrTQG+nqeHRPEqmAkxiC7YuUO3UX8ACNWFAJbPiXD2mMRCbTuR1iMeZolktF2x5aYxmTK0f8V0MCE9FqYRhsKJbqK2xLdSL4D0pdjTBQ0ed0RSq5qQvoutQGnLM8SaEItWXzqUyoRize3dXC96+ixOn2NdTH1bTArdFhLGv+zyxFKhlocxU0o04zIpIG8dyBCr6WFrk10nPVtjcG+iIZEJQ2sSwU+cR5jEzmiOds6rQdxviOLcw5nOX69iqBNua8XhgQ03pPZ3llgvW9yWs62EiBncx6aXtjoFbCuy3HPSkYFjKs73xK44+MWPRaZzKBEDauqnb851iuY6/lwe8XBmrre6jluFI6wIeLa+sJR11FPX1ZbqIxmcD+i0Wm5wov/hODx1DO9vDTboW1gi9hzHquxdjLWd7mxZOjZuppNuvHUVeWMYPykXhvEcvRwrq3RupgXmCkE07i9uwXI9KFIeBn3GDPAGO3cIzHOHYBra9Hi3wPY2OV6upuZH06Ccbd34IUJ6JZVBMqndWb64+mk4E6IL+OeTYlBRcyHg1yrRfTYPicQ8XTj3rxDLd2S9YyFleuzt28+jUMAxXjnrFIn40XB1ZI/7VjOiMRfQhlDLs+AT6m1JQEDPHUe8fSSIxghkZnacq5ye5e8+/RHvvqLNb5oBfPPUbJuDLhfoXTKYGa9K9df9HFqKI+HBFw/GbG1MogPAdsA47gAQwj82K85tcDvYRDHmRt01gfKpiXuz/SWckewlbDPVDXeRmvzAcW8y0T4w7y4j9+x3gwDUmucRDLV714PlEmMKJtEaRUMFCHcFfTOc72mv/rougML/7DegOvS0wsOGrUM9kfo2XcRrWQQWPSh8YqROvfJAyA/m9OWFzLSr4zRhxyAXVpo9ec04wku/lU5APXCIXfbimVKX1bUn+VGEsJyaO+8XnaWhLzKFFNwk3xDamJiueoaPeFlDSxrsdmCjcqTMmNiDb9L69ar/n/NTniFjTRayhNmwQe6OT+YOkY6BjoGOgY6MgxMF30jwADAGVL2HVLZ4LvAAAAAElFTkSuQmCC">
                                                <p>Transferencia Bancaria</p><span>SIN COSTO</span>
                                            </div>
                                            <div class="box"><img
                                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAA3CAYAAACo29JGAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkVCQzIyMjlGMUQ4MjExRTlBRTdGOURFRjc3OTE2MUY1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkVCQzIyMkEwMUQ4MjExRTlBRTdGOURFRjc3OTE2MUY1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RUJDMjIyOUQxRDgyMTFFOUFFN0Y5REVGNzc5MTYxRjUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RUJDMjIyOUUxRDgyMTFFOUFFN0Y5REVGNzc5MTYxRjUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7m+8n2AAAEt0lEQVR42uxaaYhNYRg+986MGcugsWQw1hlkGIUs+WENRUn4g5qyJ40QCiN7wh9LIpI18gehKDsJWSeyjBrGLsY+mMX1vO5z9HUcc8+ZOXfmu9N96/HNWe4533Pe93u3jy8QCBjVVfxGNZYouSg5DSXWt+GJbnPKAjoApRV4RgzwIFYzYl2B9Z5pTjNytTi+AFYBNVwuMdFYMTAPaKIbudcc84HNTn4QyGr1zzkstXEYknUgtxYYAiQAdXiuF/CImvCV9WMQkeslwCdgBshewRivi1lOU0j9nTOQVo5nTQau6LTmfpJcF+Ar5/QLeMVr1nAl1+oBDXj8DZgLzAQKdXMo5uRzbK4l0OSsTuM98WfNwTTNeBbQLYibE0pUzolWvhPFCkSTP2w+RD3dMxRTQ52A49SaEMkFRDN5/FukM7BA+SA+3ePcd2osh5OVcNADeGO5byqwBVgJ9AGGWUxX2yCeTGJCqDWdh1W2AgXAQaCd4mS0T5yLOD7734Qpxyz3R1RVEB/iupmWlOpOrjRcD65qclLa1AwXwaoiJy58B3Cf2cmXsBSrlUxKYthsYDwQx8BcoqyxgrK8nyIfLPcVlkWuRqjsuwJSzPi1nDHKlP08vgu0oBU1Vubl5+izeV5jpZPgV4L5W5WX/HMHaGOTmZeHhATiuiHu2wRsZEkj0sjGsaQ7XIfW+wYBK4DPJrkMi0mUR4O/qJ04EixU1rOPxM8Bs1hlq3IA6AfU53M+MzlODPE+9b4ivr8vNDnHLHp9xvq8AEuNxApqTjKGMfIC4EKlZd2sxEEqFcNVIIllUILf4QJ245wSqqS0yGr1GMMuHtaWCsLvYUjwO8wuwilDlTIqTrfE2elHXAd0E4uDORZxXXdi4r2PTmpwJJKLpWOyk4cwz/EgfErHksdp5SBxsSMTAPG0i3htL8fmOlcFoURKopPAaWgqG+NOnk9SS6HqshGSa+cYI9EsMxi6JDOJwfrqjXFhJLQZQskDoH2k1HNu5QJNUPBRSRuPVQeznGI5PmsE9xUaeqG5ZsB8I7hBmKgB2Wyme73s0kg35KRP+BxYbQQ3CKX9NqOKyU3iaJqouQX2041ZjjKCHV6R7UawfT2Gddkl4HaYSYwERnC+JSyhUhnAX7FlIR5TspMUjG3dkFvDcSwraNMkljHPG2RYNiE8liVK3alKHpPl7krxmm4mz07NMklpDZiyh2Oykol7VT5ZZTidyURgKbUlsgwZyiN2EkTOAJlcLkOdau48zeIUtSS/O8JrRy2eNzdM6dY25Xgv35PJ1Osrz18E2d1uQ8F0eqSBRnB7thZ/K19tsdJDEdnNNSjO5yVwjU0gr8mK9MMay1dCQWp54txL1kvSSujPc4eA0YoZmo2anoQ1k5fs4hZwE7gu5Ql7LW5irTiTFIsWUzjKs07YkXOyTt4BA0K82OCaeGoEd2k6c4Gn0SFk0JS8kHt0cKK5P41dmOQnO3LStepKkrEhaqkCVr4NlHulIdOUfx+2CQ3xrJxlv60L39WG3Ss3rXR53wdqaILSIPpYVuIs/fobHn3RljbkJKheJiq1ZM9mVVvT4tLdiNmbFLM4p0si6ov+T9kouSi5KDmv5LcAAwC4HiHZknvyAAAAAABJRU5ErkJggg==">
                                                <p>Depósito Bancario</p><span>SIN COSTO</span>
                                            </div>
                                            <div role="alert" aria-live="polite" aria-atomic="true"
                                                class="w-100 mt-0 infoPreFound alert alert-info">
                                                <p class="mb-0"> Solo aceptaremos pagos de <b>Kdkdk</b>. </p>
                                                <p class="mb-0"> Pagos de terceros serán Rechazados </p>
                                                <div class="custom-control custom-checkbox"><input type="checkbox"
                                                        id="defaultUnchecked" class="custom-control-input"><label
                                                        for="defaultUnchecked" class="custom-control-label"><b>Estoy de
                                                            acuerdo</b></label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/plugins.js')}}"></script>
    <script src="{{ asset('js/functions.js')}}"></script>
    <script src="{{ asset('js/jquery.steps.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
</body>
</html>