<script src="{{ asset('libs/js/jquery.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}?{{ rand() }}"></script>
<script src="{{ asset('libs/js/jquery-migrate.min.js') }}"></script>

<!-- JS Implementing Plugins -->
<script src="{{asset('libs/js/hs.megamenu.js')}}"></script>
<script src="{{asset('libs/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('libs/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('libs/js/flatpickr.min.js')}}"></script>
<script src="{{asset('libs/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('libs/js/jquery.validate.min.js')}}"></script>
 <script src="{{asset('libs/js/summernote-lite.js')}}"></script>
 <script src="{{asset('libs/js/bootstrap-tagsinput.min.js')}}"></script>
 <script src="{{asset('libs/js/jquery.fancybox.min.js')}}"></script>
 <script src="{{asset('libs/js/typed.min.js')}}"></script>
 <script src="{{asset('libs/js/svg-injector.min.js')}}"></script>
 <script src="{{asset('libs/js/slick.js')}}"></script>
 <script src="{{asset('libs/js/custombox.min.js')}}"></script>
 <script src="{{asset('libs/js/custombox.legacy.min.js')}}"></script>
 <script src="{{asset('libs/js/appear.js')}}"></script>
 <script src="{{asset('libs/js/circles.min.js')}}"></script>
 {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhnQG99YZrlRp9E1-yzEN4YqwF5QHeyrg"></script> --}}
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhnQG99YZrlRp9E1-yzEN4YqwF5QHeyrg"type="text/javascript"></script> --}}
 <!-- JS Front -->
 <script src="{{asset('libs/js/hs.core.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.header.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.unfold.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.focus-state.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.malihu-scrollbar.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.validation.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.fancybox.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.slick-carousel.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.show-animation.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.svg-injector.js')}}"></script>
 <script src="{{asset('libs/js/components/hs.go-to.js')}}"></script>
 <script src="{{asset('libs/js/hs.modal-window.js')}}"></script>
 <script src="{{asset('libs/js/hs.step-form.js')}}"></script>
 <script src="{{asset('libs/js/hs.range-datepicker.js')}}"></script>
 <script src="{{asset('libs/js/hs.chart-pie.js')}}"></script>
 <script src="{{asset('libs/js/hs.progress-bar.js')}}"></script>
 <script src="{{asset('libs/js/hs.selectpicker.js')}}"></script>
 <script src="{{asset('libs/js/hs.datatables.js')}}"></script>
 <script src="{{asset('libs/js/cotizador.js')}}"></script>
 <script src="{{asset('libs/js/countrySelect.min.js')}}"></script>
 <script src="{{asset('libs/js/hs.scroll-nav.js')}}"></script>

 <script src="{{asset('libs/js/jquery.dd.js')}}"></script>
 <script src="{{asset('libs/js/jquery.dd.min.js')}}"></script>


 <script src="{{asset('libs/js/demoad.js')}}"></script>

