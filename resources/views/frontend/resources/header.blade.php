<!-- ========== HEADER ========== -->
<header id="header" class="u-header {!! !empty($relative) ? '' : 'u-header--abs-top-md' !!} u-header--bg-transparent u-header--show-hide-md" data-header-fix-moment="500" data-header-fix-effect="slide">
<!--header id="header" class="u-header u-header--bg-transparent u-header--show-hide-md" data-header-fix-moment="500" data-header-fix-effect="slide"-->
    <div class="u-header__section d-flex p-2">
        <div id="logoAndNav" class="container p-0 ml-3">
            <!-- Nav2 -->
            <nav class="navbar navbar-expand-lg p-0 ml-2" id="myNav">
	        <div class="container">
		<div class="d-flex w-100">
			<!-- Logo -->
            <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="{{url('/')}}" aria-label="Cash">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="46px" height="46px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
                        <path fill="#3F7DE0" opacity=".65" d="M23,41L23,41c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18h11.3C38,5,41,8,41,11.7V23C41,32.9,32.9,41,23,41z" />
                        <path class="fill-info" opacity=".5" d="M28,35.9L28,35.9c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18l11.3,0C43,0,46,3,46,6.6V18C46,27.9,38,35.9,28,35.9z" />
                        <path class="fill-primary" opacity=".7" d="M18,46L18,46C8,46,0,38,0,28v0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6V28C35.9,38,27.9,46,18,46z" />
                        <text font-style="normal" font-weight="bold" xml:space="preserve" text-anchor="start" font-family="Helvetica, Arial, sans-serif" font-size="24" id="svg_5" y="34.891162" x="12.827588" fill-opacity="null" stroke-opacity="null" stroke-width="0" stroke="null" fill="#ffffff">C</text>
                    </svg>
                    <span class="u-header__navbar-brand-text">Cash</span>
                </a>
                <!-- End Logo -->

			
		
			
			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContentGlobal" aria-controls="navbarSupportedContentGlobal" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"><svg class="svg-inline--fa fa-bars fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="bars" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg><!-- <i class="fas fa-bars"></i> -->
					
				</span>
			</button>
		</div>
		
		<div class="navbar-collapse collapse" id="navbarSupportedContentGlobal" style="width:200%">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item  active ">
					<a role="button" class="nav-link pasteParamsButtonInternal" href="{{route('nosotros')}}">{{__('Nosotros')}}</a>
				</li>
				<li class="nav-item ">
					<a role="button" class="nav-link pasteParamsButtonInternal" href="{{route('faq')}}">{{__('FAQ')}}</a>
				</li>
				<li class="nav-item ">
					<a role="button" class="nav-link pasteParamsButtonInternal" href="{{route('terminos')}}">{{__('Términos')}}</a>
				</li>
				
				<li class="nav-item ">
					<a role="button" class="nav-link pasteParamsButtonInternal" href="{{route('privacidad')}}">{{__('Privacidad')}}</a>
				</li>
				<li class="nav-item ">
					<a role="button" class="nav-link" target="_blank" href="{{route('contacto')}}">{{__('Contacto')}}</a>
				</li>
				
			
				
			</ul>
		</div>
	</div>
</nav>


            <!-- End Nav2 -->







        </div>
           <!-- Topbar -->
           <div class="container p-0 u-header__hide-content w-xl-25 pt-2 d-flex align-items-center">
            <div class="">
                <!-- Language -->
                {{-- <div class="position-relative">
                  <a id="languageDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center" href="javascript:;" role="button" aria-controls="languageDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#languageDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
                  
                        <img class="dropdown-item-icon" src="{{asset('img/flags/pe.svg')}}" alt="SVG">
                        <span class="d-inline-block d-sm-none">PE</span>
                        <span class="d-none d-sm-inline-block">Perú</span>
                    
                    </a>

                    <div id="languageDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="languageDropdownInvoker">
                        <a class="dropdown-item active" href="#">English</a>
                        <a class="dropdown-item" href="#">Español‎</a>
                    </div>
                </div> --}}
                <!-- End Language -->

                <div class="ml-auto">
                     <!--Jump To -->
                    <!--<div class="d-inline-block d-sm-none position-relative mr-2">
                        <a id="jumpToDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center" href="javascript:;" role="button" aria-controls="jumpToDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#jumpToDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
                            Ir a
                        </a>

                        <div id="jumpToDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="jumpToDropdownInvoker">
                            <a class="dropdown-item" href="{{route('contacto')}}">Contacto</a>
                        </div>
                    </div> -->
                    <!-- End Jump To -->

                    <!-- Links -->
                    <!--<div class="d-none d-sm-inline-block ml-sm-auto">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-0">
                                <a class="u-header__navbar-link" href="{{route('contacto')}}">Contacto</a>
                            </li>
                        </ul>
                    </div>  -->
                    <!-- End Links -->
                </div>

                <ul class="list-inline ml-2 mb-0">
                    @guest
                    <!-- Account Login -->
                    <li class="list-inline-item">
                        <!-- Account Sidebar Toggle Button -->
                      <!--  <a id="sidebarNavToggler" class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
                            <span class="fas fa-user-circle btn-icon__inner font-size-1"></span>
                        </a> -->

                        <a role="button" 
                        class="btn btn-xs btn-primary u-btn-primary transition-3d-hover d-xs-none"
                        style="font-size: 13px"
                        href="javascript:;" 
                        role="button" 
                        aria-controls="sidebarContent" 
                        aria-haspopup="true" aria-expanded="false" 
                        data-unfold-event="click" 
                        data-unfold-hide-on-scroll="false" 
                        data-unfold-target="#sidebarContent" 
                        data-unfold-type="css-animation" 
                        data-unfold-animation-in="fadeInRight" 
                        data-unfold-animation-out="fadeOutRight" 
                        data-unfold-duration="500">
                            Iniciar Sesión
                        </a>
                        <!-- End Account Sidebar Toggle Button -->
                    </li>
                    <!-- End Account Login -->
                    @endguest
                    @auth
                    <!-- Account Login -->
                    <li class="list-inline-item">
                        <!-- Account Sidebar Toggle Button -->
                        <a id="sidebarNavToggler" class="btn btn-xs btn-text-secondary u-sidebar--account__toggle-bg ml-1" href="javascript:;" role="button"
                        aria-controls="sidebarContent"
                        aria-haspopup="true"
                        aria-expanded="false"
                        data-unfold-event="click"
                        data-unfold-hide-on-scroll="false"
                        data-unfold-target="#sidebarContent"
                        data-unfold-type="css-animation"
                        data-unfold-animation-in="fadeInRight"
                        data-unfold-animation-out="fadeOutRight"
                        data-unfold-duration="500">
                        <span class="position-relative">
                            @php
                            $details = App\Model\UsuarioDetalle::where("id_user", Auth::user()->id)->first();
                        @endphp
                      
                            <span class="u-sidebar--account__toggle-text text-capitalize">{{ Auth::user()->name }}</span>
                            <img class="u-sidebar--account__toggle-img" src="{{asset('img/'. $details->image)}}" alt="">
                            <span class="badge badge-sm badge-success badge-pos rounded-circle">3</span>
                        </span>
                        </a>
                        <!-- End Account Sidebar Toggle Button -->
                    </li>
                    <!-- End Account Login -->
                    @endauth
                </ul>
            </div>
        </div>
        <!-- End Topbar -->
    </div>
</header>
<!-- ========== END HEADER ========== -->