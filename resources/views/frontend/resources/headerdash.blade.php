<header id="header" class="header-colored dark hidden-md hidden-lg" data-fullwidth="true" >
    <div class="header-inner">
        <div class="container">
            <!--Logo-->
            <div id="logo">
                <a href="{{ url('/') }}" class="logo">Cambi Cash</a>
            </div>
            <!--End: Logo-->
            <!--Icono de Menú-->
            <div class="header-extras  ">
                <ul>
                    <li>
                        <!--side panel-->
                        <a id="side-panel-trigger" href="#" class="toggle-item" data-target="body" data-class="side-panel-active">
                            <i class="fa fa-bars"></i>
                            <i class="fa fa-times"></i>
                        </a>
                        <!--end: side panel-->
                    </li>
                </ul>
            </div>
            <!--end: Header Extras-->
            <!--Header Extras-->
            <div class="header-extras right">
                <ul>
                    <li class="d-none d-ss-block">
                        <!--Notifications-->
                        <div id="shopping-cart" class="p-dropdown">
                            <a class="">
                                <span class="shopping-cart-items">2</span>
                                <i class="icon-bell11"></i>
                            </a>
                            <div class="p-dropdown-content">
                                <div class="widget-notification">
                                    <div class="">
                                        <h4 class="mb-0">Notificaciones</h4>
                                        <p class="text-muted">tines 2 nuevas notificaciones</p>
                                        <div class="notification-item notification-new">
                                            <div class="notification-image"> <a href="#"><img
                                                        src="images/shop/products/10.jpg"></a></div>
                                            <div class="notification-meta">
                                                <a href="#">New order just placed</a>
                                                <span>18:20</span>
                                            </div>
                                        </div>
                                        <div class="notification-item notification-new">
                                            <div class="notification-image"> <a href="#"><img
                                                        src="images/shop/products/11.jpg"></a></div>
                                            <div class="notification-meta">
                                                <a href="#">New account is created</a>
                                                <span>22:03</span>
                                            </div>
                                        </div>
                                        <div class="notification-item">
                                            <div class="notification-image"> <a href="#"><img
                                                        src="images/shop/products/10.jpg"></a></div>
                                            <div class="notification-meta">
                                                <a href="#">Server Backup is finished successfully</a>
                                                <span>14:12</span>
                                            </div>
                                        </div>
                                        <div class="notification-item">
                                            <div class="notification-image"> <a href="#"><img
                                                        src="images/shop/products/11.jpg"></a></div>
                                            <div class="notification-meta">
                                                <a href="#">Failed to import document file</a>
                                                <span>11:03</span>
                                            </div>
                                        </div>
                                        <hr>
                                        <a href="#" class="text-theme">Veer Más</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end: shopping cart-->
                    </li>
                    <li class="d-none d-xl-block d-lg-block ">
                        <div class="p-dropdown">
                            <a class=""><i class="icon-user11"></i></a>
                            <div class="p-dropdown-content">
                                <div class="widget-myaccount">
                                    <div class="avatar">
                                        <img src="images/team/6.jpg">
                                        <span>Nombre</span>
                                        <p class="text-muted">correo Electronico o usuario</p>
                                    </div>
                                    <ul>
                                        <li><a href="#"><i class="icon-user11"></i>Mi perfil</a></li>
                                        <li><a href="#"><i class="icon-clock21"></i>Actividad</a></li>
                                        {{-- <li><a href="#"><i class="icon-settings1"></i>Configuración</a></li> --}}
                                        <li><a href="#"><i class="icon-log-out"></i>Cerrar Sesión</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>

                    
                </ul>
            </div>
            <!--end: Header Extras-->

            <!--Header Extras-->
				<div class="header-extras right">
                    <ul>
                        <li class="d-none d-xl-block d-lg-block ">
                            <a href="{{ route('submit') }}" class="btn btn-rounded btn-light">
                            </span>Enviar Dinero<span>
                            </a>
                        </li>
                        <li class="signIn">
                            <a class="{{ route('submit') }}" href="#"><i class="icon-dollar-sign1"></i></a>
                        </li>
                    </ul>
                </div>
            <!--end: Header Extras-->

        </div>
    </div>
</header>
<!-- end: Header -->