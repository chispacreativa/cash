<nav class="navbar navbar-expand-lg bg-navbar navbar--desktop">
    <div class="container">
        <div class="logo">
            <a href="{{ url('/') }}" class="navbar-brand nav-logo nuxt-link-active">
                <img src="{{ asset('images/logo2.png') }}" alt="" class="img-logo">
            </a>
        </div>
        <div id="navBar" class="navbar-collapse collapse">
            <div class="navbar-nav ml-auto">
                <a class="p-t-10"><button class="btn btn-outline-info" type="button">Enviar Dinero</button></a>
                <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
                    <li class="nav-item dropdown p-2">
                        <a class="nav-link dropdown-toggle " id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-circle p-r-10"></i><span class="p-b-5">Don Pepito</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#"><i class="fa fa-user p-r-10"></i> perfil</a>
                        <a class="dropdown-item" href="#"><i class="fa fa-user-friends p-r-10"></i>Cuentas</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"><i class="fa fa-sign-out-alt p-r-10"></i>Cerrar Sesión</a>
                        </div>
                    </li>
                    
                </ul>

            </div>

        </div>
    </div>
</nav>