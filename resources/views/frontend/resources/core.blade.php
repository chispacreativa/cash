<!DOCTYPE html>
<html lang="es">
<head>
    @include('frontend.resources.head')
    <!-- Google Fonts -->
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css">
    <link rel="stylesheet" href="{{ asset('libs/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/hs.megamenu.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/summernote-lite.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/flatpickr.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/css/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap-select.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/cotizar.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/css/countrySelect.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('libs/css/dd.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('libs/css/component.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/css/component2.css') }}" type="text/css" />

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"> --}}



    <link rel="shortcut icon" href="{{ asset('img/logos/favicon.png') }}">
    @yield('style')


</head>
<body>
    @yield('content')
    @include('frontend.resources.scripts')
    @yield('script')
</body>
</html>