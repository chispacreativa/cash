<div class="list-group">
    <a href="{{ route('dashboard') }}" class="list-group-item list-group-item-action text-center {{ request()->routeIs('dashboard') ? 'active' : '' }}">
        <div class="icon-preview">
            <div class=""><i class="fa fa-home icon-m"></i></div>
            <span class="menu-sub-title">Inicio</span>
        </div>
    </a>
    <a href="#" class="list-group-item list-group-item-action text-center">
        <div class="icon-preview">
            <div class=""><i class="fa fa-piggy-bank icon-m"></i></div>
            <span class="menu-sub-title">Cuentas</span>
        </div>
    </a>
    <a href="{{ route('dashboard.perfil') }}" class="list-group-item list-group-item-action text-center {{ request()->routeIs('dashboard.perfil') ? 'active' : '' }}">
        <div class="icon-preview">
            <div class=""><i class="fa fa-user icon-m"></i></div>
            <span class="menu-sub-title">Perfil</span>
        </div>
    </a>
    <a href="#" class="list-group-item list-group-item-action text-center">
        <div class="icon-preview">
            <div class=""><i class="fa fa-cogs icon-m"></i></div>
            <span class="menu-sub-title">Configurar</span>
        </div>
    </a>
    <a href="#" class="list-group-item list-group-item-action text-center">
        <div class="icon-preview">
            <div class=""><i class="fa fa-info-circle icon-m"></i></div>
            <span class="menu-sub-title">Ayuda</span>
        </div>
    </a>
</div>