@extends('frontend.resources.core')
@section('content')
@include('frontend.resources.header', [ 'relative' => true ])
<div id="intercambio-layout" class="body-inner">
    <div class="container mb-3 mt-2 mt-sm-5">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-10">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <h3 class="text-primary text-center text-uppercase">GRACIAS {{ Auth::user()->name }}</h3>
                <p class="text-center">Tu nro de orden es #{{ $id_intercambio }}</p>
                <div id="intercambio-update" class="mt-4 my-3 p-5 bg-white rounded shadow-sm mb-4">
                    <h4 class="text-primary text-center mb-2">Pago por Transferencia Bancaria</h4>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                            <p class="text-secundary text-center mb-4">Sigue las instrucciones</p>
                            <ul class="list-unstyled list-number m-auto col-lg-10">
                                <li class="mb-4">
                                    <div>
                                        @php
                                            $__exchange_type = $intercambio->getTypeExchange();
                                        @endphp
                                        <h4>Transfiere: <b class="text-info">{{ $__exchange_type['before_symbol'] }} {{ $intercambio->send_amount }}</b></h4>

                                        @if ($intercambio->type_exchange > 41)
                                        <div class="row">
                                            <div class="col-md-6">Wallet</div>
                                            <div class="col-md-6">12345-12345-12345-12345</div>
                                        </div>
                                        @else
                                        @if ($bank && $bank_details)
                                        <div class="row">
                                            <div class="col-md-6 mb-2">Banco</div>
                                            <div class="col-md-6 mb-2">{{ $bank->name }}</div>
                                            <div class="col-md-6 mb-2">Titular</div>
                                            <div class="col-md-6 mb-2">{{ $bank_details->holder }}</div>
                                            <div class="col-md-6 mb-2">Tipo de Cuenta</div>
                                            <div class="col-md-6 mb-2">{{ $bank_details->type()->first()->name}}</div>
                                            <div class="col-md-6 mb-2">Numero de Cuenta</div>
                                            <div class="col-md-6 mb-2"><b>{{ $bank_details->bank_number }}</b></div>
                                            <div class="col-md-6 mb-2">Correo</div>
                                            <div class="col-md-6 mb-2">orden@cambicash.com</div>
                                        </div>
                                        @else
                                        <div class="text-center">
                                            <p>Bank no encontrado</p>
                                        </div>
                                        @endif
                                        @endif
                                        
                                        <hr>

                                        <h4 class="d-none">Recibe: <b class="text-info">{{ $__exchange_type['after_symbol'] }} {{ $intercambio->receive_amount }}</b></h4>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <h4 class="mb-3 text-center">Notifica y adjunta tu comprobante de pago</h4>
                                        <div class="text-center">
                                            <button type="button" class="btn btn-primary" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Notificar Pago</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                            <div class="m-auto col-lg-10">
                                <h4>Envianos una captura de pantalla</h4>
                                <div>
                                    <form method="post" action="{{ route('intercambio.images', $id_intercambio) }}" enctype="multipart/form-data" class="dropzone needsclick" id="dropzone">
                                        @csrf
                                    </form>
                                    <form method="post" action="{{ route('intercambio.view.update', $id_intercambio) }}" id="form-intercambio">
                                        @csrf
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@include('frontend.dashboard.asidebar')
@endsection


@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
<script>
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });
    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
            afterOpen: function () {
                $(this).find('input[type="search"]').focus();
            }
        });

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
    });
    Dropzone.autoDiscover = false;
    $(function() {
        var myDropzone = new Dropzone(".dropzone", {
            language: "es",
            paramName: "file",
            maxFilesize: 4, //mb
            acceptedFiles: ".jpeg,.jpg,.png,.pdf",
            dictDefaultMessage: "Suelte los archivos aquí o haga clic para cargar.",
            //addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
            },
            success: function (file, response) {
                $('#form-intercambio').append('<input type="hidden" name="images[]" value="'+response.id+'">');
            },
            removedfile: function(file) {
                file.previewElement.remove();
                console.log(file);
            },
            init: function () {
            }
        });
    });
</script>
@endsection