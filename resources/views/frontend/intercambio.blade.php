@extends('frontend.resources.core')
@section('content')
    <script>
        __money_rate = @json($money_rate_json);
    </script>
    @include('frontend.resources.header', [ 'relative' => true ])
    <div id="intercambio-layout" class="body-inner form-inter">
        <div class="container mb-3 mt-2 mt-sm-5">
            <div class="row justify-content-md-center">
                <div class="col-md-12 col-lg-9">

                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('intercambio') }}" method="post">
                        @csrf
                        <h3 class="text-primary">Nuevo Intercambio</h3>
                        <div class="box-form-intercambio">
                            <div class="intercambio-container">
                                <div class="intercambio-body">
                                    <div class="form-row mb-4">
                                        <div class="form-group text-center col-md-4 col-lg-5 mb-0">
                                            <select name="select-envio" id="select-envio" class="select-title">
                                                <option value="PEN" {{$data_home['select-envio'] == 'PEN' ? 'selected' : "" }} selected>Envío Soles</option>
                                                <option value="USD" {{$data_home['select-envio'] == 'USD' ? 'selected' : "" }}>Envío Dólares</option>
                                                <option value="BTC" {{$data_home['select-envio'] == 'BTC' ? 'selected' : "" }}>Envío Bitcoin</option>
                                                <option value="COP" {{$data_home['select-envio'] == 'COP' ? 'selected' : "" }}>Envío Pesos</option>
                                                <option value="VES" {{$data_home['select-envio'] == 'VES' ? 'selected' : "" }}>Envío Bolivares</option>
                                            </select>
                                            <div class="input-group mt-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text font-ubuntu" name="simbolo1" style="font-size: 28px">$</span>
                                                </div>
                                                <input type="text" class="form-control" name="cambio01" value="{{ $data_home['cambio01'] ? $data_home['cambio01'] : 10 }}" style="font-size: 28px">
                                                @if ($errors->has('cambio01'))
                                                    <span class="text-danger">{{ $errors->first('cambio01') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-2 d-flex align-items-center mt-3 mb-3 mt-sm-4 mb-sm-4" id="btn-change-int">
                                            <div class="btn-change-int-component m-auto">
                                                <i class="fas fa-exchange-alt"></i>
                                            </div>
                                        </div>
                                        <div class="form-group text-center col-md-4 col-lg-5 mb-0">
                                            <select name="select-recibo" id="select-recibo" class="select-title">
                                                <option value="PEN" {{$data_home['select-recibo'] == 'PEN' ? 'selected' : "" }}>Recibo Soles</option>
                                                <option value="USD" {{$data_home['select-recibo'] == 'USD' ? 'selected' : "" }} selected>Recibo Dólares</option>
                                                <option value="BTC" {{$data_home['select-recibo'] == 'BTC' ? 'selected' : "" }}>Recibo Bitcoin</option>
                                                <option value="COP" {{$data_home['select-recibo'] == 'COP' ? 'selected' : "" }}>Recibo Pesos</option>
                                                <option value="VES" {{$data_home['select-recibo'] == 'VES' ? 'selected' : "" }}>Recibo Bolivares</option>
                                            </select>
                                            <div class="input-group mt-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text font-ubuntu" name="simbolo2" style="font-size: 28px">S/</span>
                                                </div>
                                                <input type="text" class="form-control" name="cambio02" style="font-size: 28px" value="10">
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="text-center">Gana S/20.85 más que en el banco.</div>-->
                                    <br>
                                    <div class="form-group mb-3">
                                        <label for="bank-accounts">¿De qué cuenta envias el dinero?</label>
                                        <select name="cuenta-bank" id="bank-accounts" class="form-control mb-2" required>
                                            <option value="" selected>Selecciona una cuenta...</option>
                                            @foreach ($bank_account as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                        <p class="text-right mt-1 mb-2"><a href="#" class="btn-add-account text-primary" data-type="1">Agregar cuenta <i class="fas fa-plus-circle"></i></a></p>
                                    </div>

                                    <br>

                                    <div class="form-group mb-3">
                                        <label for="bank-accounts">¿En qué cuenta deseas recibir el dinero?</label>
                                        <select name="cuenta-bank-send" id="bank-accounts-send" class="form-control mb-2" required>
                                            <option value="" selected>Selecciona una cuenta...</option>
                                            @foreach ($bank_account_send as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                        <p class="text-right mt-1 mb-2"><a href="#" class="btn-add-account text-primary" data-type="2">Agregar cuenta <i class="fas fa-plus-circle"></i></a></p>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">Inciar Intercambio</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @include('frontend.modals.add_bank_account')
    </div>
    @include('frontend.dashboard.asidebar')

    <!-- Modal -->
    <div class="modal fade" id="myModalHorario" tabindex="-1" role="dialog" aria-labelledby="myModalHorarioTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="myModalHorarioTitle">Cerrado</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Lunes a Viernes de 9:00 a.m. a 6:00 p.m. y Sábado de 9:00 a.m. a 12:30 p.m.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('script')
<script>
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');

        @if(!empty(Session::get('error_code')) && Session::get('error_code') == 1)
        $('#myModalHorario').modal('show');
        @endif
    });

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
            afterOpen: function () {
                $(this).find('input[type="search"]').focus();
            }
        });

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');


    });
</script>
@endsection
