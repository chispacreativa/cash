@extends('frontend.resources.core')

@section('content')
  <!-- ========== HEADER ========== -->
  @include('frontend.resources.header', ['relative' => true])
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    @include('frontend.dashboard.breadcrumb')

    <!-- Content Section -->
    <div class="bg-light">
      <div class="container space-2">
        @if($errors->any())
        <h4>{{$errors->first()}}</h4>
        @endif
        <!-- Update Avatar Form -->
        <form class="js-validate" method="POST" action="{{ route('dashboard.edit-profile') }}" enctype="multipart/form-data">
          <div class="u-lg-avatar mr-3">
            <img style="height: 90px;
            width: 100px;" id="imgProfilePerson" class="img-fluid rounded-circle" src="{{asset('img/'. $detalle->image)}}" alt="Image Description">
          </div>

          <div class="media-body mt-4 mb-4">
            <label class="btn btn-sm btn-primary transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1" for="fileAttachmentBtn">
              {{ __('Upload New Picture') }}
              <input id="inputImgProfilePerson" name="img_avatar" type="file" class="file-attachment-btn__label" accept="image/*" onchange="img_profile('inputImgProfilePerson', 'imgProfilePerson')">
            </label>
            <small class="form-text text-muted">Las imagenes son de formatos jpeg,jpg,png con un maximo de 1M .</small>

            @error('img_avatar')
              <span class="invalid-feedback" role="alert" style="display: block">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
          </div>
        
        <!-- End Update Avatar Form -->

        <!-- Personal Info Form -->
        
          @csrf
          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label id="nameLabel" class="form-label">
                  Nombre
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="text" class="form-control" name="name" value="{{ $data->name }}" placeholder="Nombre Completo" aria-label="Nombre completo" required aria-describedby="nameLabel"
                         data-msg="Campo requerido."
                         data-error-class="u-has-error"
                         data-success-class="u-has-success">
                </div>
              </div>

              
            <!-- End Input -->

            <!-- Input -->
            {{-- <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label id="usernameLabel" class="form-label">
                  Username
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="text" class="form-control" name="username" value="{{ $data->email }}" placeholder="Enter your username" aria-label="Enter your username" required aria-describedby="usernameLabel"
                         data-msg="Please enter your username."
                         data-error-class="u-has-error"
                         data-success-class="u-has-success">
                </div>
              </div>
            </div> --}}
            <!-- End Input -->
          </div>
          <div class="col-sm-6 mb-6">
            <div class="js-form-message">
              <label id="nameLabel" class="form-label">
                Apellido
                <span class="text-danger">*</span>
              </label>

              <div class="form-group">
                <input type="text" class="form-control" name="lastname" value="{{ $data->lastname }}" placeholder="Apellido completo" aria-label="Enter your name" required aria-describedby="nameLabel"
                       data-msg="Campo requerido."
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
        </div>
      </div>

          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label id="emailLabel" class="form-label">
                  Correo electronico
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="email" class="form-control" disabled value="{{ $data->email }}" placeholder="Correo electronico" aria-label="Enter your email address" required aria-describedby="emailLabel"
                         data-msg="Please enter a valid email address."
                         data-error-class="u-has-error"
                         data-success-class="u-has-success">
                  <small class="form-text text-muted">Tu email no puede ser editado.</small>
                </div>
              </div>
            </div>
            <!-- End Input -->

            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label id="locationLabel" class="form-label">
                  Tel&eacute;fono
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="text" class="form-control" name="phone" value="{{ $detalle->phone }}" placeholder="Tel&eacute;fono" aria-label="Enter your location" aria-describedby="locationLabel"
                         data-msg="Campo requerido"
                         data-error-class="u-has-error"
                         data-success-class="u-has-success" required>
                </div>
              </div>
            </div>
            <!-- End Input -->
          </div>



          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label id="emailLabel" class="form-label">
                  Nueva Contraseña
                  
                </label>

                <div class="form-group">
                  <input type="text" class="form-control" name="password" placeholder="Nueva Contraseña" aria-label="Enter your email address" aria-describedby="emailLabel"
                         data-msg="Please enter a valid email address."
                         data-error-class="u-has-error"
                         data-success-class="u-has-success">
                </div>
              </div>
            </div>
            <!-- End Input -->

            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label id="locationLabel" class="form-label">
                  Tipo documento
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <select name="type_document" id="tipodedocumento" class="form-control">
                    <option value="DNI" @if ($detalle->type_document == "DNI") selected @endif>DNI</option>
                    <option value="Carnet de extranjeria" @if ($detalle->type_document == "Carnet de extranjeria") selected @endif>Carnet de extranjeria</option>
                    <option value="PTP" @if ($detalle->type_document == "PTP") selected @endif>PTP</option>
                  </select>
                </div>
              </div>
            </div>
            <!-- End Input -->
          </div>



          <div class="row">
            <!-- Input -->
            <div class="col-sm-12 mb-6">
              <div class="js-form-message">
                <label id="emailLabel" class="form-label">
                  Nr° Documento
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="text" class="form-control" name="number_document" value="{{ $detalle->number_document }}" placeholder="Nr° Documento" aria-label="Enter your email address" aria-describedby="emailLabel"
                         data-msg="Please enter a valid email address."
                         data-error-class="u-has-error"
                         data-success-class="u-has-success">
                </div>
              </div>
            </div>
            <!-- End Input -->
          </div>

         
          <!-- Buttons -->
          <button type="submit" class="btn btn-sm btn-primary transition-3d-hover mr-1">{{__('Guardar')}}</button>
          <!--button type="submit" class="btn btn-sm btn-soft-secondary transition-3d-hover">Cancelar</button-->
          <!-- End Buttons -->

          

          <!-- Buttons -->
          <!--button type="submit" class="btn btn-sm btn-primary transition-3d-hover mr-1">Save Changes</button-->
          <!--button type="submit" class="btn btn-sm btn-soft-secondary transition-3d-hover">Cancel</button-->
          <!-- End Buttons -->
        </form>
        <!-- End Personal Info Form -->

      </div>
    </div>
    <!-- End Content Section -->
  </main>
  
  <!-- ========== FOOTER ========== -->
  @include('frontend.resources.footer')
  <!-- ========== END FOOTER ========== -->

  <!-- ========== SECONDARY CONTENTS ========== -->
  @include('frontend.dashboard.asidebar')
  <!-- ========== END SECONDARY CONTENTS ========== -->
  
  <a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 15, "right": 15 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  @endsection
  
  @section('script')
  {{--<script src="../../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../../assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
  <script src="../../assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="../../assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="../../assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
  <script src="../../assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
  <script src="../../assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="../../assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="../../assets/vendor/summernote/dist/summernote-lite.js"></script>
  <script src="../../assets/vendor/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
  <script src="../../assets/vendor/flatpickr/dist/flatpickr.min.js"></script>

  <!-- JS Front -->
  <script src="../../assets/js/hs.core.js"></script>
  <script src="../../assets/js/components/hs.header.js"></script>
  <script src="../../assets/js/components/hs.unfold.js"></script>
  <script src="../../assets/js/components/hs.malihu-scrollbar.js"></script>
  <script src="../../assets/js/components/hs.focus-state.js"></script>
  <script src="../../assets/js/components/hs.validation.js"></script>
  <script src="../../assets/js/components/hs.summernote-editor.js"></script>
  <script src="../../assets/js/components/hs.range-datepicker.js"></script>
  <script src="../../assets/js/components/hs.svg-injector.js"></script>
  <script src="../../assets/js/components/hs.go-to.js"></script>--}}

  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function () {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of HSMegaMenu component
      $('.js-breadcrumb-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 991.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of unfold component
      $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
        afterOpen: function () {
          $(this).find('input[type="search"]').focus();
        }
      });

      // initialization of malihu scrollbar
      $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of form validation
      $.HSCore.components.HSValidation.init('.js-validate', {
        rules: {
          confirmPassword: {
            equalTo: '#signupPassword'
          }
        }
      });

      // initialization of text editors
     // $.HSCore.components.HSSummernoteEditor.init('.js-summernote-editor');

      // initialization of range datepicker
      $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to');

      
    });

    function img_profile(input, imgProfile) {
      var __file = document.getElementById(input).files[0];
      var __render_img = document.getElementById(imgProfile);
      var reader = new FileReader();
      if (__file) {
        reader.readAsDataURL(__file);
        reader.onloadend = function () {
          __render_img.src = reader.result;
        }
      }
    }
  </script>
  @endsection