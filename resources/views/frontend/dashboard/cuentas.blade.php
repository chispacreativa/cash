
@extends('frontend.resources.core')

@section('content')
@include('frontend.resources.header', ['relative' => true])
  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    @include('frontend.dashboard.breadcrumb')

    <!-- Content Section -->
    <div class="bg-light">
        <div class="container space-2">
          <!-- Title -->
          <div class="mb-3">
            <h2 class="h5 mb-0">{{ __('Cuentas a enviar') }}</h2>
          </div>
          <!-- End Title -->
  
          <!-- Accordion -->
          <div id="paymentDetails" class="accordion mb-9">
            @foreach ($bank_accounts as $item)
            <!-- Card -->
            <div class="card">
              <div class="card-header card-collapse" id="cardHeading{{$item->id}}">
                <h5 class="mb-0">
                  <button class="btn btn-link btn-block card-btn p-3 collapsed" role="button" data-toggle="collapse" data-target="#cardT{{$item->id}}" aria-expanded="false" aria-controls="cardT{{$item->id}}">
                    <span class="row align-items-center">
                      <span class="col-md-6 mb-2 mb-md-0">
                        <span class="media align-items-center">
                          <img class="max-width-9 mr-3" src="{{ asset('img/bank-icon.jpg') }}" alt="Image Description">
                          <span class="media-body">
                            <span class="font-size-1">{{ $item->name }}</span>
                          </span>
                        </span>
                      </span>
                      <span class="col-4 col-md-2 text-md-right">
                        {{-- <span class="btn btn-xs btn-soft-warning btn-pill">Primary</span> --}}
                      </span>
                      <span class="col-6 col-md-3">
                        <span class="d-block font-size-1">{{ $item->_country->name }}</span>
                      </span>
                      <span class="col-2 col-md-1 text-right">
                        <span class="card-btn-arrow">
                          <span class="fas fa-arrow-down small"></span>
                        </span>
                      </span>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="cardT{{$item->id}}" class="collapse" aria-labelledby="cardHeading{{$item->id}}" data-parent="#paymentDetails" style="">
                <div class="card-body px-4">
                  <!-- Card Details -->
                  <div class="row">
                    <div class="col-sm-7 mb-2 mb-sm-0">
                      <h4 class="h6 mb-1">{{ $item->name }} {{ $item->lastname }}</h4>
                      <span class="d-block font-size-1 mb-1">{{ $item->_bank->name }}</span>

                      <h5 class="h6 mb-1">{{ __('Cedula') }}</h5>
                      <p class="h6">{{ $item->tipo_documento }} {{ $item->cedula }}</p>

                      <h5 class="h6 mb-1">{{ __('Correo') }}</h5>
                      <p class="h6">{{ $item->email }}</p>

                      <h5 class="h6 mb-1">{{ __('Direccion') }}</h5>
                      <p class="h6">{{ $item->casa }}</p>
                    </div>
                    <div class="col-sm-5 mb-2 mb-sm-0">
                      <h5 class="h6 mb-0">{{ __('Numero de cuenta') }}</h5>
                      <p class="h6">{{ $item->value }}</p>
                      
                      <h5 class="h6 mb-0">{{ __('Moneda') }}</h5>
                      <p class="h6">{{ $item->_currency->name }}</p>

                      <h5 class="h6 mb-0">{{ __('Tipo de cuenta') }}</h5>
                      <p class="h6">{{ $item->_type->name }}</p>
                      <br>
                      
                      <form id="changeNameForm" class="js-validate d-inline-block" novalidate="novalidate" method="POST" action="{{ route('dashboard.cuentas.edit', ['id' => $item->id]) }}">
                        @csrf
                        <input type="hidden" name="action" value="delete">
                        <button type="submit" class="btn btn-xs btn-soft-secondary mr-1">Delete</button>
                      </form>
                      
                      <a href="{{ route('dashboard.cuentas.edit', ['id' => $item->id])}}" class="btn btn-xs btn-primary">Edit</a>
                    </div>
                  </div>
                  <!-- End Card Details -->
                </div>
              </div>
            </div>
            <!-- End Card -->
            @endforeach
          </div>
          <!-- End Accordion -->
  
          <h3 class="h5 mb-0">{{ __('Cuentas a recibir') }}</h3>
  
          <hr class="mt-2 mb-4">

          <div id="paymentDetails2" class="accordion mb-9">
              @foreach ($bank_accounts_send as $item)
              <!-- Card -->
              <div class="card">
                <div class="card-header card-collapse" id="cardHeading{{$item->id}}">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block card-btn p-3 collapsed" role="button" data-toggle="collapse" data-target="#cardT{{$item->id}}" aria-expanded="false" aria-controls="cardT{{$item->id}}">
                      <span class="row align-items-center">
                        <span class="col-md-6 mb-2 mb-md-0">
                          <span class="media align-items-center">
                            <img class="max-width-9 mr-3" src="{{ asset('img/bank-icon.jpg') }}" alt="Image Description">
                            <span class="media-body">
                              <span class="font-size-1">{{ $item->name }}</span>
                            </span>
                          </span>
                        </span>
                        <span class="col-4 col-md-2 text-md-right">
                          {{-- <span class="btn btn-xs btn-soft-warning btn-pill">Primary</span> --}}
                        </span>
                        <span class="col-6 col-md-3">
                          <span class="d-block font-size-1">{{ $item->_country->name }}</span>
                        </span>
                        <span class="col-2 col-md-1 text-right">
                          <span class="card-btn-arrow">
                            <span class="fas fa-arrow-down small"></span>
                          </span>
                        </span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="cardT{{$item->id}}" class="collapse" aria-labelledby="cardHeading{{$item->id}}" data-parent="#paymentDetails" style="">
                  <div class="card-body px-4">
                    <!-- Card Details -->
                    <div class="row">
                      <div class="col-sm-7 mb-2 mb-sm-0">
                        <h4 class="h6 mb-1">{{ $item->name }} {{ $item->lastname }}</h4>
                        <span class="d-block font-size-1 mb-1">{{ $item->_bank->name }}</span>
  
                        <h5 class="h6 mb-1">{{ __('Cedula') }}</h5>
                        <p class="h6">{{ $item->tipo_documento }} {{ $item->cedula }}</p>
  
                        <h5 class="h6 mb-1">{{ __('Correo') }}</h5>
                        <p class="h6">{{ $item->email }}</p>
  
                        <h5 class="h6 mb-1">{{ __('Direccion') }}</h5>
                        <p class="h6">{{ $item->casa }}</p>
                      </div>
                      <div class="col-sm-5 mb-2 mb-sm-0">
                        <h5 class="h6 mb-0">{{ __('Numero de cuenta') }}</h5>
                        <p class="h6">{{ $item->value }}</p>
                        
                        <h5 class="h6 mb-0">{{ __('Moneda') }}</h5>
                        <p class="h6">{{ $item->_currency->name }}</p>
  
                        <h5 class="h6 mb-0">{{ __('Tipo de cuenta') }}</h5>
                        <p class="h6">{{ $item->_type->name }}</p>
                        <br>
                        
                        <form id="changeNameForm" class="js-validate d-inline-block" novalidate="novalidate" method="POST" action="{{ route('dashboard.cuentas.edit', ['id' => $item->id]) }}">
                          @csrf
                          <input type="hidden" name="action" value="delete">
                          <button type="submit" class="btn btn-xs btn-soft-secondary mr-1">Delete</button>
                        </form>
                        
                        <a href="{{ route('dashboard.cuentas.edit', ['id' => $item->id])}}" class="btn btn-xs btn-primary">Edit</a>
                      </div>
                    </div>
                    <!-- End Card Details -->
                  </div>
                </div>
              </div>
              <!-- End Card -->
              @endforeach
          </div>
  
          <!-- Add Payment -->
          <!---<div class="row align-items-center">

            <div class="col-md-6">
              <h4 class="h6 mb-0">.....</h4>
              <p class="mb-2">....</p>
              <a href="#addPaymentModal" data-modal-target="#addPaymentModal">Add a Card</a>
            </div>
            <div class="col-md-6 text-md-right">
              <span class="d-inline-block bg-white border rounded p-1">
                <img class="max-width-9" src="{{ asset('img/100x60/img1.jpg') }}" alt="Image Description">
              </span>
              <span class="d-inline-block bg-white border rounded p-1">
                <img class="max-width-9" src="{{ asset('img/100x60/img2.jpg') }}" alt="Image Description">
              </span>
              <span class="d-inline-block bg-white border rounded p-1">
                <img class="max-width-9" src="{{ asset('img/100x60/img3.jpg') }}" alt="Image Description">
              </span>
            </div>
          </div>-->
          <!-- End Add Payment -->
        </div>
      </div>
      <!-- End Content Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  @include('frontend.resources.footer')
  <!-- ========== END FOOTER ========== -->

  <!-- ========== SECONDARY CONTENTS ========== -->
  @include('frontend.dashboard.asidebar')
  <!-- ========== END SECONDARY CONTENTS ========== -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 15, "right": 15 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->
  @endsection

  @section('script')
  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function () {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of HSMegaMenu component
      $('.js-breadcrumb-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 991.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of unfold component
      $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
        afterOpen: function () {
          $(this).find('input[type="search"]').focus();
        }
      });

      // initialization of malihu scrollbar
      $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of range datepicker
      $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');

      // initialization of datatables
      $.HSCore.components.HSDatatables.init('.js-datatable');

      // initialization of select picker
      $.HSCore.components.HSSelectPicker.init('.js-select');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to', {
        "order": [[ 3, "desc" ]]
      });
    });
  </script>
@endsection