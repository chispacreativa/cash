<aside id="sidebarContent" class="u-sidebar" aria-labelledby="sidebarNavToggler">
  <div class="u-sidebar__scroller">
    <div class="u-sidebar__container">
      <div class="u-sidebar--account__footer-offset">
        <!-- Toggle Button -->
        <div class="d-flex justify-content-between align-items-center pt-4 px-7">
          <h3 class="h6 mb-0">{{__('My Account')}}</h3>

          <button type="button" class="close ml-auto"
                  aria-controls="sidebarContent"
                  aria-haspopup="true"
                  aria-expanded="false"
                  data-unfold-event="click"
                  data-unfold-hide-on-scroll="false"
                  data-unfold-target="#sidebarContent"
                  data-unfold-type="css-animation"
                  data-unfold-animation-in="fadeInRight"
                  data-unfold-animation-out="fadeOutRight"
                  data-unfold-duration="500">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <!-- End Toggle Button -->

        <!-- Content -->
        <div class="js-scrollbar u-sidebar__body">
          <!-- Holder Info -->
          <header class="d-flex align-items-center u-sidebar--account__holder mt-3">
            <div class="position-relative">
              @php
                      $details = App\Model\UsuarioDetalle::where("id_user", Auth::user()->id)->first();
                  @endphp
                
              <img style="width: 72px; height: 74px;" class="u-sidebar--account__holder-img" src="{{asset('img/'. $details->image)}}" alt="">

              <span class="badge badge-xs badge-outline-success badge-pos rounded-circle"></span>
            </div>
            <div class="ml-3">
              <span class="font-weight-semi-bold">{{ Auth::user()->name }} <span class="badge badge-success ml-1">Pro</span></span>
              <span class="u-sidebar--account__holder-text">---</span>
            </div>

            <!-- Settings -->
            <div class="btn-group position-relative ml-auto mb-auto">
              <a id="sidebar-account-settings-invoker" class="btn btn-xs btn-icon btn-text-secondary rounded" href="javascript:;" role="button"
                      aria-controls="sidebar-account-settings"
                      aria-haspopup="true"
                      aria-expanded="false"
                      data-toggle="dropdown"
                      data-unfold-event="click"
                      data-unfold-target="#sidebar-account-settings"
                      data-unfold-type="css-animation"
                      data-unfold-duration="300"
                      data-unfold-delay="300"
                      data-unfold-animation-in="slideInUp"
                      data-unfold-animation-out="fadeOut">
                <span class="fas fa-ellipsis-v btn-icon__inner"></span>
              </a>

              <div id="sidebar-account-settings" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="sidebar-account-settings-invoker">
                {{-- <a class="dropdown-item" href="#">{{__('Settings')}}</a> --}}
                
                
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{__('Sign Out')}}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <div class="dropdown-divider"></div>
              </div>
            </div>
            <!-- End Settings -->
          </header>
          <!-- End Holder Info -->

          <div class="u-sidebar__content--account">
            <!-- List Links -->
            <ul class="list-unstyled u-sidebar--account__list">
              {{-- <li class="u-sidebar--account__list-item">
                <a class="u-sidebar--account__list-link" href="{{route('dashboard')}}">
                  <span class="fas fa-home u-sidebar--account__list-icon mr-2"></span>
                  {{__('Dashboard')}}
                </a>
              </li> --}}
              
              <li class="u-sidebar--account__list-item">
                <a class="u-sidebar--account__list-link" href="{{route('dashboard.edit-profile')}}">
                  <span class="fas fa-user-circle u-sidebar--account__list-icon mr-2"></span>
                  {{__('Profile')}}
                </a>
              </li>
              <li class="u-sidebar--account__list-item">
                <a class="u-sidebar--account__list-link" href="{{route('dashboard.activity')}}">
                  <span class="fas fa-exchange-alt u-sidebar--account__list-icon mr-2"></span>
                  {{__('Activity')}}
                </a>
              </li>
            </ul>
            <!-- End List Links -->

            <div class="u-sidebar--account__list-divider"></div>

            <!-- List Links -->
            {{-- <ul class="list-unstyled u-sidebar--account__list">
              <li class="u-sidebar--account__list-item">
                <a class="u-sidebar--account__list-link" href="#">
                  <span class="fas fa-user-plus u-sidebar--account__list-icon mr-2"></span>
                  {{__('Invite friends')}}
                </a>
              </li>
            </ul> --}}
            
            <!-- End List Links -->
          </div>
        </div>
      </div>

      <!-- Footer -->
      <footer id="SVGwaveWithDots" class="svg-preloader u-sidebar__footer u-sidebar__footer--account">
        <ul class="list-inline mb-0">
          <li class="list-inline-item pr-3">
            <a class="u-sidebar__footer--account__text" href="{{route('privacidad')}}">{{__('Privacidad')}}</a>
          </li>
          <li class="list-inline-item pr-3">
            <a class="u-sidebar__footer--account__text" href="{{route('terminos')}}">{{__('Terminos')}}</a>
          </li>
          <li class="list-inline-item">
            <a class="u-sidebar__footer--account__text" href="#">
              <i class="fas fa-info-circle"></i>
            </a>
          </li>
        </ul>

        <!-- SVG Background Shape -->
        <div class="position-absolute right-0 bottom-0 left-0">
          <img class="js-svg-injector" src="{{asset('svg/components/wave-bottom-with-dots.svg')}}" alt="" data-parent="#SVGwaveWithDots">
        </div>
        <!-- End SVG Background Shape -->
      </footer>
      <!-- End Footer -->
    </div>
  </div>
</aside>