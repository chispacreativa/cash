
@extends('frontend.resources.core')

@section('content')
@include('frontend.resources.header', ['relative' => true])
  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    @include('frontend.dashboard.breadcrumb')

    <!-- Content Section -->
    <div class="bg-light">
        <div class="container space-2">
          <!-- Title -->
          <div class="mb-3">
            <h2 class="h5 mb-0">{{ __('Editar cuenta bancaria') }}</h2>
          </div>
          <!-- End Title -->

          <form id="changeNameForm" class="js-validate" novalidate="novalidate" method="POST" action="{{ route('dashboard.cuentas.edit', ['id' => $bank_account->id]) }}">
            @csrf
            <input type="hidden" name="action" value="update">
            <div class="form-row mb-2">
                <div class="form-group col-md-6">
                    <label for="nombre-cuenta" class="xs-only">Nombre Completo</label>
                    <input type="text" name="nombre_cuenta" class="form-control" id="nombre-cuenta" placeholder="Nombre Completo" required value="{{ $bank_account->name }}">
                </div>

                <div class="form-group col-md-6">
                    <label for="apellido-cuenta" class="xs-only">Apellido Completo</label>
                    <input type="text" name="apellido_cuenta" class="form-control" id="apellido-cuenta" placeholder="Apellido Completo" required value="{{ $bank_account->lastname }}">
                </div>
            </div>

            <div class="form-row mb-2">
                
                <div class="form-group col-md-6">
                    <label for="nombre">Tipo de documento</label>
                    <div class="form-row">
                      <div class="col">
                        <select name="tipo_documento" id="tipodedocumento" class="form-control">
                          <option value="DNI" @if ($bank_account->tipo_documento == "DNI") selected @endif>DNI</option>
                          <option value="Carnet de extranjeria" @if ($bank_account->tipo_documento == "Carnet de extranjeria") selected @endif>Carnet de extranjeria</option>
                          <option value="PTP" @if ($bank_account->tipo_documento == "PTP") selected @endif>PTP</option>
                        </select>
                      </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="cedula-cuenta" class="xs-only">Cedula</label>
                    <input type="text" name="cedula" class="form-control" id="cedula-cuenta" placeholder="Cedula" required  value="{{ $bank_account->cedula }}">
                </div>
            </div>

            <div class="form-row mb-2">
                <div class="form-group col-md-6">
                    <label for="email-cuenta" class="xs-only">Correo Electronico</label>
                    <input type="text" name="email" class="form-control" id="email-cuenta" placeholder="Correo Electronico" required  value="{{ $bank_account->email }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="casa-cuenta" class="xs-only">Direccion</label>
                    <input type="text" name="casa" class="form-control" id="casa-cuenta" placeholder="Direccion" required  value="{{ $bank_account->casa }}">
                </div>
            </div>


            <div class="form-row mb-2">
                <div class="form-group col-md-6">
                    <label for="nombre-cuenta" class="xs-only">Pais</label>
                    <select id="pais" name="pais_cuenta" class="form-control mb-2">
                        <option value="" selected>Selecciona pais...</option>
                        @foreach ($country as $key => $value)
                            <option value="{{ $value->id }}" {{ $value->id == $bank_account->country_id ? 'selected': '' }}>{{ $value->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="tipo-moneda" class="xs-only">Tipo de moneda</label>
                    <select id="tipo-moneda" name="tipo_moneda" class="form-control mb-2">
                        <option value="" selected>Tipo de moneda</option>
                        @foreach ($currencies as $key => $value)
                                    <option value="{{ $value['id'] }}" {{  $value['id'] == $bank_account->type_currency_id ? 'selected': '' }}>{{  $value['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="form-group col-md-6">
                    <label for="entidad-bancaria" class="xs-only">Entidad bancaria</label>
                    <select id="entidad-bancaria" name="entidad_bancaria" class="form-control mb-2">
                        <option value="" selected>Selecciona una entidad bancaria...</option>
                        @foreach ($banks as $key => $value)
                                    <option value="{{ $value['id'] }}" {{  $value['id'] == $bank_account->bank_id ? 'selected': '' }}>{{  $value['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="tipo-cuenta" class="xs-only">Tipo de cuenta</label>
                    <select id="tipo-cuenta" name="tipo_cuenta" class="form-control mb-2">
                        <option value="" selected>Tipo de cuenta</option>
                        @foreach ($bank_types as $key => $value)
                                    <option value="{{ $value['id'] }}" {{  $value['id'] == $bank_account->type_account_id ? 'selected': '' }}>{{  $value['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row mb-2">
                <div class="form-group col-md-12">
                    <label for="numero-cuenta" class="xs-only">Numero de cuenta</label>
                    <input type="text" name="numero_cuenta" class="form-control" id="numero-cuenta" placeholder="Numero de cuenta" required value="{{ $bank_account->value }}">
                </div>
            </div>
            
            {{-- 
            <div class="js-form-message mb-6">
              <label class="form-label">{{ __('Name') }}</label>
  
              <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Enter your current Name" aria-label="Enter your current Name" required="" data-msg="Name does not match." data-error-class="u-has-error" data-success-class="u-has-success" value="{{ $bank_account->name }}">
              </div>
            </div>
  
            <div class="mb-6">
              <div class="js-form-message">
                <label class="form-label">{{ __('Account') }}</label>
  
                <div class="form-group">
                    <input type="text" class="form-control" name="value" placeholder="Enter your Account" aria-label="Enter your Account" required="" data-msg="Please enter your Account." data-error-class="u-has-error" data-success-class="u-has-success" value="{{ $bank_account->value }}">
                </div>
              </div>
            </div> --}}
            
  
            
  
              <button type="submit" name="btn-save" class="btn btn-sm btn-primary transition-3d-hover mr-1">{{ __('Save Account') }}</button>
              <a href="{{ route('dashboard.cuentas') }}" class="btn btn-sm btn-soft-secondary transition-3d-hover">{{ __('Cancel') }}</a>
            </div>
          </form>

  
          
        </div>
      </div>
      <!-- End Content Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  @include('frontend.resources.footer')
  <!-- ========== END FOOTER ========== -->

  <!-- ========== SECONDARY CONTENTS ========== -->
  @include('frontend.dashboard.asidebar')
  <!-- ========== END SECONDARY CONTENTS ========== -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 15, "right": 15 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->
  @endsection

  @section('script')
  <!-- JS Plugins Init. -->
  <script>
$(function() {
    
    $('#pais').change(function(){
        axios.post(
            '/intercambio/tipomoneda',
            {
                'pais' : $("#pais").val()
            }
        ).then(function (response) {
            if (response.status == 200) {
                if (response.data.error) {
                    
                } else {
                    var n='<option value="" selected>Tipo de moneda</option>';
                    response.data.data.forEach(function(t){
                        n+='<option value="'.concat(t.id,'">').concat(t.name,"</option>")
                    });
                    $('#tipo-moneda').empty().html(n);
                }
            }
        });
    });


    $('#tipo-moneda').change(function(){
        axios.post(
            '/intercambio/bank',
            {
                'type_money' : $("#tipo-moneda").val(),
                'pais' : $("#pais").val()
            }
        ).then(function (response) {
            if (response.status == 200) {
                if (response.data.error) {
                    
                } else {
                    var e='<option value="" selected>Selecciona una entidad bancaria...</option>';
                    response.data.banks.forEach(function(t){
                        e+='<option value="'.concat(t.id,'">').concat(t.name,"</option>")
                    });
                    $('#entidad-bancaria').empty().html(e);


                    var i='<option value="" selected>Tipo de cuenta</option>';
                    response.data.bank_types.forEach(function(t){
                        i+='<option value="'.concat(t.id,'">').concat(t.name,"</option>")
                    });
                    $('#tipo-cuenta').empty().html(i);
                }
            }
        });
    });
});

    $(window).on('load', function () {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of HSMegaMenu component
      $('.js-breadcrumb-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 991.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of unfold component
      $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
        afterOpen: function () {
          $(this).find('input[type="search"]').focus();
        }
      });

      // initialization of malihu scrollbar
      $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of range datepicker
      $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');

      // initialization of datatables
      $.HSCore.components.HSDatatables.init('.js-datatable');

      // initialization of select picker
      $.HSCore.components.HSSelectPicker.init('.js-select');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to', {
        "order": [[ 3, "desc" ]]
      });


    });
  </script>
@endsection