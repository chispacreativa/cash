
@extends('frontend.resources.core')

@section('content')
@include('frontend.resources.header', ['relative' => true])
  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    @include('frontend.dashboard.breadcrumb')

    <!-- Content Section -->
    <div class="bg-light">
      <div class="container space-2">
        <div class="card">
          <div class="card-header py-4 px-0 mx-4">
            <!-- Activity Menu -->
            <div class="row justify-content-sm-between align-items-sm-center">
              <div class="col-md-5 col-lg-4 mb-2 mb-md-0">
                <!-- Datepicker -->
                {{-- <div id="datepickerWrapper" class="js-focus-state u-datepicker w-auto input-group input-group-sm">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <span class="fas fa-calendar"></span>
                    </span>
                  </div>
                  <input type="text" class="js-range-datepicker form-control bg-white rounded-right"
                         data-rp-wrapper="#datepickerWrapper"
                         data-rp-type="range"
                         data-rp-date-format="d M Y"
                         data-rp-default-date='["05 Jul 2018", "19 Jul 2018"]'
                         data-rp-is-disable-future-dates="true">
                </div> --}}
                <!-- End Datepicker -->
              </div>

              <div class="col-md-6">
                <div class="d-flex">
                  <div class="mr-2">
                    <!-- Select -->
                    <select id="datatableEntries" class="js-select selectpicker dropdown-select" data-width="fit" data-style="btn-soft-primary btn-sm">
                      <option value="6">6 entries</option>
                      <option value="12" selected>12 entries</option>
                      <option value="18">18 entries</option>
                      <option value="24">24 entries</option>
                    </select>
                    <!-- End Select -->
                  </div>

                  <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="searchActivities">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearch" type="email" class="form-control" placeholder="{{ __('Search exchanges') }}" aria-label="{{ __('Search exchanges') }}" aria-describedby="searchActivities">
                  </div>
                  <!-- End Search -->
                </div>
              </div>
            </div>
            <!-- End Activity Menu -->
          </div>

          <div class="card-body p-4">
            <!-- Activity Table -->
            <div class="table-responsive-md u-datatable">
              <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5"
                     data-dt-info="#datatableInfo"
                     data-dt-search="#datatableSearch"
                     data-dt-entries="#datatableEntries"
                     data-dt-page-length="12"
                     data-dt-ordering="false"
                     data-dt-is-responsive="true"
                     data-dt-is-show-paging="true"
                     data-dt-details-invoker=".js-datatabale-details"
                     data-dt-select-all-control="#invoiceToggleAllCheckbox"

                     data-dt-pagination="datatablePagination"
                     data-dt-pagination-classes="pagination mb-0"
                     data-dt-pagination-items-classes="page-item"
                     data-dt-pagination-links-classes="page-link"

                     data-dt-pagination-next-classes="page-item"
                     data-dt-pagination-next-link-classes="page-link"
                     data-dt-pagination-next-link-markup='<span aria-hidden="true">&raquo;</span>'

                     data-dt-pagination-prev-classes="page-item"
                     data-dt-pagination-prev-link-classes="page-link"
                     data-dt-pagination-prev-link-markup='<span aria-hidden="true">&laquo;</span>'>
                <thead>
                  <tr class="text-uppercase font-size-1">
                    <th scope="col">
                      <div class="custom-control custom-checkbox d-flex align-items-center">
                        <input type="checkbox" class="custom-control-input" id="invoiceToggleAllCheckbox">
                        <label class="custom-control-label" for="invoiceToggleAllCheckbox">
                          <span class="text-hide">Checkbox</span>
                        </label>
                      </div>
                    </th>
                    <th scope="col" class="font-weight-medium">
                      <div class="d-flex justify-content-between align-items-center">
                        Id
                        <div class="ml-2">
                          <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                          <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                        </div>
                      </div>
                    </th>
                    <th scope="col" class="font-weight-medium">
                      <div class="d-flex justify-content-between align-items-center">
                        {{ __('Type Exchange') }}
                        <div class="ml-2">
                          <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                          <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                        </div>
                      </div>
                    </th>
                    <th scope="col" class="font-weight-medium">
                      <div class="d-flex justify-content-between align-items-center">
                        {{ __('Amount') }}
                        <div class="ml-2">
                          <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                          <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                        </div>
                      </div>
                    </th>
                    <th scope="col" class="font-weight-medium">
                      <div class="d-flex justify-content-between align-items-center">
                        {{ __('Date') }}
                        <div class="ml-2">
                          <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                          <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                        </div>
                      </div>
                    </th>
                    <th scope="col" class="font-weight-medium">
                      <div class="d-flex justify-content-between align-items-center">
                        {{ __('Status') }}
                        <div class="ml-2">
                          <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                          <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                        </div>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody class="font-size-1">
                  @foreach ($transactions as $item)
                  @php
                    $__exchange_type = $item->getTypeExchange();
                    $end = $item->created_at;
                        $monthdate =  Carbon\Carbon::parse($item->created_at)->addMinute(90);
                        $dateDiff =  Carbon\Carbon::now()->diffInMinutes($monthdate,false);
                        if ($dateDiff > 0 ||  $item->status == 2 ||  $item->status == 1 || $item->status == 3) {
                          # code...
                        } else {
                          $item->status = 4;
                        }
                  @endphp
                  @php
                    $__label = '';
                    $__value = '';
                    switch ($item->status) {
                        case '0':
                            $__label = 'text-danger';
                            $__value = 'SIN CONFIRMAR';
                            break;
                        case '1':
                            $__label = 'text-warning';
                            $__value = 'EN PROCESO';
                            break;
                        case '2':
                            $__label = 'text-danger';
                            $__value = 'RECHAZADO';
                            break;
                        case '3':
                            $__label = 'text-success';
                            $__value = 'ENVIADO';
                            break;
                        case '4':
                            $__label = 'text-danger';
                            $__value = 'EXCEDIÓ TIEMPO';
                            break;
                        default:
                            $__label = 'text-danger';
                            $__value = '-';
                            break;
                    }
                @endphp
                  <tr class="js-datatabale-details" data-details='
                    <div class="border rounded p-5">
                      <h4 class="h3">Orden #{{ $item->id }}</h4>

                      <div class="row mb-2">
                        <div class="col-4">
                          <span class="text-secondary">{{ __('Date') }}:</span>
                          <span class="font-weight-medium">{{ $item->created_at->format('d M Y') }}</span>
                          <br />
                          <span class="text-secondary">Tipo:</span>
                          <span class="font-weight-medium">{{ $__exchange_type['before'] . ' ' . __('to') . ' ' . $__exchange_type['after'] }}</span>
                        </div>
                        <div class="col-2">
                          
                        </div>
                        <div class="col-3">
                          <span class="text-secondary">Identificador:</span>
                          <span class="font-weight-medium">#{{ $item->id }}</span>
                        </div>
                        <div class="col-3">
                          <span class="text-secondary">Estado:</span>
                          <span class="font-weight-medium"><u>{{ $__value }}</u></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                          <h5 class="text-dark font-size-1 text-uppercase">Cliente:</h5>
                          <address class="text-secondary">
                            <h6 class="h5 text-dark text-capitalize">{{ $item->user()->name }}</h6>
                            {{ $item->user()->email }}
                          </address>
                        </div>

                        <div class="col-sm-6">
                          <h5 class="text-dark font-size-1 text-uppercase">Para:</h5>
                          <ul class="list-unstyled mb-0">
                            <li class="mb-2">
                              <span class="text-secondary">Nombre:</span>
                              <span class="font-weight-medium">{{ $item->bankAccount()->name }}</span>
                            </li>
                            <li class="mb-2">
                              <span class="text-secondary">Banco:</span>
                              <span class="font-weight-medium">{{ $item->bankAccount()->bank()->name }}</span>
                            </li>
                            <li class="mb-2">
                              <span class="text-secondary">Numero de Cuenta:</span>
                              <span class="font-weight-medium">{{ $item->bankAccount()->value }}</span>
                            </li>
                            <li class="mb-2">
                              <span class="text-secondary">Tipo de Cuenta:</span>
                              <span class="font-weight-medium">{{ $item->bankAccount()->type()->name }}</span>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div class="row justify-content-end mb-4">
                        <div class="col-sm-6">
                          <hr class="my-4">

                          <h5 class="text-dark font-size-1 text-uppercase">Detalles de la transacción:</h5>
                          <ul class="list-unstyled mb-0">
                            <li class="d-flex justify-content-between align-items-center mb-2">
                              <span class="text-secondary">Monto Origen</span>
                              <span class="font-weight-medium">{{ $__exchange_type['before_symbol'] }} {{ $item->send_amount }}</span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center mb-2">
                              <span class="text-secondary">Monto Final</span>
                              <span class="text-primary font-weight-medium">{{ $__exchange_type['after_symbol'] }} {{ $item->receive_amount }}</span>
                            </li>
                          </ul>
                        </div>
                      </div>

                      @if($item->status == 3)
                      <ul class="list-inline mb-0">
                        <li class="list-inline-item u-ver-divider pr-3 mr-3">
                          <a href="#">
                            <span class="fas fa-file-word text-secondary mr-1"></span>
                            Descargar factura
                          </a>
                        </li>
                        <li class="list-inline-item">
                          <a href="#">
                            <span class="fas fa-print text-secondary mr-1"></span>
                            Imprimir detalles
                          </a>
                        </li>
                      </ul>
                      @elseif ($item->status == 0)
                      @if ($dateDiff == 0)
                          La operacion a sido cancelada, debido al tiempo estimado para su confirmaci&oacute;n
                          @else
                          <div>
                            <a href="{{route('intercambio.view', $item->id)}}" class="btn btn-primary btn-sm">Confirmar Transferencia</a>
                          </div>
                      @endif
                        
                      @endif

                    </div>'>
                    <td class="align-middle">
                      <div class="custom-control custom-checkbox d-flex align-items-center">
                        <input type="checkbox" class="custom-control-input" id="invoiceCheckbox01">
                        <label class="custom-control-label" for="invoiceCheckbox01">
                          <span class="text-hide">Checkbox</span>
                        </label>
                      </div>
                    </td>
                    <td class="align-middle text-secondary font-weight-normal u-datatable__trigger-icon">{{ $item->id }}</td>
                    <td class="align-middle">
                      <div class="media align-items-center">
                        <span class="btn btn-sm btn-icon btn-soft-success rounded-circle mr-2">
                          <span class="btn-icon__inner font-weight-medium">S</span>
                        </span>
                        
                        <span>{{ $__exchange_type['before'] . ' ' . __('to') . ' ' . $__exchange_type['after'] }}</span>
                      </div>
                    </td>
                    <td class="align-middle text-primary">{{ $__exchange_type['after_symbol'] }} {{ number_format($item->receive_amount, 2) }}</td>
                   
                    <td class="align-middle text-secondary">
                      
                      @if ($dateDiff > 0)
                        Quedan {{ $dateDiff }} minutos para la operacion
                        @else
                        {{ $item->created_at }}
                      @endif
                      
                      
                    
                    </td>
                    <td class="align-middle {{ $__label }}">{{ $__value }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- End Activity Table -->

            <!-- Pagination -->
            <div class="d-flex align-items-center">
              <nav id="datatablePagination" aria-label="Activity pagination"></nav>

              <small id="datatableInfo" class="text-secondary ml-auto"></small>
            </div>
            <!-- End Pagination -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Content Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  @include('frontend.resources.footer')
  <!-- ========== END FOOTER ========== -->

  <!-- ========== SECONDARY CONTENTS ========== -->
  @include('frontend.dashboard.asidebar')
  <!-- ========== END SECONDARY CONTENTS ========== -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 15, "right": 15 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->
  @endsection

  @section('script')
  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function () {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of HSMegaMenu component
      $('.js-breadcrumb-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 991.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of unfold component
      $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
        afterOpen: function () {
          $(this).find('input[type="search"]').focus();
        }
      });

      // initialization of malihu scrollbar
      $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of range datepicker
      $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');

      // initialization of datatables
      $.HSCore.components.HSDatatables.init('.js-datatable');

      // initialization of select picker
      $.HSCore.components.HSSelectPicker.init('.js-select');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to', {
        "order": [[ 3, "desc" ]]
      });
    });
  </script>
  @endsection