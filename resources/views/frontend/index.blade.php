@extends('frontend.resources.core')
@section('content')
<script>
    __money_rate = @json($money_rate_json);
</script>
<!-- Skippy -->
<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
    <div class="container">
        <span class="u-skiplink-text">Skip to main content</span>
    </div>
</a>
<!-- End Skippy -->

@include('frontend.resources.header')

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main">
    <!-- Hero Section -->

    <div class="fondo_perso">
    <div class="d-lg-flex position-relative">
        <div class="container-fluid d-lg-flex align-items-lg-center space-top-md-5 space-lg-0 min-height-lg-100vh">
            <!-- Content -->
            <div class="row w-100 justify-content-between align-items-center m-auto">
                <div class="col-lg-5 mb-4">
                    <h1 class="display-9 font-size-md-down-5 mb-3">Envía y recibe dinero:</h1>
                    <p>
                        <span class="text-primary">
                          <h1 style="color:#13be49;">  <strong class="u-text-animation u-text-animation--typing"></strong></h1>
                        </span>
                        <br>
                        Envía y recibe dinero desde donde estés, más rápido que una transferencia bancaria y con las mejores tarifas del mercado.
                        
                        
                    </p>

                    <div class="d-flex align-items-center flex-wrap">
                        <a class="btn btn-soft-primary btn-wide transition-3d-hover" id="comenzar-home" href="{{ route('intercambio') }}">{{__('Start')}}</a>

                        <!-- Fancybox -->
                        <a class="js-fancybox media align-items-center u-media-player min-width-21 ml-3" href="javascript:;" data-src="https://youtu.be/p_Z-DRL_qMQ" data-speed="700" data-animate-in="zoomIn" data-animate-out="zoomOut" data-caption="">
                            <span class="u-media-player__icon u-media-player__icon--success mr-3">
                                <span class="fas fa-play u-media-player__icon-inner"></span>
                            </span>
                            <span class="media-body">Ver video</span>
                        </a>
                        <!-- End Fancybox -->
                    </div>
                </div>
                {{-- Intercambio Form --}}
                <div class="col-lg-6 cotizador-form" style="background-image: url({{asset('svg/components/soft-triangle-shape.svg')}}); background-size: cover;">
                    <form id="form-home" action="{{ route('intercambio') }}" method="get" class="form-intercambio">
                        <div class="box-form-intercambio">
                            <div class="intercambio-container">
                                <div class="intercambio-body">
                                    <div class="form-row mb-4">
                                        <div class="form-group text-center col-md-4 col-lg-5 mb-0">
                                            <select name="select-envio" id="select-envio" class="select-title mb-4" style="width: 100%;">
                                                <option value="PEN" data-image="https://cambicash.com/images/banderas/per.png" data-title="Envío Soles" selected>Envío PEN</option> 

                                                <option value="USD" data-image="https://cambicash.com/images/banderas/usa.png" data-title="Envío Dólares">Envío USD</option> 
                                                <option value="BTC" data-image="https://cambicash.com/images/banderas/bitcoin.png" data-title="Envío Bitcoin">Envío BTC</option> 
                                                <option value="COP" data-image="https://cambicash.com/images/banderas/col.png" data-title="Envío Pesos">Envío COP</option> 
                                                <option value="VES" data-image="https://cambicash.com/images/banderas/ven.png" data-title="Envío Bolivares">Envío VES</option>

                                            </select>
                                            <div class="input-group mt-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text gradient-half-info-v1 text-white font-ubuntu" name="simbolo1" style="font-size: 28px">$</span>
                                                </div>
                                                <input type="text" class="form-control" name="cambio01" value="10" style="font-size: 28px">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-2 d-flex align-items-center mt-sm-4 mb-sm-4 cirlce-content">
                                            <div id="button-circle" class="btn-change-int-component m-auto">
                                                <i class="fas fa-exchange-alt"></i>
                                            </div>
                                        </div>
                                        <div class="form-group text-center col-md-4 col-lg-5 mb-0">
                                            <select name="select-recibo" id="select-recibo" class="select-title mb-4" style="width: 100%;">
                                                <option value="PEN" data-image="https://cambicash.com/images/banderas/per.png" data-title="Recibo Soles">Recibo PEN</option> 
                                                <option value="USD" data-image="https://cambicash.com/images/banderas/usa.png" data-title="Recibo Dólares"selected>Recibo USD</option> 
                                                <option value="BTC" data-image="https://cambicash.com/images/banderas/bitcoin.png" data-title="Recibo Bitcoin">Recibo BTC</option> 
                                                <option value="COP" data-image="https://cambicash.com/images/banderas/col.png" data-title="Recibo Pesos">Recibo COP</option> 
                                                <option value="VES" data-image="https://cambicash.com/images/banderas/ven.png" data-title="Recibo Bolivares">Recibo VES</option> 

                                            </select>
                                            <div class="input-group mt-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text gradient-half-info-v1 text-white font-ubuntu" name="simbolo2" style="font-size: 28px">$</span>
                                                </div>
                                                <input type="text" class="form-control" name="cambio02" style="font-size: 28px">
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="text-center">Gana S/20.85 más que en el banco.</div>-->
                                    <br>
                                    <div class="text-center">
                                        <button type="submit" class="cotizadorButtonToSubmit-g66" >Iniciar Intercambio</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End Content -->

            <!-- SVG Background -->
            <!--div id="SVGMainHero"
                class="col-lg-9 col-xl-7 svg-preloader d-none d-lg-block position-absolute top-0 right-0 pr-0"
                style="margin-top: 105.6px;">
                <figure class="ie-main-hero">
                  <img class="js-svg-injector" src="svg/illustrations/main-hero.svg" alt="Image Description" data-img-paths='[{"targetId": "#SVGMainHeroImg1", "newPath": "img/750x750/img2.jpg"}]' data-parent="#SVGMainHero">
                </figure>
            </div-->
            <!-- End SVG Background -->
        </div>
    </div>
    </div>

    
    <!-- End Hero Section -->

    <!-- Section Adicional-->

    <div class="container">

        <div class="row">

            <div class="container space-2 space-top-md-5 space-top-lg-4">
                <div class="w-lg-80 text-center mx-lg-auto">
                    <div>

                    <div class="encabezado">
                        <!--Content before waves-->
                        <div class="inner-encabezado flex2">
                        <!--Just the logo.. Don't mind this-->
                      
                        <h1 class="h1_pers">La solución de envíos</h1>
                       
                  
                        </div>

                        <p style="color:#15d455;" class="h3 font-weight-semi-bold">Sin fronteras</p>
                    <div class="w-lg-65 mx-lg-auto">
                            <hr style="border-top: 2px solid #15d455;" class="my-0">
                    </div>

                        <!--Waves Container-->
                        <div>
                        <svg class="waves2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
                        <defs>
                        <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
                        </defs>
                        <g class="parallax">
                        <use xlink:href="#gentle-wave" x="48" y="0" fill="rgba(255,255,255,0.7" />
                        <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.5)" />
                        <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.3)" />
                        <use xlink:href="#gentle-wave" x="48" y="7" fill="#fff" />
                        </g>
                        </svg>
                        </div>
                        <!--Waves end-->

                        </div>
                        <!--Header ends-->

                
                        <!--Content ends-->

                    
                  
                    </div>
                </div>
            </div>

            <div class="clearfix">

            </div>

            <div class="container space-top-1 space-bottom-2 space-bottom-lg-3">
                <div class="row justify-content-lg-center">

                    <div class="col-sm-4 col-lg-3 mb-7 mb-sm-0">                   

                        <div class="text-center">

                        <div style="width:30%;  margin-left: auto; margin-right: auto;  display: block;">
                            <img src="https://cambicash.com/images/simple_mov.svg" class="center-block img-responsive pb-4" style="width:100%">                         
                        </div>
           

                     
                            <span class="d-block h3 font-weight-semi-bold text-uppercase mb-0">Fácil</span>
                            <span class="text-secondary">CambiCash le permite Enviar dinero al extranjero o entre tus cuentas a través de una interfaz fácil de usar, cómoda e intuitiva.</span>
                        </div>

                    </div>
                    <div class="col-sm-4 col-lg-3 mb-7 mb-sm-0">
                    
                        <div class="text-center">

                        
                        <div style="width:60%;  margin-left: auto; margin-right: auto;  display: block;">
                            <img src="https://cambicash.com/images/rapido_mov.svg" class="center-block img-responsive pb-4" style="width:100%">                         
                        </div>


                            <span class="d-block h3 font-weight-semi-bold text-uppercase mb-0">Rápido</span>
                            <span class="text-secondary">Opere en solo 30 minutos en Perú, Colombia  y Venezuela a través de la tecnología Blockchain.</span>
                        </div>

                    </div>
                    <div class="col-sm-4 col-lg-3"> 

                                    

                        <div class="text-center">

                        <div style="width:40%;  margin-left: auto; margin-right: auto;  display: block;">
                            <img src="https://cambicash.com/images/seguro_mov.svg" class="center-block img-responsive pb-4" style="width:100%">                         
                        </div>    
                        
                            <span class="d-block h3 font-weight-semi-bold text-uppercase mb-0">Seguro</span>
                            <span class="text-secondary">Protegemos sus transacciones con los mejores parámetros de seguridad y con el permiso para mover su dinero a través de los principales bancos de los países donde operamos.</span>
                        </div>

                    </div>
                </div>

            </div>

            <div class="container">
                <div>
                    <hr class="my-0">
                </div>
            </div>
        </div>

    </div>

    <!-- End Section Adicional -->


    <!-- Section Adicional-->

    <div class="container">

        <div class="row">

            <div class="container space-1 space-top-md-5 space-top-lg-4">
                <div class="w-lg-80 text-center mx-lg-auto">
                    <div>






                    <div class="encabezado">
                        <!--Content before waves-->
                        <div class="inner-encabezado flex2">
                        <!--Just the logo.. Don't mind this-->
                      
                        <h1  class="h1_pers">¿Cómo funciona Cambi Cash?</h1>
                        </div>

                        <!--Waves Container-->
                        <div>
                        <svg class="waves2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
                        <defs>
                        <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
                        </defs>
                        <g class="parallax">
                        <use xlink:href="#gentle-wave" x="48" y="0" fill="rgba(255,255,255,0.7" />
                        <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.5)" />
                        <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.3)" />
                        <use xlink:href="#gentle-wave" x="48" y="7" fill="#fff" />
                        </g>
                        </svg>
                        </div>
                        <!--Waves end-->

                        </div>
                        <!--Header ends-->

                
                        <!--Content ends-->













                        
               
                    <br>      

                    </div>
                </div>
                <div class="text-center">
                    <img src="https://cambicash.com/images/p_cambicash.gif" class="center-block img-responsive pb-4" style="width:100%">
                         
                    </div>
            </div>

            <div class="clearfix">

            </div>

            <div class="container space-top-1 space-bottom-2 space-bottom-lg-3">
                <div class="row justify-content-lg-center">

                    <div class="col-md px-2 mb-3">                   

                        <div class="text-center">
                        
                            <span class="d-block h5 font-weight-semi-bold text-uppercase mb-0">1.-Cotiza tu
operación</span>
                            <div class="w-lg-65 mx-lg-auto">
                                <hr style="border-top: 2px solid #377dff;" class="my-0">
                                <br>  
                            </div>
                            <div class="text-justify">
                                <span class="text-secondary">Cotiza el monto a cambiar y registra la cuenta  en la que deseas recibir tu dinero  puedes usar tu cuenta o la de tu beneficiario.</span>
                            </div>
                        </div>

                    </div>
                    <div class="col-md px-2 mb-3">
                    
                        <div class="text-center">
                        
                            <span class="d-block h5 font-weight-semi-bold text-uppercase mb-0">2.- Transfiere </span>
                            <div class="w-lg-65 mx-lg-auto">
                                <hr style="border-top: 2px solid #377dff;" class="my-0">
                                <br>  
                            </div>
                            <div class="text-justify">
                                <span class="text-secondary">Realiza la transferencia a las cuentas que te indicamos y sube el Boucher de comprobación</span>
                            </div>
                        </div>

                    </div>
                    <div class="col-md px-2 mb-3">                    

                        <div class="text-center">
                        
                            <span class="d-block h5 font-weight-semi-bold text-uppercase mb-0">3.-Recibe Tu Cambio</span>
                            <div class="w-lg-65 mx-lg-auto">
                                <hr style="border-top: 2px solid #377dff;"  class="my-0">
                                <br>  
                            </div>
                            <div class="text-justify">
                                <span class="text-secondary">Te abonaremos a la cuenta registrada y recibirás una constancia a tu correo en cada etapa del proceso.</span>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="container">
                <div class="w-lg-65 mx-lg-auto">
                    <hr class="my-0">
                </div>
            </div>
        </div>

    </div>

    <!-- End Section Adicional -->


    <!-- Section Adicional2 -->

    <div id="SVGhireUsBg" class="position-relative gradient-half-primary-v1" style="">
      <div class="container space-2 space-top-md-4 space-top-lg-3">
        <div class="row justify-content-lg-between align-items-center">
          <div class="col-md-6 col-lg-5">
            <h1 class="display-4 font-size-md-down-5 text-white mb-4"><strong>Siempre sabrás donde está tu  <span class="text-warning">dinero</span> </strong></h1>
            <p class="lead text-white-70">Te notificaremos cada etapa de tu envío para que puedas preocuparte de las cosas que importan.</p>
            <p class="lead text-white-70">Recibirás correos con información en tiempo real sobre dónde está tu dinero. </p>
          </div>
          <div class="col-md-6">

         

            <img src="https://cambicash.com/images/escudo_mov.svg" class="center-block img-responsive pb-4" style="width:80%"> 
           
         

          </div>

        </div>
      </div>

      <!-- SVG Background -->
      <figure class="position-absolute right-0 bottom-0 left-0">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" width="100%" height="140px" viewBox="0 0 300 100" style="margin-bottom: -8px; enable-background:new 0 0 300 100;" xml:space="preserve" class="injected-svg js-svg-injector" data-parent="#SVGhireUsBg">
<style type="text/css">
  .wave-bottom-1-sm-0{fill:#FFFFFF;}
</style>
<g>
  <defs>
    <rect id="waveBottom1SMID1" width="300" height="100"></rect>
  </defs>
  <clipPath id="waveBottom1SMID2">
    <use xlink:href="#waveBottom1SMID1" style="overflow:visible;"></use>
  </clipPath>
  <path class="wave-bottom-1-sm-0 fill-white" opacity=".4" clip-path="url(#waveBottom1SMID2)" d="M10.9,63.9c0,0,42.9-34.5,87.5-14.2c77.3,35.1,113.3-2,146.6-4.7C293.7,41,315,61.2,315,61.2v54.4H10.9V63.9z"></path>
  <path class="wave-bottom-1-sm-0 fill-white" opacity=".4" clip-path="url(#waveBottom1SMID2)" d="M-55.7,64.6c0,0,42.9-34.5,87.5-14.2c77.3,35.1,113.3-2,146.6-4.7c48.7-4.1,69.9,16.2,69.9,16.2v54.4H-55.7     V64.6z"></path>
  <path class="wave-bottom-1-sm-0 fill-white" opacity=".4" fill-opacity="0" clip-path="url(#waveBottom1SMID2)" d="M23.4,118.3c0,0,48.3-68.9,109.1-68.9c65.9,0,98,67.9,98,67.9v3.7H22.4L23.4,118.3z"></path>
  <path class="wave-bottom-1-sm-0 fill-white" clip-path="url(#waveBottom1SMID2)" d="M-54.7,83c0,0,56-45.7,120.3-27.8c81.8,22.7,111.4,6.2,146.6-4.7c53.1-16.4,104,36.9,104,36.9l1.3,36.7l-372-3     L-54.7,83z"></path>
</g>
</svg>
      </figure>
      <!-- End SVG Background Section -->
    </div>


    <!-- End Section Adicional2 -->


    <!--  Section Adicional4 -->



            <div class="home5" style="align-items: center; margin-top: 10rem; min-height: 550px; position: relative; width: 100%;">
                <div class="content" style="position: relative; text-align: center; z-index: 2; ">

                <a class="js-fancybox media align-items-center u-media-player min-width-21 ml-3" href="javascript:;" data-src="https://youtu.be/p_Z-DRL_qMQ" data-speed="700" data-animate-in="zoomIn" data-animate-out="zoomOut" data-caption="">
                           
                       <img src="https://cambicash.com/images/playButton.png" alt="play video CambiCash" style="border-style: none; vertical-align: middle; filter: drop-shadow(5px 5px 4px rgba(0,0,0,.1)); margin-top: 2rem; width: 80px;">
                        <h3 style="color: #fff;font-size: 30px;font-weight: 500;margin-bottom: 0;margin-top: 1rem;text-shadow: 2px 2px 4px rgba(0,0,0,.2);">Mira cómo funciona</h3>
                    </a>
                </div>
                <div class="back" style="height: 100%; left: 0; overflow: hidden; position: absolute; top: 0; width: 100%;">
                    <video src="https://cambicash.com/videos/bgcomp2.mp4" autoplay="autoplay" muted="muted" loop="" id="videoback" alt="CambiCash" style="min-height: 100%;  min-width: 100%;"></video>
                </div>
            </div>


    <!-- End Section Adicional4  -->




    <!-- Section Adicional3 -->

    <div class="container space-2 space-lg-3" style="background-color:#fefefe">
      <div class="row justify-content-lg-between">
        <div class="col-lg-4 mb-5 mb-lg-0">


        <div style="position:relative; width:100%">
            <img src="https://cambicash.com/images/fondo_celularmov.svg" class="center-block img-responsive pb-4" style="width:100%">
          
            <div style="position:absolute; top:0; left:0; width:100%">
                <img src="https://cambicash.com/images/celular_smov.svg" class="center-block img-responsive pb-4" style="width:100%">            
                           
            </div>
        </div> 





      
          
        </div>
        <div class="col-lg-6">
        <h2 style="color:#377dff;" class="font-weight-semi-bold">Somos Diferentes</h2>
            <div class="intercambio-container">
                <div class="intercambio-body">
                    <div class="form-row mb-4">
                        <h4>Somos la evolución de los mercados financieros.</h4>
                        <h4>Envía dinero al extranjero desde donde estés y por el dispositivo tecnológico que más te acomode.<h4>                                       
                    </div>
                         <!--<div class="text-center">Gana S/20.85 más que en el banco.</div>-->
                        <br>
                        <a class="btn btn-soft-success btn-wide transition-3d-hover" id="comenzar-home" href="{{route('contacto')}}">Contáctanos</a>
                </div>
            </div>         
        </div>
      </div>
    </div>


    <!-- End Section Adicional3 -->


    <div class="waves">
    <svg width="100%" height="200px" fill="none" version="1.1"
     xmlns="http://www.w3.org/2000/svg">
      <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" stop-color="#00B4DB" />
        <stop offset="50%" stop-color="#224488" />
        <stop offset="100%" stop-color="#0083B0" />
      </linearGradient>
      <path 
        fill="url(#grad1)" 
        d="
          M0 67
          C 273,183
            822,-40
            1920.00,106 
          
          V 359 
          H 0 
          V 67
          Z">
        <animate 
          repeatCount="indefinite" 
          fill="url(#grad1)" 
          attributeName="d" 
          dur="15s"
          attributeType="XML"
          values="
            M0 77 
            C 473,283
              822,-40
              1920,116 
            
            V 359 
            H 0 
            V 67 
            Z; 

            M0 77 
            C 473,-40
              1222,283
              1920,136 
            
            V 359 
            H 0 
            V 67 
            Z; 

            M0 77 
            C 973,260
              1722,-53
              1920,120 
            
            V 359 
            H 0 
            V 67 
            Z; 

            M0 77 
            C 473,283
              822,-40
              1920,116 
            
            V 359 
            H 0 
            V 67 
            Z
            ">
        </animate>
      </path>
    </svg>
  </div>



</main>
<!-- ========== END MAIN CONTENT ========== -->

@include('frontend.resources.footer')

<!-- ========== SECONDARY CONTENTS ========== -->
@guest
@include('frontend.auth.login_side')
@endguest
@auth
@include('frontend.dashboard.asidebar')
@endauth
<!-- ========== END SECONDARY CONTENTS ========== -->

<!-- Go to Top -->
    {{-- <ul id="chat-content">
        <li class="facebook-icon"><a href="#"><i class="fab fa-facebook-messenger fa-2x"></i></a></li>
        <li class="whatsapp-icon"><a href="#"><i class="fab fa-whatsapp fa-2x  "></i></a></li>
    </ul>
    <a href="#"
     id="chat" 
     class="chat-toggle"
    >
        <i class="fa fa-comments fa-2x" aria-hidden="true"></i>
    </a> --}}
{{-- @section('style')
<style>

    #chat{
        background: #3498db;
        position: fixed;
        bottom: 15px;
        right: 59px;
        border-radius: 50%;
        padding: 5px;
        color: white;
        z-index: 999;
    }

    #chat-content{
        position: fixed;
        bottom:50px;
        right: 59px;
        list-style: none;
    }

    .facebook-icon{
        background: #0084ff;
        /* color: white; */
        padding: 5px;
        border-radius: 50%;
        box-shadow: 0px 1px 5px black;
        margin: 5px 0 10px 0;
        transition: all .2s;

    }
  

    .whatsapp-icon{
        background: #4dc247;
        padding: 5px;
        border-radius: 50%;
        box-shadow: 0px 1px 5px black;
        margin: 5px 0 3px 0;
        transition: all .2s;

    }
    .whatsapp-icon:hover,.facebook-icon:hover{
        transition: all .2s;
        box-shadow: 0px 2px 10px black;
    }
    .whatsapp-icon a{
        color: white;
        margin-left: 3px;
    }
    .facebook-icon a {
        color: white;
        margin-top: 2px;

    }

</style>
@endsection --}}
<!-- End Go to Top -->
<!-- Go to Top -->
<a class="js-go-to u-go-to" href="#" data-position='{"bottom": 24, "right": 90 }' data-type="fixed"
    data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
</a>
<!-- End Go to Top -->

@endsection

@section('script')
<!-- JS Plugins Init. -->



<script>
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {

        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
            afterOpen: function () {
                $(this).find('input[type="search"]').focus();
            }
        });

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of forms
        $.HSCore.components.HSFocusState.init();

        // initialization of form validation
        $.HSCore.components.HSValidation.init('.js-validate', {
            rules: {
                confirmPassword: {
                    equalTo: '#signupPassword'
                }
            }
        });

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of text animation (typing)
        var typed = new Typed(".u-text-animation.u-text-animation--typing", {
            strings: ["Fácil", "Seguro", "Económico"],
            typeSpeed: 90,
            loop: true,
            backSpeed: 25,
            backDelay: 1500
        });

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        /* Plugin Paises */
        $("#country_selector").countrySelect({
			// defaultCountry: "jp",
			// onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
			// responsiveDropdown: true,
			preferredCountries: ['ca', 'gb', 'us']
		});
    });

    // $('#basic').flagStrap();

    // $('.select-country').flagStrap({
    //     countries: {
    //         "US": "USD",
    //         "AU": "AUD",
    //         "CA": "CAD",
    //         "SG": "SGD",
    //         "GB": "GBP",
    //     },
    //     buttonSize: "btn-sm",
    //     buttonType: "btn-info",
    //     labelMargin: "10px",
    //     scrollable: false,
    //     scrollableHeight: "350px"
    // });

    // $('#advanced').flagStrap({
    //     buttonSize: "btn-lg",
    //     buttonType: "btn-primary",
    //     labelMargin: "20px",
    //     scrollable: false,
    //     scrollableHeight: "350px"
    // });


    /*Flags*/
    function setCountry(code){
        if(code || code==''){
            var text = jQuery('a[cunt_code="'+code+'"]').html();
            $(".dropdown dt a span").html(text);
        }
    }
    $(document).ready(function() {
        $(".dropdown img.flag").addClass("flagvisibility");

        $(".dropdown dt a").click(function() {
            $(".dropdown dd ul").toggle();
        });

        $(".dropdown dd ul li a").click(function() {
            //console.log($(this).html())
            var text = $(this).html();
            $(".dropdown dt a span").html(text);
            $(".dropdown dd ul").hide();
            $("#result").html("Selected value is: " + getSelectedValue("country-select"));
        });

        function getSelectedValue(id) {
            //console.log(id,$("#" + id).find("dt a span.value").html())
            return $("#" + id).find("dt a span.value").html();
        }

        $(document).bind('click', function(e) {
            var $clicked = $(e.target);
            if (! $clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").hide();
        });


        $("#flagSwitcher").click(function() {
            $(".dropdown img.flag").toggleClass("flagvisibility");
        });
    });

</script>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "320800488538138", // Facebook page ID
            whatsapp: "+51(991)897590", // WhatsApp number
            // call_to_action: "Contactanos", // Call to action
            button_color: "#129BF4", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,whatsapp", // Order of buttons
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->

<script>
        
        function animateCSS(element, animationName,delay,callback) {
            const node = document.querySelector(element)
            node.classList.add('animated', animationName,delay)

            function handleAnimationEnd() {
                node.classList.remove('animated', animationName,delay)
                node.removeEventListener('animationend', handleAnimationEnd)

                if (typeof callback === 'function') callback()
            }

            node.addEventListener('animationend', handleAnimationEnd)
        }
        let hideChat = true;
        $('.chat-toggle').click(function(e){
            e.preventDefault();
                animateCSS('.chat-toggle', 'rotateIn','faster',function(){
                    animateCSS('#chat-content', 'bounceInUp','faster')
                    console.log(hideChat)
                })
        })

			var hash = window.location.hash,
				current = 0,
				demos = Array.prototype.slice.call( document.querySelectorAll( '#codrops-demos > a' ) );
			
			if( hash === '' ) hash = '#set-1';
			setDemo( demos[ parseInt( hash.match(/#set-(\d+)/)[1] ) - 1 ] );

			demos.forEach( function( el, i ) {
				el.addEventListener( 'click', function() { setDemo( this ); } );
			} );

			function setDemo( el ) {
				var idx = demos.indexOf( el );
				if( current !== idx ) {
					var currentDemo = demos[ current ];
					currentDemo.className = currentDemo.className.replace(new RegExp("(^|\\s+)" + 'current-demo' + "(\\s+|$)"), ' ');
				}
				current = idx;
				el.className = 'current-demo'; 
			}
		</script>
@endsection