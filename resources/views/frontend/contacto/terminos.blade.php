@extends('frontend.resources.core')
@section('content')
<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
    <div class="container">
        <span class="u-skiplink-text">Skip to main content</span>
    </div>
</a>
<!-- End Skippy -->

@include('frontend.resources.header')






<main id="content" role="main">
    <div class="container space-2 space-top-md-4 space-bottom-md-3">
      <div class="row">
        <div id="stickyBlockStartPoint" class="col-md-4 col-lg-3 mb-7 mb-md-0">
          <nav class="js-sticky-block card shadow-sm p-4"
               data-parent="#stickyBlockStartPoint"
               data-sticky-view="md"
               data-start-point="#stickyBlockStartPoint"
               data-end-point="#stickyBlockEndPoint"
               data-offset-top="24"
               data-offset-bottom="24">
            <ul class="js-scroll-nav list-group list-group-transparent list-group-flush list-group-borderless">
              <li>
                <a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#services">1.Empresa</a>

                <ul class="list-group list-group-transparent list-group-flush list-group-borderless">
                  <li>
                    <a class="list-group-item list-group-item-action mb-2" href="#personal-data">A.Servicio</a>
                  </li>
                  <li>
                    <a class="list-group-item list-group-item-action mb-2" href="#information">B.  Objeto del contrato</a>
                  </li>
                </ul>
              </li>
              <li>
                <a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#privacy">2. Registro</a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#yourContent">3. Your content in our services</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col-md-8 col-lg-9">
          <div id="intro" class="space-bottom-1">
            <!-- Title -->
            <div class="border-bottom pb-5 mb-5">
              <h1 class="text-primary"><span class="font-weight-semi-bold"¡</span> TÉRMINOS Y CONDICIONES </h1>
              <p>Effective date: 1 January 2019</p>
            </div>
            <!-- End Title -->

            <!-- Title -->
            <div class="mb-3">
              <h2 class="h5">DATOS</h2>
            </div>
            <!-- End Title -->

            <p>Este es un contrato entre usted, en adelante “EL CLIENTE” y CAMBICASH SAC, en adelante  “CAMBICASH”. Este contrato detalla los servicios que presta CAMBICASH.</p>
            
          </div>

          <div id="services" class="space-bottom-1">
            <!-- Title -->
            <div class="mb-3">
              <h3 class="h5">1. Uso de servicios</h3>
            </div>
            <!-- End Title -->

            <p>CAMBICASH ofrece sus servicios a través de medios digitales, donde cuenta con un portal en Internet cambicash.com, el cual en adelante denominaremos en su conjunto como “LA WEB”. LA WEB han sido creada con la finalidad de brindar el servicio de cambio de divisas.</p>
            <p>Se denomina “CLIENTE” a todo usuario que se registra a través de cualquiera de LA WEB. Para hacer uso de los servicios de CAMBICASH, el CLIENTE debe contar con una cuenta bancaria en una entidad financiera supervisada por la SBS, es decir, debe aceptar realizar transferencias y recepción de fondos a través de entidades financieras.</p>
            <p>Tener en cuenta que los datos e información incluidos en LA WEB brindan elementos para la toma de decisión financiera, por lo cual no debe tomarse como una asesoría o sugerencia por parte de CAMBICASH para la compra o venta de los servicios que ofrecemos.</p>

            <!-- Title -->
            <div id="personal-data" class="mb-3">
              <h4 class="h6">A. Enviar una Orden de Compra y Venta.</h4>
            </div>
            <!-- End Title -->

            <p>Usted debe enviar órdenes de compra online utilizando su cuenta en la página web
www.cambicash.com, o a través de la web que la Empresa pone a su disposición
en IOS y Android; para ello se le pedirá que brinde la siguiente información:

:</p>

            <ul class="text-secondary">
              <li class="pb-2">Número de la cuenta en Soles o Dólares en la cual se realizará el abono de la
transacción.</li>
              <li class="pb-2">Número de documento de identidad (DNI, C.E., RUC, pasaporte) del titular de la
cuenta.</li>
              <li class="pb-2"> Monto a ser cambiado y moneda.</li>
              <li class="pb-2">Una vez completada la orden de compra online usted recibirá en su correo electrónico
la confirmación de la recepción de su operación.</li>
            </ul>

            <p>Es importante señalar que una orden de compra sólo será procesada y completada una vez que se haya confirmado y validado la transacción de los fondos.</p>
 <p>Es responsabilidad del usuario enviar los fondos durante el horario de atención de la Empresa, a fin de asegurar el procesamiento de la orden de compra.<p>
<p>Cambicash no influye en las operaciones que los bancos realizan, por ende, no se hace responsable por la dilación en los tiempos del envío de los fondos por su entidad bancaria o proveedor de pagos.</p>
<p>Dicho lo anterior, cabe señalar que la Empresa monitorea continuamente sus cuentas bancarias, y asume la obligación de notificar y completar su orden de compra siempre y cuando hayan sido recibidas antes de las 19:00 horas de un día útil. Si Cambicas recibiera los fondos pasada dicha hora, la orden de compra será completada al día hábil siguiente.</p>
<p>Por otro lado, las órdenes de compra colocadas durante un día no hábil también seránprocesadas al siguiente día útil.</p>
<p>El usuario reconoce y acepta que es responsable por verificar la cuenta de destino a la que le transfiere el dinero a la Empresa, para ello debe tener en consideración que Cambicash tiene cuentas en Soles , Pesos colombianos, Bolivares, Dolares y Bitcoin y no se responsabiliza de ninguna manera por la negligencia del usuario en este aspecto.
</p>

            <!-- Title -->
            <div id="information" class="mb-3">
              <h4 class="h6">B. Information that we collect automatically on our Sites.</h4>
            </div>
            <!-- End Title -->

            <p>We also may collect information about your online activities on websites and connected devices over time and across third-party websites, devices, apps and other online features and services. We use Google Analytics on our Sites to help us analyze Your use of our Sites and diagnose technical issues.</p>

            <p>To learn more about the cookies that may be served through our Sites and how You can control our use of cookies and third-party analytics, please see our Cookie Policy.</p>
          </div>

          <div id="privacy" class="space-bottom-1">
            <!-- Title -->
            <div class="mb-3">
              <h3 class="h5">2. Privacy and copyright protection</h3>
            </div>
            <!-- End Title -->

            <p>Front's privacy policies explain how we treat your personal data and protect your privacy when you use our Services. By using our Services, you agree that Front can use such data in accordance with our privacy policies.</p>
            <p>We respond to notices of alleged copyright infringement and terminate accounts of repeat infringers according to the process set out in the U.S. Digital Millennium Copyright Act.</p>
            <p>We provide information to help copyright holders manage their intellectual property online. If you think somebody is violating your copyrights and want to notify us, you can find information about submitting notices and Front's policy about responding to notices in <a href="help.html">our Help Center</a>.</p>
          </div>

          <div id="yourContent">
            <!-- Title -->
            <div class="mb-3">
              <h3 class="h5">3. Your content in our services</h3>
            </div>
            <!-- End Title -->

            <p>Some of our Services allow you to upload, submit, store, send or receive content. You retain ownership of any intellectual property rights that you hold in that content. In short, what belongs to you stays yours.</p>
            <p>When you upload, submit, store, send or receive content to or through our Services, you give Front (and those we work with) a worldwide license to use, host, store, reproduce, modify, create derivative works (such as those resulting from translations, adaptations or other changes we make so that your content works better with our Services), communicate, publish, publicly perform, publicly display and distribute such content. The rights you grant in this license are for the limited purpose of operating, promoting, and improving our Services, and to develop new ones. This license continues even if you stop using our Services (for example, for a business listing you have added to Front Maps). Some Services may offer you ways to access and remove content that has been provided to that Service. Also, in some of our Services, there are terms or settings that narrow the scope of our use of the content submitted in those Services. Make sure you have the necessary rights to grant us this license for any content that you submit to our Services.</p>
          </div>
        </div>
      </div>
    </div>
    <div id="stickyBlockEndPoint"></div>
</main>












<!-- ========== END MAIN CONTENT ========== -->

@include('frontend.resources.footer')

<!-- ========== SECONDARY CONTENTS ========== -->
@guest
@include('frontend.auth.login_side')
@endguest
@auth
@include('frontend.dashboard.asidebar')
@endauth
<!-- ========== END SECONDARY CONTENTS ========== -->

<!-- Go to Top -->
<a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed"
    data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
</a>
<!-- End Go to Top -->

@endsection

@section('script')
<!-- JS Plugins Init. -->
<script>
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
            afterOpen: function () {
                $(this).find('input[type="search"]').focus();
            }
        });

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of forms
        $.HSCore.components.HSFocusState.init();

        // initialization of form validation
        $.HSCore.components.HSValidation.init('.js-validate', {
            rules: {
                confirmPassword: {
                    equalTo: '#signupPassword'
                }
            }
        });

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of text animation (typing)
        var typed = new Typed(".u-text-animation.u-text-animation--typing", {
            strings: ["Costos Altos", "Costos Ocultos", "Malos Servicios"],
            typeSpeed: 60,
            loop: true,
            backSpeed: 25,
            backDelay: 1500
        });

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        /* Plugin Paises */
        $("#country_selector").countrySelect({
			// defaultCountry: "jp",
			// onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
			// responsiveDropdown: true,
			preferredCountries: ['ca', 'gb', 'us']
		});
    });

    /*Flags*/
    function setCountry(code){
        if(code || code==''){
            var text = jQuery('a[cunt_code="'+code+'"]').html();
            $(".dropdown dt a span").html(text);
        }
    }
    $(document).ready(function() {
        $(".dropdown img.flag").addClass("flagvisibility");

        $(".dropdown dt a").click(function() {
            $(".dropdown dd ul").toggle();
        });

        $(".dropdown dd ul li a").click(function() {
            //console.log($(this).html())
            var text = $(this).html();
            $(".dropdown dt a span").html(text);
            $(".dropdown dd ul").hide();
            $("#result").html("Selected value is: " + getSelectedValue("country-select"));
        });

        function getSelectedValue(id) {
            //console.log(id,$("#" + id).find("dt a span.value").html())
            return $("#" + id).find("dt a span.value").html();
        }

        $(document).bind('click', function(e) {
            var $clicked = $(e.target);
            if (! $clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").hide();
        });


        $("#flagSwitcher").click(function() {
            $(".dropdown img.flag").toggleClass("flagvisibility");
        });
    });

</script>
@endsection