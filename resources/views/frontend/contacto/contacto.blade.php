@extends('frontend.resources.core')
@section('content')
<!-- Skippy -->
<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
    <div class="container">
        <span class="u-skiplink-text">Skip to main content</span>
    </div>
</a>
<!-- End Skippy -->
@include('frontend.resources.header', [ 'relative' => true ])

  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    <!-- Hero Section -->
    <div id="SVGhireUsBg" class="svg-preloader position-relative gradient-half-primary-v1">
      <div class="container space-2 space-top-md-4 space-top-lg-3">
        <div class="row justify-content-lg-between align-items-center">
          <div class="col-md-6 col-lg-5">
            <h1 class="display-4 font-size-md-down-5 text-white mb-4"><strong>¿Cómo pueden <span class="text-warning">ayudarte</span> los asistentes de CambiCash?</strong></h1>
            <p class="lead text-white-70">Nos encantaría hablar sobre cómo podemos ayudarlo.</p>
          </div>
          <div class="col-md-6">
            <img class="js-svg-injector" src="{{asset('svg/components/contact-us.svg')}}" alt="contact us" data-parent="#SVGhireUsBg">
          </div>
        </div>
      </div>

      <!-- SVG Background -->
      <figure class="position-absolute right-0 bottom-0 left-0">
        <img class="js-svg-injector" src="{{asset('svg/components/wave-1-bottom-sm.svg')}}" alt="Image Description"
             data-parent="#SVGhireUsBg">
      </figure>
      <!-- End SVG Background Section -->
    </div>
    <!-- End Hero Section -->

    <!-- Contact Form Section -->
    <div class="container space-2 space-md-3">
      <div class="row justify-content-lg-between">
        <div class="col-lg-6 mb-9 mb-lg-0">
          <!-- Title -->
          <div class="mb-7">
            <h2 class="text-primary font-weight-normal">
              <span class="font-weight-semi-bold">Tell us</span>
              <br>
              about yourself
            </h2>
            <p>Whether you have questions or you would just like to say hello, contact us.</p>
          </div>
          <!-- End Title -->

          <iframe class="embed-responsive " src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5093765568954!2d-75.576193585231!3d6.196322695514285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4682881ca00127%3A0xf96762aa39ea4209!2sCentro%20Comercial%20Santaf%C3%A9!5e0!3m2!1ses!2sve!4v1576250759898!5m2!1ses!2sve" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
          {{-- <!-- Google Map -->
          <div id="gMapMultipleMarkers" class="embed-responsive embed-responsive-21by9 u-gmap-interactive min-height-300 mb-4"
               data-type="custom"
               data-lat="37.4040344"
               data-lng="-122.0289704"
               data-zoom="13"
               data-pin="true"
               data-multiple-markers="true"
               data-styles='[[
                "", "", [
                  {"saturation": -100},
                  {"lightness": 50},
                  {"visibility": "simplified"}
                ]],[
                  "",
                  "geometry", [
                    {"color": "#f3f3f3"}
                ]],[
                  "road", "", [
                    {"color": "#ffffff"},
                    {"lightness": 100}
                ]],[
                  "road",
                  "labels.text.fill", [
                    {"color": "#ffffff"},
                    {"lightness": -50}]
                ],[
                  "water", "", [
                    {"color": "#377dff"}
                ]]
               ]'
               data-markers-locations='[[
                  "../../assets/img/map-markers/map-marker1.png",
                  "Twisters Sports",
                  37.4037277, -122.0093084,
                  "../../assets/img/150x80/img1.jpg",
                  "Image description",
                  "17:48, April 27, 2017",
                  "United States",
                  "Twisters Sports"
                ],[
                  "../../assets/img/map-markers/map-marker5.png",
                  "Equinix Data Center",
                  37.4011793, -122.013562,
                  "../../assets/img/150x80/img5.jpg",
                  "Image description",
                  "17:30, May 21, 2017",
                  "United States",
                  "Equinix Data Center"
                ],[
                  "../../assets/img/map-markers/map-marker2.png",
                  "Amazon Lab126",
                  37.4093023, -122.0385558,
                  "../../assets/img/150x80/img2.jpg",
                  "Image description",
                  "11:48, June 3, 2017",
                  "United States",
                  "Amazon Lab126"
                ],[
                  "../../assets/img/map-markers/map-marker3.png",
                  "Google Building TC4",
                  37.4061573, -122.0400798,
                  "../../assets/img/150x80/img3.jpg",
                  "Image description",
                  "9:48, January 1, 2017",
                  "United States",
                  "Google Building TC4"
                ],[
                  "../../assets/img/map-markers/map-marker4.png",
                  "Sheraton Sunnyvale",
                  37.4040344, -122.0289704,
                  "{{asset('svg/components/img4.png')}}",
                  "Image description",
                  "9:48, January 1, 2017",
                  "United States",
                  "Sheraton Sunnyvale"
                ]]'></div>
          <!-- End Google Map --> --}}

          <!-- Office Hours -->
          <div class="media">
            <h4 class="h6 mr-3">Office Hours:</h4>
            <div class="media-body">
              <p class="small mb-0">Monday - Friday: 09:00 AM - 06:00 PM</p>
              <p class="small mb-0">Closed on Sunday &amp; Holidays</p>
            </div>
          </div>
          <!-- End Office Hours -->
        </div>

        <div id="SVGwave1BottomShape" class="col-lg-5 svg-preloader d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v1 min-height-550 rounded-top-pseudo" style="background-image: url({{asset('svg/components/img4.jpg')}});">
          <!-- Contacts Info -->
          <div class="position-relative w-100 text-center p-5 pb-9">
            <!-- Info -->
            <div class="mb-5">
              <h2 class="text-white font-weight-semi-bold mb-0">Cambicash</h2>
              <p class="text-white-70">153 Williamson Plaza, Maggieberg, MT 09514</p>
            </div>
            <!-- End Info -->

            <!-- Info -->
            <div class="mb-5">
              <h3 class="h6 text-white">Número de teléfono</h3>
              <span class="d-block h5 text-warning">+1 (062) 109-9222</span>
            </div>
            <!-- End Info -->

            <!-- Subscribe Form -->
            <form class="js-validate js-form-message">
              <label class="sr-only" for="heroSubscribeSrEmail">Ingresar Correo Electrónico</label>
              <div class="input-group input-group-lg input-group-borderless input-group-pill">
                <input type="email" class="form-control" name="email" id="heroSubscribeSrEmail" placeholder="Ingresar Correo Electrónico" aria-label="Enter your email address" aria-describedby="heroSubscribeButton" required>
                <div class="input-group-append">
                  <button class="btn btn-white text-primary" type="submit" id="heroSubscribeButton">
                    <span class="fas fa-paper-plane"></span>
                  </button>
                </div>
              </div>
            </form>
            <!-- End Subscribe Form -->
          </div>
          <!-- End Contacts Info -->

          <!-- SVG Bottom Shape -->
          <figure class="position-absolute right-0 bottom-0 left-0">
            <img class="js-svg-injector" src="{{asset('svg/components/wave-1-bottom.svg')}}" alt="Image Description"
                 data-parent="#SVGwave1BottomShape">
          </figure>
          <!-- End SVG Bottom Shape -->
        </div>
      </div>
    </div>
    <!-- End Contact Form Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  @include('frontend.resources.footer')
  <!-- ========== END FOOTER ========== -->

  <!-- ========== SECONDARY CONTENTS ========== -->
  <!-- Account Sidebar Navigation -->
  @guest
  @include('frontend.auth.login_side')
  @endguest
  @auth
  @include('frontend.dashboard.asidebar')
  @endauth
  <!-- End Account Sidebar Navigation -->
  <!-- ========== END SECONDARY CONTENTS ========== -->
<!-- Go to Top -->
<a class="js-go-to u-go-to" href="#" data-position='{"bottom": 24, "right": 90 }' data-type="fixed"
    data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
</a>
<!-- End Go to Top -->

@endsection

@section('script')
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
  (function () {
      var options = {
          facebook: "320800488538138", // Facebook page ID
          whatsapp: "+51(991)897590", // WhatsApp number
          // call_to_action: "Contactanos", // Call to action
          button_color: "#129BF4", // Color of button
          position: "right", // Position may be 'right' or 'left'
          order: "facebook,whatsapp", // Order of buttons
      };
      var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
      var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
      s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
      var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
  })();
</script>
<!-- /WhatsHelp.io widget -->
<!-- JS Plugins Init. -->

<script>
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
      // map = new google.maps.Map(document.getElementById('gMapMultipleMarkers'), {
      //     center: {lat: -34.397, lng: 150.644},
      //     zoom: 8
      //   });
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
            afterOpen: function () {
                $(this).find('input[type="search"]').focus();
            }
        });

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of forms
        $.HSCore.components.HSFocusState.init();

        // initialization of form validation
        $.HSCore.components.HSValidation.init('.js-validate', {
            rules: {
                confirmPassword: {
                    equalTo: '#signupPassword'
                }
            }
        });

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        /* Plugin Paises */
        $("#country_selector").countrySelect({
			preferredCountries: ['ca', 'gb', 'us']
		});
    });

    /*Flags*/
    function setCountry(code){
        if(code || code==''){
            var text = jQuery('a[cunt_code="'+code+'"]').html();
            $(".dropdown dt a span").html(text);
        }
    }
    $(document).ready(function() {
        $(".dropdown img.flag").addClass("flagvisibility");

        $(".dropdown dt a").click(function() {
            $(".dropdown dd ul").toggle();
        });

        $(".dropdown dd ul li a").click(function() {
            //console.log($(this).html())
            var text = $(this).html();
            $(".dropdown dt a span").html(text);
            $(".dropdown dd ul").hide();
            $("#result").html("Selected value is: " + getSelectedValue("country-select"));
        });

        function getSelectedValue(id) {
            //console.log(id,$("#" + id).find("dt a span.value").html())
            return $("#" + id).find("dt a span.value").html();
        }

        $(document).bind('click', function(e) {
            var $clicked = $(e.target);
            if (! $clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").hide();
        });


        $("#flagSwitcher").click(function() {
            $(".dropdown img.flag").toggleClass("flagvisibility");
        });
    });

</script>
@endsection