@extends('frontend.resources.core')
@section('content')
<!-- Skippy -->
<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
    <div class="container">
        <span class="u-skiplink-text">Skip to main content</span>
    </div>
</a>
<!-- End Skippy -->

@include('frontend.resources.header')

  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    <!-- Hero Section -->
    <div class="container space-2 space-top-md-5 space-top-lg-4">
      <div class="w-lg-80 text-center mx-lg-auto">
        <div class="mb-11">
          <h1 class="display-4 font-weight-semi-bold">{{__('Sobre Nosotros')}}</h1>
          <p class="lead font-weight-normal">Nacimos con el sueño de ofrecer la mejor experiencia de cambiar dinero en Latinoamérica.</p>
        </div>
      </div>

      <!-- Gallery Section -->
      <div class="row mx-n2">
        <div class="col-md px-2 mb-3">
          <div class="height-250 bg-img-hero" style="background-image: url({{asset('images/nosotros/img31.jpg')}});"></div>
        </div>
        <div class="col-md-3 px-2 mb-3">
          <div class="height-250 bg-img-hero" style="background-image: url({{asset('images/nosotros/img38.jpg')}});"></div>
        </div>
        <div class="col-md px-2 mb-3">
          <div class="height-250 bg-img-hero" style="background-image: url({{asset('images/nosotros/img34.jpg')}});"></div>
        </div>

        <div class="w-100"></div>

        <div class="col-md px-2 mb-3 mb-md-0">
          <div class="height-250 bg-img-hero" style="background-image: url({{asset('images/nosotros/img25.jpg')}});"></div>
        </div>
        <div class="col-md-4 px-2 mb-3 mb-md-0">
          <div class="height-250 bg-img-hero" style="background-image: url({{asset('images/nosotros/img32.jpg')}});"></div>
        </div>
        <div class="col-md px-2">
          <div class="height-250 bg-img-hero" style="background-image: url({{asset('images/nosotros/img36.jpg')}});"></div>
        </div>
      </div>
      <!-- End Gallery Section -->
    </div>
    <!-- End Hero Section -->

    <!-- Stats Section -->
    <div class="container space-top-1 space-bottom-2 space-bottom-lg-3">
      <div class="row justify-content-lg-center">
        <div class="col-sm-4 col-lg-3 mb-7 mb-sm-0">
          <!-- Stats -->
          <div class="text-center">
            <span class="d-block h2 font-weight-semi-bold text-uppercase mb-0">17</span>
            <span class="text-secondary">years in business</span>
          </div>
          <!-- End Stats -->
        </div>

        <div class="col-sm-4 col-lg-3 mb-7 mb-sm-0">
          <!-- Stats -->
          <div class="text-center">
            <span class="d-block h2 font-weight-semi-bold text-uppercase mb-0">1k+</span>
            <span class="text-secondary">products launched</span>
          </div>
          <!-- End Stats -->
        </div>

        <div class="col-sm-4 col-lg-3">
          <!-- Stats -->
          <div class="text-center">
            <span class="d-block h2 font-weight-semi-bold text-uppercase mb-0">53%</span>
            <span class="text-secondary">remote employees</span>
          </div>
          <!-- End Stats -->
        </div>
      </div>
    </div>
    <!-- End Stats Section -->

    <!-- Divider -->
    <div class="container">
      <div class="w-lg-65 mx-lg-auto">
        <hr class="my-0">
      </div>
    </div>
    <!-- End Divider -->

    <!-- About Section -->
    <div class="container space-2 space-lg-3">
      <div class="row justify-content-lg-between">
        <div class="col-lg-4 mb-5 mb-lg-0">
          <h2 class="font-weight-semi-bold">Llegamos a través de la tecnología
hasta donde tu estás.
 
En tu escritorio, en tu bolsillo,
desde tu celular y en cualquier parte.</h2>
        </div>
        <div class="col-lg-6">
          <p>Cambicash es una Fintech  que utiliza la tecnología Blockchain para enviar y recibir transferencias de dinero entre usuarios internos y externos. Orientada al beneficio del cliente mediante la reducción de riesgo y el aumento de la competitividad empresarial con el uso del tipo de cambio. Ofrece una solución para el cambio de divisas basado en la tecnología y eficiencia. De esta manera, brinda un servicio de calidad, seguro y con excelente tasa.</p>
          <p>Ofrecemos una  red de servicios públicos y remesas. Gracias a la tecnología detrás de Bitcoin, podemos hacer que sea un proceso simple y efectivo para todo el mundo. Rápido, simple y de bajo costo</p>
        </div>
      </div>
    </div>
    <!-- End About Section -->

    <!-- Divider -->
    <div class="container">
      <div class="w-lg-65 mx-lg-auto">
        <hr class="my-0">
      </div>
    </div>
    <!-- End Divider -->

    <!-- Team Section -->
    <div class="container space-2 space-lg-3">
      <!-- Title -->
      <div class="w-md-80 w-lg-60 text-center mx-md-auto mb-9">
        <span class="btn btn-xs btn-soft-success btn-pill mb-2">Nuestro Equipo</span>
        <h2 class="h3 font-weight-semi-bold">
mentes creativas de personas como tú</h2>
      </div>
      <!-- End Title -->

      <div class="row mx-n2 mb-5 d-flex justify-content-center">
        <div class="col-sm-6 col-lg-3 px-2 mb-3">
          <!-- Team -->
          <div class="card shadow transition-3d-hover p-4">
            <div class="u-lg-avatar mb-4">
              <img class="img-fluid rounded-circle" src="{{asset('img/100x100/img1.jpg')}}" alt="Image Description">
            </div>

            <small class="d-block text-secondary text-uppercase mb-1">Founder / CEO</small>
            <h4 class="h5 text-lh-sm">Julio Peña</h4>
            <p class="font-size-1">I am an ambitious workaholic, but apart from that, pretty simple person.</p>

            <!-- Social Networks -->
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="https://www.facebook.com/juliio.pena.5">
                  <i class="fab fa-linkedin  btn-icon__inner   "></i>
                </a>
              </li>
              
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="#">
                  <span class="fab fa-twitter btn-icon__inner"></span>
                </a>
              </li>
            </ul>
            <!-- End Social Networks -->
          </div>
          <!-- End Team -->
        </div>

        <div class="col-sm-6 col-lg-3 px-2 mb-3">
          <!-- Team -->
          <div class="card shadow transition-3d-hover p-4">
            <div class="u-lg-avatar mb-4">
              <img class="img-fluid rounded-circle" src="{{asset('img/100x100/img3.jpg')}}" alt="Image Description">
            </div>

            <small class="d-block text-secondary text-uppercase mb-1">Project Manager</small>
            <h4 class="h5 text-lh-sm">Jack Wayley</h4>
            <p class="font-size-1">I am an ambitious workaholic, but apart from that, pretty simple person.</p>

            <!-- Social Networks -->
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="#">
                  <i class="fab fa-linkedin  btn-icon__inner   "></i>
                </a>
              </li>
              
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="#">
                  <span class="fab fa-twitter btn-icon__inner"></span>
                </a>
              </li>
            </ul>
            <!-- End Social Networks -->
          </div>
          <!-- End Team -->
        </div>

        {{-- <div class="col-sm-6 col-lg-3 px-2 mb-3 mb-sm-0">
          <!-- Team -->
          <div class="card shadow transition-3d-hover p-4">
            <div class="u-lg-avatar mb-4">
              <img class="img-fluid rounded-circle" src="{{asset('img/100x100/img10.jpg')}}" alt="Image Description">
            </div>

            <small class="d-block text-secondary text-uppercase mb-1">Product Designer</small>
            <h4 class="h5 text-lh-sm">Emmely Jackson</h4>
            <p class="font-size-1">I am an ambitious workaholic, but apart from that, pretty simple person.</p>

            <!-- Social Networks -->
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="#">
                  <i class="fab fa-linkedin  btn-icon__inner   "></i>
                </a>
              </li>
              
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="#">
                  <span class="fab fa-twitter btn-icon__inner"></span>
                </a>
              </li>
            </ul>
            <!-- End Social Networks -->
          </div>
          <!-- End Team -->
        </div> --}}

        <div class="col-sm-6 col-lg-3 px-2">
          <!-- Team -->
          <div class="card shadow transition-3d-hover p-4">
            <div class="u-lg-avatar mb-4">
              <img class="img-fluid rounded-circle" src="{{asset('img/100x100/img2.jpg')}}" alt="Image Description">
            </div>

            <small class="d-block text-secondary text-uppercase mb-1">Web designer</small>
            <h4 class="h5 text-lh-sm">Mark McManus</h4>
            <p class="font-size-1">I am an ambitious workaholic, but apart from that, pretty simple person.</p>

            <!-- Social Networks -->
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="#">
                  <i class="fab fa-linkedin  btn-icon__inner   "></i>
                </a>
              </li>
              
              <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary rounded" href="#">
                  <span class="fab fa-twitter btn-icon__inner"></span>
                </a>
              </li>
            </ul>
            <!-- End Social Networks -->
          </div>
          <!-- End Team -->
        </div>
      </div>

      <!-- Hire Us -->
      <div class="text-center">
        <div class="d-sm-inline-flex align-items-center bg-white shadow-soft rounded-pill p-2 pr-3">
          <span class="btn btn-xs btn-primary btn-pill mb-2 mb-sm-0 mr-2">New</span>
          <span class="d-block d-sm-inline-block">
            We are hiring.
            <a href="../pages/hire-us.html">Send your application.</a>
          </span>
        </div>
      </div>
      <!-- End Hire Us -->
    </div>
    <!-- End Team Section -->

    <!-- Subscribe Section -->
    <div class="space-1">
      <div class="bg-img-hero" style="background-image: url({{asset('img/bg-shapes/bg7.png')}});">
        <div class="container space-2 space-bottom-lg-3">
          <!-- Title -->
          <div class="w-md-60 text-center mx-auto mb-7">
            <h2 class="font-weight-semi-bold">Stay in the know</h2>
            <p>Get special offers on the latest developments from Front.</p>
          </div>
          <!-- End Title -->

          <!-- Subscribe Form -->
          <div class="w-md-75 w-lg-50 mx-md-auto">
            <form class="js-validate form-row">
              <div class="col-xs-12 col-md-8 buttons--input">
                <div class="js-form-message">
                  <label class="sr-only" for="signupSrEmailExample1">Your email</label>
                  <div class="input-group input-group-pill">
                    <input type="email" class="form-control" name="email" id="signupSrEmailExample1" placeholder="Your email" aria-label="Your email" required data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-4 get--estart">
                <button type="submit" class="btn btn-primary btn-pill btn-wide transition-3d-hover">Get Started</button>
              </div>
            </form>
          </div>
          <!-- End Subscribe Form -->
        </div>
      </div>
    </div>
    <!-- End Subscribe Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  @include('frontend.resources.footer')
  <!-- ========== END FOOTER ========== -->

<!-- ========== SECONDARY CONTENTS ========== -->
@guest
@include('frontend.auth.login_side')
@endguest
@auth
@include('frontend.dashboard.asidebar')
@endauth
<!-- ========== END SECONDARY CONTENTS ========== -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 24, "right": 90 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

@endsection

@section('script')
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
  (function () {
      var options = {
          facebook: "320800488538138", // Facebook page ID
          whatsapp: "+51(991)897590", // WhatsApp number
          // call_to_action: "Contactanos", // Call to action
          button_color: "#129BF4", // Color of button
          position: "right", // Position may be 'right' or 'left'
          order: "facebook,whatsapp", // Order of buttons
      };
      var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
      var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
      s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
      var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
  })();
</script>
<!-- /WhatsHelp.io widget -->
  <!-- JS Plugins Init. -->
  <script>
      $(window).on('load', function () {
          // initialization of HSMegaMenu component
          $('.js-mega-menu').HSMegaMenu({
              event: 'hover',
              pageContainer: $('.container'),
              breakpoint: 767.98,
              hideTimeOut: 0
          });
  
          // initialization of svg injector module
          $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
      });
  
      $(document).on('ready', function () {
          // initialization of header
          $.HSCore.components.HSHeader.init($('#header'));
  
          // initialization of unfold component
          $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
              afterOpen: function () {
                  $(this).find('input[type="search"]').focus();
              }
          });
  
          // initialization of malihu scrollbar
          $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));
  
          // initialization of forms
          $.HSCore.components.HSFocusState.init();
  
          // initialization of form validation
          $.HSCore.components.HSValidation.init('.js-validate', {
              rules: {
                  confirmPassword: {
                      equalTo: '#signupPassword'
                  }
              }
          });
  
          // initialization of show animations
          $.HSCore.components.HSShowAnimation.init('.js-animation-link');
  
          // initialization of fancybox
          $.HSCore.components.HSFancyBox.init('.js-fancybox');
  
          // initialization of text animation (typing)
          var typed = new Typed(".u-text-animation.u-text-animation--typing", {
              strings: ["Costos Altos", "Costos Ocultos", "Malos Servicios"],
              typeSpeed: 60,
              loop: true,
              backSpeed: 25,
              backDelay: 1500
          });
  
          // initialization of slick carousel
          $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
  
          // initialization of go to
          $.HSCore.components.HSGoTo.init('.js-go-to');
  
          /* Plugin Paises */
          $("#country_selector").countrySelect({
              preferredCountries: ['ca', 'gb', 'us']
          });
      });
  
      /*Flags*/
      function setCountry(code){
          if(code || code==''){
              var text = jQuery('a[cunt_code="'+code+'"]').html();
              $(".dropdown dt a span").html(text);
          }
      }
      $(document).ready(function() {
          $(".dropdown img.flag").addClass("flagvisibility");
  
          $(".dropdown dt a").click(function() {
              $(".dropdown dd ul").toggle();
          });
  
          $(".dropdown dd ul li a").click(function() {
              //console.log($(this).html())
              var text = $(this).html();
              $(".dropdown dt a span").html(text);
              $(".dropdown dd ul").hide();
              $("#result").html("Selected value is: " + getSelectedValue("country-select"));
          });
  
          function getSelectedValue(id) {
              //console.log(id,$("#" + id).find("dt a span.value").html())
              return $("#" + id).find("dt a span.value").html();
          }
  
          $(document).bind('click', function(e) {
              var $clicked = $(e.target);
              if (! $clicked.parents().hasClass("dropdown"))
                  $(".dropdown dd ul").hide();
          });
  
  
          $("#flagSwitcher").click(function() {
              $(".dropdown img.flag").toggleClass("flagvisibility");
          });
      });
  
  </script>
@endsection