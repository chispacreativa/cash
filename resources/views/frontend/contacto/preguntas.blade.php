@extends('frontend.resources.core')
@section('content')
<!-- Skippy -->
<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
    <div class="container">
        <span class="u-skiplink-text">Skip to main content</span>
    </div>
</a>
<!-- End Skippy -->
@include('frontend.resources.header')

  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    <!-- Hero Section -->
    <div class="gradient-overlay-half-primary-v1 bg-img-hero" style="background-image: url(../../assets/img/1920x800/img14.jpg);">
      <div class="container space-2 space-top-md-5 space-bottom-md-3">
        <div class="w-md-80 w-lg-60 text-center mx-auto">
          <!-- Title -->
          <div class="mb-5">
            <h1 class="display-4 font-size-md-down-5 text-white mb-0">¿Cómo podemos<span class="font-weight-semi-bold">Ayudar?</span></h1>
          </div>
          <!-- End Title -->

          <!-- Input -->
          <form class="input-group input-group-lg input-group-borderless mb-5">
            <div class="input-group-prepend">
              <span class="input-group-text" id="askQuestions">
                <span class="fas fa-search"></span>
              </span>
            </div>
            <input type="search" class="form-control" placeholder="Pregunta aquí" aria-label="Pregunta aquí" aria-describedby="Pregunta aquí">
          </form>
          <!-- End Input -->

          <!-- Text/Links -->
          <p class="text-white-70">
            Temas de ayuda:
            <a class="link-light" href="https://cambicash.com/preguntas-frecuentes#basics">Información básica</a>
            <a class="link-light ml-1" href="https://cambicash.com/preguntas-frecuentes#syncing">Seguridad,</a>
            <a class="link-light ml-1" href="https://cambicash.com/preguntas-frecuentes#account">Cuenta,</a>
            <a class="link-light ml-1" href="https://cambicash.com/preguntas-frecuentes#privacy">Condiciones y Políticas</a>
          </p>
          <!-- End Text/Links -->
        </div>
      </div>
    </div>
    <!-- End Hero Section -->

    <!-- Basic Articles Section -->
    {{-- <div class="container space-2">
      <div class="row">
        <div class="col-lg-6 u-ver-divider u-ver-divider--none-lg mb-9 mb-md-0">
          <!-- Basic Articles -->
          <article class="text-center p-md-8">
            <figure id="featuresSVG1" class="svg-preloader max-width-27 w-100 mx-auto mb-4">
              <img class="js-svg-injector" src="{{asset('svg/illustrations/drawing-man.svg')}}" alt="Image Description"
                   data-parent="#featuresSVG1">
            </figure>

            <div class="mb-4">
              <h2 class="h4">Using Front</h2>
              <p>Find the answer to any question, from the basics all the way to advanced tips and tricks!</p>
            </div>
            <a class="btn btn-soft-primary btn-wide transition-3d-hover" href="#">Browse all article</a>
          </article>
          <!-- End Basic Articles -->
        </div>

        <div class="col-lg-6">
          <!-- Basic Articles -->
          <article class="text-center p-md-8">
            <figure id="featuresSVG2" class="svg-preloader max-width-27 w-100 mx-auto mb-4">
              <img class="js-svg-injector" src="../../assets/svg/illustrations/charts-and-graphs.svg" alt="Image Description"
                   data-parent="#featuresSVG2">
            </figure>

            <div class="mb-4">
              <h2 class="h4">Workspace administration</h2>
              <p>Want to learn more about setting up and managing your team? Look no further!</p>
            </div>
            <a class="btn btn-soft-primary btn-wide transition-3d-hover" href="#">Browse all article</a>
          </article>
          <!-- End Basic Articles -->
        </div>
      </div>
    </div> --}}
    <!-- End Basic Articles Section -->

    <hr class="my-0">

    <!-- FAQ Topics Section -->
    <div class="container space-2 space-md-3">
      <div class="row">
        <div id="stickyBlockStartPoint" class="col-lg-3 mb-7 mb-lg-0">
          <!-- Nav -->
          <nav class="js-sticky-block card border-0 bg-primary"
               data-offset-target="#logoAndNav"
               data-parent="#stickyBlockStartPoint"
               data-sticky-view="lg"
               data-start-point="#stickyBlockStartPoint"
               data-end-point="#stickyBlockEndPoint"
               data-offset-top="24"
               data-offset-bottom="24">
            <ul class="js-scroll-nav list-group list-group-transparent list-group-white list-group-flush list-group-borderless py-3 px-5">
              <li>
                <a class="list-group-item list-group-item-action py-3" href="#basics">Información básica</a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action py-3" href="#syncing">Seguridad</a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action py-3" href="#account">Cuenta</a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action py-3" href="#privacy">Condiciones y Políticas</a>
              </li>
            </ul>
          </nav>
          <!-- End Nav -->
        </div>

        <div class="col-lg-9">
          <div id="basics" class="space-bottom-2">
            <!-- Title -->
            <div class="mb-3">
              <h3 class="text-primary font-weight-semi-bold">Información básica</h3>
            </div>
            <!-- End Title -->

            <!-- Basics Accordion -->
            <div id="basicsAccordion">
              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="basicsHeadingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn p-3"
                            data-toggle="collapse"
                            data-target="#basicsCollapseOne"
                            aria-expanded="true"
                            aria-controls="basicsCollapseOne">
                      Que es Cambicash?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="basicsCollapseOne" class="collapse show"
                     aria-labelledby="basicsHeadingOne"
                     data-parent="#basicsAccordion">
                  <div class="card-body">
                   <div> Cambicash es una Fintech que utiliza la tecnología Blockchain para enviar y recibir transferencias de dinero entre usuarios internos y externos. Orientada al beneficio del cliente mediante la reducción de riesgo y el aumento de la competitividad empresarial con el uso del tipo de cambio.</div>
                   Ofrece una solución para el cambio de divisas basado en la tecnología y eficiencia. De esta manera, brinda un servicio de calidad, seguro y con excelente tasa.
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="basicsHeadingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#basicsCollapseTwo"
                            aria-expanded="false"
                            aria-controls="basicsCollapseTwo">
                      Como funciona Cambicash?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="basicsCollapseTwo" class="collapse"
                     aria-labelledby="basicsHeadingTwo"
                     data-parent="#basicsAccordion">
                  <div class="card-body">
                   <a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#services">1- Lo primero que debes hacer es tener una cuenta con nosotros.</a> 
¿No la tienes aún? ¿Qué esperas? Registrate aqui.

<a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#services">2- Al momento de ingresar en www.Cambicash.com te aparecerá nuestro cotizador:</a>


Recuerda:
Recibimos el dinero en la moneda local del país que envías.
Entregamos el dinero en la moneda local del país que recibe.
NO cobramos NADA más por el envío.
<a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#services">3.- Completa tus datos bancarios O cartera bitcoin y los de tu beneficiario donde desees que llegue el dinero.</a>

Recuerda: Solo enviamos a cuentas bancarias o monedero bitcoin, no hacemos retiros en efectivo.
<a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#services">4.-Hora de elegir el método de tu preferencia!</a>
Te aparecerán las opciones que tenemos disponibles para ti:


Recuerda: No aceptamos pagos de terceros, por lo que los mismos serán rechazados sin excepción.

<a class="list-group-item list-group-item-action font-weight-medium mb-2" href="#services">5- Te aparecerán nuestros datos bancarios o billetera de bitcoin para que realices tu pago, una vez tengas el pago listo no olvides subir el comprobante para que el sistema valide tu transferencia.</a>

Una vez subido el comprobante de la transferencia nosotros nos encargaremos de tu envío y te mandaremos un correo electonico cuando se complete la opeación.
4.  Hora de elegir el método de Pago!
Te aparecerán las opciones que tenemos disponibles para ti:


Recuerda: No aceptamos pagos de terceros, por lo que los mismos serán rechazados sin excepción.

5- Te aparecerán nuestros datos bancarios para que realices tu pago, una vez tengas el pago listo no olvides subir el comprobante para que el sistema valide tu transferencia.

Una vez subido el comprobante de la transferencia nosotros nos encargaremos de tu envío 💪😄

¿Qué esperas para hacer tu envío?
<br>
                                    <div class="text-center">
                                        <button type="submit" class="cotizadorButtonToSubmit-g66" >Iniciar Intercambio</button>
                                    </div>
                                        
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="basicsHeadingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#basicsCollapseThree"
                            aria-expanded="false"
                            aria-controls="basicsCollapseThree">
                      ¿Cuáles son los horarios de atención?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="basicsCollapseThree" class="collapse"
                     aria-labelledby="basicsHeadingThree"
                     data-parent="#basicsAccordion">
                  <div class="card-body">
                   Los horarios de atención son:

Lunes a Jueves desde 09:00 am hasta 18:00 pm y Viernes hasta las 17:00 pm
(A excepción de días feriados).

Nuestra cede principal de operaciones se encuentra en Lima Perú, por lo qué nuestro horario se debe a dicho país. 

Aviso: Todas las transferencias realizadas fuera de horario laboral, quedarán pendientes al próximo día hábil en Perú. Avisaremos en todo momento cuando no estaremos disponibles para tu conveniencia..
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="basicsHeadingFour">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#basicsCollapseFour"
                            aria-expanded="false"
                            aria-controls="basicsCollapseFour">
                    ¿Se puede operar en efectivo?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="basicsCollapseFour" class="collapse"
                     aria-labelledby="basicsHeadingFour"
                     data-parent="#basicsAccordion">
                  <div class="card-body">
                    No aceptamos dinero en efectivo. Solamente trabajamos con dinero bancarizado recibido mediante transferencia bancaria. Esto nos permite, entre otras cosas, que puedas seguir el rastro a tu dinero en todo momento.
                  </div>
                </div>
              </div>
              <!-- End Card -->
            </div>
            <!-- End Basics Accordion -->
          </div>

          <hr class="my-0">

          <div id="syncing" class="space-2">
            <!-- Title -->
            <div class="mb-3">
              <h3 class="text-primary font-weight-semi-bold">Seguridad</h3>
            </div>
            <!-- End Title -->

            <!-- Syncing Accordion -->
            <div id="syncingAccordion">
              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="syncingHeadingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn p-3"
                            data-toggle="collapse"
                            data-target="#syncingCollapseOne"
                            aria-expanded="false"
                            aria-controls="syncingCollapseOne">
                      ¿Qué tan seguro es usar Cambicash para cambiar dinero o tus bitcoins online?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="syncingCollapseOne" class="collapse show"
                     aria-labelledby="syncingHeadingOne"
                     data-parent="#syncingAccordion">
                  <div class="card-body">
                    
Nuestras operaciones están enmarcadas dentro del marco regulatorio pertinente establecido por la Superintendencia de Banca y Seguros (SBS).
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="syncingHeadingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#syncingCollapseTwo"
                            aria-expanded="false"
                            aria-controls="syncingCollapseTwo">
                      ¿Qué seguridad tengo sobre mis fondos y como  me aseguro que llegara a su destino?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="syncingCollapseTwo" class="collapse"
                     aria-labelledby="syncingHeadingTwo"
                     data-parent="#syncingAccordion">
                  <div class="card-body">
                    Una vez hecha una operación, nuestra plataforma la procesa automáticamente generando los movimientos de fondos a las cuentas sin intervención humana alguna. Tan pronto se reciben tus fondos y los de la contraparte, se instruye el movimiento bancario para depositarte. Todo este proceso lo puedes seguir a través de nuestra web y te mantenemos informado a través de correos electrónicos.
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="syncingHeadingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#syncingCollapseThree"
                            aria-expanded="false"
                            aria-controls="syncingCollapseThree">
                      ¿si tengo  algún problema en la operación con quien me comunico?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="syncingCollapseThree" class="collapse"
                     aria-labelledby="syncingHeadingThree"
                     data-parent="#syncingAccordion">
                  <div class="card-body">
                    Tenemos una apoyo directo y on line por parte del equipo durante las horas en que operamos. Encontrarás un link en la pantalla de operaciones para ponerte en contacto con nosotros por el medio que te sea más cómodo: correo, teléfono, whatsapp y live chat.
                  </div>
                </div>
              </div>
              <!-- End Card -->
            </div>
            <!-- End Syncing Accordion -->
          </div>

          <hr class="my-0">

          <div id="account" class="space-2">
            <!-- Title -->
            <div class="mb-3">
              <h3 class="text-primary font-weight-semi-bold">Cuenta</h3>
            </div>
            <!-- End Title -->

            <!-- Account Accordion -->
            <div id="accountAccordion">
              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="accountHeadingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn p-3"
                            data-toggle="collapse"
                            data-target="#accountCollapseOne"
                            aria-expanded="false"
                            aria-controls="accountCollapseOne">
                       ¿Olvidaste tu contraseña?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="accountCollapseOne" class="collapse show"
                     aria-labelledby="accountHeadingOne"
                     data-parent="#accountAccordion">
                  <div class="card-body">
                    Puedes crear una nueva contraseña dándole click a “olvidé mi contraseña“. Recibirás un correo electrónico con las instrucciones a seguir para restablecer tu contraseña.
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="accountHeadingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#accountCollapseTwo"
                            aria-expanded="false"
                            aria-controls="accountCollapseTwo">
                      ¿Qué es mi historial de pagos?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="accountCollapseTwo" class="collapse"
                     aria-labelledby="accountHeadingTwo"
                     data-parent="#accountAccordion">
                  <div class="card-body">
                   Todas tus transacciones quedan registradas y ligadas a tu cuenta. Puedes verificar el avance de cada transacción que has colocado. Asimismo, podrás seleccionar transacciones frecuentes para poder realizarlas con más rapidez.
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="accountHeadingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#accountCollapseThree"
                            aria-expanded="false"
                            aria-controls="accountCollapseThree">
                      ¿Qué datos necesito como usuario para realizar una cotización?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="accountCollapseThree" class="collapse"
                     aria-labelledby="accountHeadingThree"
                     data-parent="#accountAccordion">
                  <div class="card-body">
                    Para poder enviar dinero con nosotros, debes tener una cuenta con nosotros.
¿Aún no tienes cuentas? Sencillo! click aquí para registrarte 📝.

A continuación debes llenar tus datos personales:
Nombres, Apellidos, Identificación, dirección, teléfono, numero de cuenta, etc...

Recuerda: Llenar la información lo más completo posible, esto nos ayuda a proteger tu cuenta y evitar retrasos con tus envíos.
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="accountHeadingFour">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#accountCollapseFour"
                            aria-expanded="false"
                            aria-controls="accountCollapseFour">
                     ¿Donde y como le llega el dinero a mi beneficiario?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="accountCollapseFour" class="collapse"
                     aria-labelledby="accountHeadingFour"
                     data-parent="#accountAccordion">
                  <div class="card-body">
                    Todos nuestros envíos llegan directamente a la cuenta bancaria de tu beneficiarioo billetera de bitcoin.
Nuestras transferencias son NACIONALES, ya que poseemos cuentas en todas las rutas del destino.
Recuerda: Recibimos el pago en la moneda local de tu país y hacemos llegar el dinero en la moneda local del país de destino.

Acá un pequeño ejemplo:

Envías 100.000 soles Desde Perú a Venezuela, Recibiremos tu pago desde Perú a nuestra cuenta en el mismo país y haremos llegar el dinero a Venezuela desde una cuenta Venezolana, nunca haremos transferencias desde un banco que no pertenezca al mismo país.
                  </div>
                </div>
              </div>
              <!-- End Card -->
            </div>
            <!-- End Account Accordion -->
          </div>

          <hr class="my-0">

          <div id="privacy" class="space-top-2">
            <!-- Title -->
            <div class="mb-3">
              <h3 class="text-primary font-weight-semi-bold">Condiciones y Políticas</h3>
            </div>
            <!-- End Title -->

            <!-- Privacy Accordion -->
            <div id="privacyAccordion">
              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="privacyHeadingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn p-3"
                            data-toggle="collapse"
                            data-target="#privacyCollapseOne"
                            aria-expanded="false"
                            aria-controls="privacyCollapseOne">
                      Límite de envío

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="privacyCollapseOne" class="collapse show"
                     aria-labelledby="privacyHeadingOne"
                     data-parent="#privacyAccordion">
                  <div class="card-body">
                    Por medidas de seguridad, El monto máximo diario por depósitos en efectivo es de 1000 USD validado la moneda local.
Para el caso de Transferencias Electrónicas el limite es hasta los 20.000 USD diarios y 30.000 USD mensuales representados en tu moneda local.
Al llegar al limite tus envíos automáticamente  cambiaran su estado a "Revisión" o "Rechazado" y nos contactaremos contigo mediante correo electrónico para darte una solución personalizada 
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="privacyHeadingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#privacyCollapseTwo"
                            aria-expanded="false"
                            aria-controls="privacyCollapseTwo">
                      ¿En qué casos puedo solicitar una devolución?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="privacyCollapseTwo" class="collapse"
                     aria-labelledby="privacyHeadingTwo"
                     data-parent="#privacyAccordion">
                  <div class="card-body">
                    1- Envío rechazado por parte del Banco receptor.
Si los datos del beneficiario están correctos y el banco no acepta la transferencia se podrá solicitar la devolución del dinero.

2- Datos del beneficiario incorrectos.
Si la transferencia fue rechazada por el banco emisor, y el cliente decide no volver a realizar el envío, se procederá a devolver los fondos en cuanto al dinero sea devuelto a la cuenta de CAMBICASH.COM de origen.

                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="privacyHeadingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#privacyCollapseThree"
                            aria-expanded="false"
                            aria-controls="privacyCollapseThree">
                      ¿Cuánto tiempo tarda la devolución?

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="privacyCollapseThree" class="collapse"
                     aria-labelledby="privacyHeadingThree"
                     data-parent="#privacyAccordion">
                  <div class="card-body">
                    Procesamos las devoluciones desde el momento en que el cliente lo solicita y envía la información correspondiente. La demora corresponde a la demora de envío al país correspondiente.
                  </div>
                </div>
              </div>
              <!-- End Card -->

              <!-- Card -->
              <div class="card mb-3">
                <div class="card-header card-collapse" id="privacyHeadingFour">
                  <h5 class="mb-0">
                    <button class="btn btn-link btn-block d-flex justify-content-between card-btn collapsed p-3"
                            data-toggle="collapse"
                            data-target="#privacyCollapseFour"
                            aria-expanded="false"
                            aria-controls="privacyCollapseFour">
                      Comprobante Depósitos en Efectivo 

                      <span class="card-btn-arrow">
                        <span class="fas fa-arrow-down small"></span>
                      </span>
                    </button>
                  </h5>
                </div>
                <div id="privacyCollapseFour" class="collapse"
                     aria-labelledby="privacyHeadingFour"
                     data-parent="#privacyAccordion">
                  <div class="card-body">
                    Si deseas hacer un depósito en efectivo debes seguir los siguientes pasos: 

Cotiza el monto que quieres enviar, este no puede ser mayor a 1.000 Dólares.
Con la información bancaria y respetando el límite, dirígete al banco a realizar el depósito.
Adjunta el Comprobante de Pago
Sólo el documento que te señalamos es válido para que te confirmemos el depósito, sino sera rechazado. 
                  </div>
                </div>
              </div>
              <!-- End Card -->
            </div>
            <!-- End Privacy Accordion -->
          </div>
        </div>
      </div>
    </div>
    <!-- End FAQ Topics Section -->

    <!-- Sticky Block End Point -->
    <div id="stickyBlockEndPoint"></div>

    <hr class="my-0">

    <!-- Contacts Section -->
    {{-- <div class="container">
      <div class="row space-2">
        <div class="col-lg-6 u-ver-divider u-ver-divider--none-lg mb-7 mb-lg-0">
          <!-- Contacts Info -->
          <div class="media pr-lg-9">
            <figure id="icon15" class="svg-preloader ie-height-56 w-100 max-width-8 mr-4">
              <img class="js-svg-injector" src="../../assets/svg/icons/icon-15.svg" alt="SVG"
                   data-parent="#icon15">
            </figure>
            <div class="media-body">
              <h3 class="h5">Can't find your answer?</h3>
              <p class="mb-1">We want to answer all of your queries. Get in touch and we'll get back to you as soon as we can.</p>
              <a class="font-size-1" href="#">Email us <span class="fas fa-angle-right align-middle ml-2"></span></a>
            </div>
          </div>
          <!-- End Contacts Info -->
        </div>

        <div class="col-lg-6">
          <!-- Contacts Info -->
          <div class="media pl-lg-9">
            <figure id="icon4" class="svg-preloader ie-height-56 w-100 max-width-8 mr-4">
              <img class="js-svg-injector" src="../../assets/svg/icons/icon-4.svg" alt="SVG"
                   data-parent="#icon4">
            </figure>
            <div class="media-body">
              <h3 class="h5">Technical questions</h3>
              <p class="mb-1">Have some technical questions? Hit us on community page or just say hello.</p>
              <a class="font-size-1" href="#">Open ticket <span class="fas fa-angle-right align-middle ml-2"></span></a>
            </div>
          </div>
          <!-- End Contacts Info -->
        </div>
      </div>

      <hr class="my-0">
    </div> --}}
    <!-- End Contacts Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  @include('frontend.resources.footer')

  <!-- ========== SECONDARY CONTENTS ========== -->
  @guest
  @include('frontend.auth.login_side')
  @endguest
  @auth
  @include('frontend.dashboard.asidebar')
  @endauth
  <!-- ========== END SECONDARY CONTENTS ========== -->
  
  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed"
      data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
      <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->
  
  @endsection
  
  @section('script')
  <!-- JS Plugins Init. -->
  <script>
      $(window).on('load', function () {
          // initialization of HSMegaMenu component
          $('.js-mega-menu').HSMegaMenu({
              event: 'hover',
              pageContainer: $('.container'),
              breakpoint: 767.98,
              hideTimeOut: 0
          });
  
          // initialization of svg injector module
          $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
      });
  
      $(document).on('ready', function () {
          // initialization of header
          $.HSCore.components.HSHeader.init($('#header'));
  
          // initialization of unfold component
          $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
              afterOpen: function () {
                  $(this).find('input[type="search"]').focus();
              }
          });
  
          // initialization of malihu scrollbar
          $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));
  
          // initialization of forms
          $.HSCore.components.HSFocusState.init();
  
          // initialization of form validation
          $.HSCore.components.HSValidation.init('.js-validate', {
              rules: {
                  confirmPassword: {
                      equalTo: '#signupPassword'
                  }
              }
          });
  
          // initialization of show animations
          $.HSCore.components.HSShowAnimation.init('.js-animation-link');
  
          // initialization of fancybox
          $.HSCore.components.HSFancyBox.init('.js-fancybox');
  
          // initialization of text animation (typing)
          var typed = new Typed(".u-text-animation.u-text-animation--typing", {
              strings: ["Costos Altos", "Costos Ocultos", "Malos Servicios"],
              typeSpeed: 60,
              loop: true,
              backSpeed: 25,
              backDelay: 1500
          });
  
          // initialization of slick carousel
          $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
  
          // initialization of go to
          $.HSCore.components.HSGoTo.init('.js-go-to');
  
          /* Plugin Paises */
          $("#country_selector").countrySelect({
              // defaultCountry: "jp",
              // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
              // responsiveDropdown: true,
              preferredCountries: ['ca', 'gb', 'us']
          });
      });
  
      // $('#basic').flagStrap();
  
      // $('.select-country').flagStrap({
      //     countries: {
      //         "US": "USD",
      //         "AU": "AUD",
      //         "CA": "CAD",
      //         "SG": "SGD",
      //         "GB": "GBP",
      //     },
      //     buttonSize: "btn-sm",
      //     buttonType: "btn-info",
      //     labelMargin: "10px",
      //     scrollable: false,
      //     scrollableHeight: "350px"
      // });
  
      // $('#advanced').flagStrap({
      //     buttonSize: "btn-lg",
      //     buttonType: "btn-primary",
      //     labelMargin: "20px",
      //     scrollable: false,
      //     scrollableHeight: "350px"
      // });
  
  
      /*Flags*/
      function setCountry(code){
          if(code || code==''){
              var text = jQuery('a[cunt_code="'+code+'"]').html();
              $(".dropdown dt a span").html(text);
          }
      }
      $(document).ready(function() {
          $(".dropdown img.flag").addClass("flagvisibility");
  
          $(".dropdown dt a").click(function() {
              $(".dropdown dd ul").toggle();
          });
  
          $(".dropdown dd ul li a").click(function() {
              //console.log($(this).html())
              var text = $(this).html();
              $(".dropdown dt a span").html(text);
              $(".dropdown dd ul").hide();
              $("#result").html("Selected value is: " + getSelectedValue("country-select"));
          });
  
          function getSelectedValue(id) {
              //console.log(id,$("#" + id).find("dt a span.value").html())
              return $("#" + id).find("dt a span.value").html();
          }
  
          $(document).bind('click', function(e) {
              var $clicked = $(e.target);
              if (! $clicked.parents().hasClass("dropdown"))
                  $(".dropdown dd ul").hide();
          });
  
  
          $("#flagSwitcher").click(function() {
              $(".dropdown img.flag").toggleClass("flagvisibility");
          });
      });
  
  </script>
  @endsection