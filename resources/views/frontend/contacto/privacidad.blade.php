
@extends('frontend.resources.core')
@section('content')
<!-- Skippy -->
<a id="skippy" class="sr-only sr-only-focusable u-skippy" href="#content">
    <div class="container">
        <span class="u-skiplink-text">Skip to main content</span>
    </div>
</a>
<!-- End Skippy -->

@include('frontend.resources.header')
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN ========== -->
  <main id="content" role="main" class="svg-preloader">
    <!-- Privacy Section -->
    <div class="container space-top-2 space-top-md-4 space-bottom-1 overflow-hidden">
      <div class="w-lg-80 mx-lg-auto">
        <div class="card shadow-sm">
          <!-- Hero -->
          <div class="card-header position-relative gradient-half-primary-v1 space-top-2 space-bottom-3 rounded-top-pseudo px-7 px-md-9">
            <h1 class="text-white font-weight-semi-bold">Política &amp; Privacidad</h1>
            <p class="text-white-70">Última modificación: 27 de marzo de 2019 (<a class="text-light" href="#"> ver versiones archivadas</a>)</p>
          </div>
          <!-- End Hero -->

          <!-- Content -->
          <div class="card-body p-7 p-md-9">
            <div class="mb-7">
              <!-- Title -->
              <div class="mb-3">
                <h3 class="h5 text-primary font-weight-semi-bold">Introducción</h3>
              </div>
              <!-- End Title -->

              <!-- Text -->
              <p>Proteger su información, privacidad y datos financieros es muy importante para Cambicash S.A.C,
(¨Cambicash”, “La Empresa” o “nosotros”)

              <p>CAMBICASH ha alineado su política de privacidad de acuerdo a la Ley N° 29733, el Decreto Supremo No. 003-2013-JUS por el que se aprueba su Reglamento y la Directiva de Seguridad de la Información (en adelante “Ley de Protección de Datos personales”).</p>
              <p>Esta Política (en conjunto con los Términos y Condiciones de uso y cualquier documento referido en éstos),
<p>define las bases sobre las cuales cualquier información suya recolectada será procesada por nosotros.</p>
<p>Por favor lea detalladamente para entender la información que recolectamos sobre usted, como usamos dicha información y las circunstancias bajo las cuales la compartiremos con terceras partes.</p>
<p>Al visitar nuestra página web usted acepta y consiente las prácticas descritas en esta Política.</p>
<p>Para propósitos de las leyes asociadas a la Protección de Datos Personales, el controlador de la información</p>
es Cambicash S.A.C. con RUC 20601703456, con domicilio en Av. junin1232, Piso 8, Miraflores, Lima,
Perú.</p>
<p>Por información personal nos referimos a información que se encuentre asociada con una persona natural en específico y que puede ser razonablemente utilizada para identificar a esta persona.</p>
<p>La información personal no incluye a la información que haya pasado por un proceso de anonimización y que por ende no puede ser utilizada para identificar a una persona.</p>
Esta Política de Privacidad abarca cómo recolectamos, utilizamos y los casos en los que compartimos su
información personal.</p>
              <!-- End Text -->
            </div>

            <div class="mb-7">
              <!-- Title -->
              <div class="mb-3">
                <h3 class="h5 text-primary font-weight-semi-bold">1. Conceptos</h3>
              </div>
              <!-- End Title -->

              <!-- Text -->
              <p>Todos aquellos conceptos recogidos en esta política de privacidad, deben ser entendidos de acuerdo a lo establecido en la Ley de Protección de Datos personales.</p>
<p>Aplicaciones: portal en Internet Cambicash.com y/o  aplicación móvil denominada Cambicash.</p>
<p>Cliente: usuarios registrados en las aplicaciones de Cambicash.</p>
              
              <!-- End Text -->
            </div>

            <div class="mb-7">
              <!-- Title -->
              <div class="mb-3">
                <h3 class="h5 text-primary font-weight-semi-bold">2. Tratamiento de información</h3>
              </div>
              <!-- End Title -->

              <!-- Text -->
              <p>CAMBICASH se centra en mantener la seguridad de información, confidencialidad y privacidad de los datos personales que EL CLIENTE hubiese entregado voluntariamente. Por ello, CAMBICASH no divulgará dicha información, salvo que esta sea exigida por la normativa aplicable.</p>
              <p>En Cambicash se ha establecido las condiciones necesarias para dar un adecuado tratamiento a los datos personales suministrados por EL CLIENTE.</p>
            
              <!-- End Text -->
            </div>

            <div class="mb-7">
              <!-- Title -->
              <div class="mb-3">
                <h3 class="h5 text-primary font-weight-semi-bold">3. Tu Seguridad</h3>
              </div>
              <!-- End Title -->

              <!-- Text -->
              <p>No está permitido el uso de LAS APLICACIONES para actividades que impliquen infringir o violar la normativa en general.</p>
              <p>No está permitido realizar acciones de “hacking” (actos que involucren la eliminación, apropiación, interferencia, daño o la extracción parcial o total no autorizada de información, dinero o bienes de propiedad de terceros registrados en Internet con cualquier propósito, incluyendo, pero sin limitarse, el de causar daño, u obtener un beneficio económico en favor de aquel que realice el acto en favor de terceros), a través de LAS APLICACIONES.</p>
               <p>CAMBICASH dispone sus mejores esfuerzos en habilitar las medidas de seguridad detalladas y no se hace responsable por las actividades de “hacking” que puedan realizar el/los CLIENTE/s o Usuario/s de internet en detrimento de la cuenta de otro CLIENTE ni por la disposición de información y fondos no autorizada, producto de actividades de “hacking”.</p>
               </p>Está estrictamente prohibido desarrollar mecanismos que intenten defraudar LAS APLICACIONES. Asimismo, queda terminantemente prohibido, utilizar LAS APLICACIONES y medios informáticos de CAMBICASH como medio de defraudación a terceros.</p>
               <p>El aprovechamiento de los Servicios y Contenidos de LAS APLICACIONES es exclusiva responsabilidad del CLIENTE, quien en todo caso deberá servirse de ellos acorde a las funcionalidades permitidas en las propias APLICACIONES y a los usos autorizados en los Términos y Condiciones de Uso y Política de Privacidad. EL CLIENTE se obliga a utilizarlos de modo tal que no atenten contra las normas de uso y convivencia en Internet, las leyes de la República del Perú y la legislación vigente en el país en que el CLIENTE se encuentre al usarlos, las buenas costumbres, la dignidad de la persona y los derechos de terceros. LAS APLICACIONES son para el uso personal y del CLIENTE, por lo que no podrá comercializar de manera alguna los Servicios y Contenidos de LAS APLICACIONES.</p>
               <p>Ni CAMBICASH, sus accionistas, gerentes, funcionarios, ejecutivos, empleados, agentes, contratistas u otras personas naturales o jurídicas vinculadas, asumirán la responsabilidad por ningún daño y/o perjuicio, a consecuencia de inexactitudes, errores tipográficos y cambios o mejoras que se realicen periódicamente a los Servicios y Contenidos, por el uso de LAS APLICACIONES u otros websites vinculados a través de ésta. Esta condición no será aplicable cuando la legislación peruana así lo establezca.</p>
               <p>CAMBICASH no se hace responsables por los daños que el CLIENTE pueda sufrir como consecuencia del uso de LAS APLICACIONES. </p>
                <p>REXTIE no se hace responsable, ni respaldan la información que aparezca en LAS APLICACIONES y que no sea de su directa autoría.</p>
                <p>CAMBICASH no se hace responsable por los virus que puedan estar presentes en LAS APLICACIONES y/o en los mensajes que se remitan, en consecuencia se encuentra liberado desde ya de toda responsabilidad por los daños que los mismos puedan causar a el/los CLIENTE/s.</p>
                <!-- End Text -->
            </div>

            <div class="mb-7">
              <!-- Title -->
              <div class="mb-3">
                <h3 class="h5 text-primary font-weight-semi-bold">4. Otros</h3>
              </div>
               <!-- Text -->
              <p>En todo lo no mencionado en la presente Política de Privacidad, se aplica supletoriamente los Términos y Condiciones a lo que sea aplicable y las normas que regulan la materia.</p>
              <p>En caso de cualquier consulta sobre la presente política, sírvanse enviar un correo a info@cambicash.com</p>
                <!-- End Text -->
            </div>

            <div class="mb-7">
              <!-- Title -->
              <div class="mb-3">
                <h3 class="h5 text-primary font-weight-semi-bold">5. Actualización de Política</h3>
              </div>
               <!-- Text -->
              <!-- End Title -->
              <!-- Text -->
              <p>CAMBICASH se preocupa por la seguridad de información, por tanto, estará en constante actualización de medidas de seguridad a favor de la mitigación de riesgos, y por ende, esto puede actualizar la presente política en cualquier momento. Toda modificación entrará en vigencia y tendrá efectos frente a los terceros desde su publicación en LAS APLICACIONES.</p>
              <p>Finalmente, CAMBICASH realizará los esfuerzos necesarios para enviar una notificación de dicha actualización a EL CLIENTE que se encuentre operando a través de su correo registrado o haciendo su publicación en LAS APLICACIONES, salvo la actualización se realice en mérito de salvaguardar la seguridad de información de EL CLIENTE, para lo cual no emitirá un aviso previo a su actualización.</p>  
              
                
              <!-- End Text -->
            </div>
          </div>
          <!-- End Content -->
        </div>
      </div>
    </div>
    <!-- End Privacy Section -->

    <!-- SVG Background Shapes - Right Side -->
    <div class="w-35 content-centered-y right-0 z-index-n1 mt-n9">
      <figure class="ie-bg-elements-4">
        <img class="js-svg-injector" src="../../assets/svg/components/bg-elements-4.svg" alt="Image Description"
             data-parent="#content">
      </figure>
    </div>
    <!-- End SVG Background Shapes - Right Side -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  @include('frontend.resources.footer')
  <!-- ========== END FOOTER ========== -->

  <!-- ========== SECONDARY CONTENTS ========== -->
  <!-- Account Sidebar Navigation -->
  @guest
  @include('frontend.auth.login_side')
  @endguest
  @auth
  @include('frontend.dashboard.asidebar')
  @endauth
  <!-- End Account Sidebar Navigation -->
  <!-- ========== END SECONDARY CONTENTS ========== -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 15, "right": 15 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->
  @endsection

  @section('script')
  <!-- JS Plugins Init. -->
  <script>
      $(window).on('load', function () {
          // initialization of HSMegaMenu component
          $('.js-mega-menu').HSMegaMenu({
              event: 'hover',
              pageContainer: $('.container'),
              breakpoint: 767.98,
              hideTimeOut: 0
          });
  
          // initialization of svg injector module
          $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
      });
  
      $(document).on('ready', function () {
          // initialization of header
          $.HSCore.components.HSHeader.init($('#header'));
  
          // initialization of unfold component
          $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
              afterOpen: function () {
                  $(this).find('input[type="search"]').focus();
              }
          });
  
          // initialization of malihu scrollbar
          $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));
  
          // initialization of forms
          $.HSCore.components.HSFocusState.init();
  
          // initialization of form validation
          $.HSCore.components.HSValidation.init('.js-validate', {
              rules: {
                  confirmPassword: {
                      equalTo: '#signupPassword'
                  }
              }
          });
  
          // initialization of show animations
          $.HSCore.components.HSShowAnimation.init('.js-animation-link');
  
          // initialization of fancybox
          $.HSCore.components.HSFancyBox.init('.js-fancybox');
  
          // initialization of text animation (typing)
          var typed = new Typed(".u-text-animation.u-text-animation--typing", {
              strings: ["Costos Altos", "Costos Ocultos", "Malos Servicios"],
              typeSpeed: 60,
              loop: true,
              backSpeed: 25,
              backDelay: 1500
          });
  
          // initialization of slick carousel
          $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
  
          // initialization of go to
          $.HSCore.components.HSGoTo.init('.js-go-to');
  
          /* Plugin Paises */
          $("#country_selector").countrySelect({
              // defaultCountry: "jp",
              // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
              // responsiveDropdown: true,
              preferredCountries: ['ca', 'gb', 'us']
          });
      });
  
      // $('#basic').flagStrap();
  
      // $('.select-country').flagStrap({
      //     countries: {
      //         "US": "USD",
      //         "AU": "AUD",
      //         "CA": "CAD",
      //         "SG": "SGD",
      //         "GB": "GBP",
      //     },
      //     buttonSize: "btn-sm",
      //     buttonType: "btn-info",
      //     labelMargin: "10px",
      //     scrollable: false,
      //     scrollableHeight: "350px"
      // });
  
      // $('#advanced').flagStrap({
      //     buttonSize: "btn-lg",
      //     buttonType: "btn-primary",
      //     labelMargin: "20px",
      //     scrollable: false,
      //     scrollableHeight: "350px"
      // });
  
  
      /*Flags*/
      function setCountry(code){
          if(code || code==''){
              var text = jQuery('a[cunt_code="'+code+'"]').html();
              $(".dropdown dt a span").html(text);
          }
      }
      $(document).ready(function() {
          $(".dropdown img.flag").addClass("flagvisibility");
  
          $(".dropdown dt a").click(function() {
              $(".dropdown dd ul").toggle();
          });
  
          $(".dropdown dd ul li a").click(function() {
              //console.log($(this).html())
              var text = $(this).html();
              $(".dropdown dt a span").html(text);
              $(".dropdown dd ul").hide();
              $("#result").html("Selected value is: " + getSelectedValue("country-select"));
          });
  
          function getSelectedValue(id) {
              //console.log(id,$("#" + id).find("dt a span.value").html())
              return $("#" + id).find("dt a span.value").html();
          }
  
          $(document).bind('click', function(e) {
              var $clicked = $(e.target);
              if (! $clicked.parents().hasClass("dropdown"))
                  $(".dropdown dd ul").hide();
          });
  
  
          $("#flagSwitcher").click(function() {
              $(".dropdown img.flag").toggleClass("flagvisibility");
          });
      });
  
  </script>
  @endsection