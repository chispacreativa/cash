@extends('frontend.resources.core')
@section('content')
<!-- ========== HEADER ========== -->
<header id="header" class="u-header u-header--bg-transparent u-header--abs-top">
  <div class="u-header__section">
    <div id="logoAndNav" class="container-fluid">
      <!-- Nav -->
      <nav class="navbar navbar-expand u-header__navbar">
        <!-- White Logo -->
        <a class="d-none d-lg-flex navbar-brand u-header__navbar-brand u-header__navbar-brand-center u-header__navbar-brand-text-white" href="{{url('/')}}" aria-label="Front">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="46px" height="46px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
                <path fill="#3F7DE0" opacity=".65" d="M23,41L23,41c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18h11.3C38,5,41,8,41,11.7V23C41,32.9,32.9,41,23,41z" />
                <path class="fill-info" opacity=".5" d="M28,35.9L28,35.9c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18l11.3,0C43,0,46,3,46,6.6V18C46,27.9,38,35.9,28,35.9z" />
                <path class="fill-primary" opacity=".7" d="M18,46L18,46C8,46,0,38,0,28v0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6V28C35.9,38,27.9,46,18,46z" />
                <text font-style="normal" font-weight="bold" xml:space="preserve" text-anchor="start" font-family="Helvetica, Arial, sans-serif" font-size="24" id="svg_5" y="34.891162" x="12.827588" fill-opacity="null" stroke-opacity="null" stroke-width="0" stroke="null" fill="#ffffff">C</text>
            </svg>
          <span class="u-header__navbar-brand-text">Cash</span>
        </a>
        <!-- End White Logo -->
        <!-- Default Logo -->
        <a class="d-flex d-lg-none navbar-brand u-header__navbar-brand u-header__navbar-brand-center u-header__navbar-brand-collapsed" href="{{url('/')}}" aria-label="Front">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="46px" height="46px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
                <path fill="#3F7DE0" opacity=".65" d="M23,41L23,41c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18h11.3C38,5,41,8,41,11.7V23C41,32.9,32.9,41,23,41z" />
                <path class="fill-info" opacity=".5" d="M28,35.9L28,35.9c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18l11.3,0C43,0,46,3,46,6.6V18C46,27.9,38,35.9,28,35.9z" />
                <path class="fill-primary" opacity=".7" d="M18,46L18,46C8,46,0,38,0,28v0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6V28C35.9,38,27.9,46,18,46z" />
                <text font-style="normal" font-weight="bold" xml:space="preserve" text-anchor="start" font-family="Helvetica, Arial, sans-serif" font-size="24" id="svg_5" y="34.891162" x="12.827588" fill-opacity="null" stroke-opacity="null" stroke-width="0" stroke="null" fill="#ffffff">C</text>
            </svg>
          <span class="d-inline-block u-header__navbar-brand-text">Cash</span>
        </a>
        <!-- End Default Logo -->
      </nav>
      <!-- End Nav -->
    </div>
  </div>
</header>
<!-- ========== END HEADER ========== -->

<!-- ========== MAIN ========== -->
<main id="content" role="main">
  <!-- Form -->
  <div class="d-flex align-items-center position-relative height-lg-100vh">
    <div class="col-lg-5 col-xl-4 d-none d-lg-flex align-items-center gradient-half-primary-v1 height-lg-100vh px-0">
      <div class="w-100 p-5">
        <!-- SVG Quote -->
        <figure class="text-center mb-5 mx-auto">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px"
             viewBox="0 0 8 8" style="enable-background:new 0 0 8 8;" xml:space="preserve">
            <path class="fill-white" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
              C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
              c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
              C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z"/>
          </svg>
        </figure>
        <!-- End SVG Quote -->

        <!-- Testimonials Carousel Main -->
        <div id="testimonialsNavMain" class="js-slick-carousel u-slick mb-4"
             data-infinite="true"
             data-autoplay="true"
             data-speed="5000"
             data-fade="true"
             data-nav-for="#testimonialsNavPagination">
          <div class="js-slide">
            <!-- Testimonials -->
            <div class="w-md-80 w-lg-60 text-center mx-auto">
              <blockquote class="h5 text-white font-weight-normal mb-4">The template is really nice and offers quite a large set of options. Thank you!</blockquote>
              <h1 class="h6 text-white-70">Maria Muszynska, Google</h1>
            </div>
            <!-- End Testimonials -->
          </div>

          <div class="js-slide">
            <!-- Testimonials -->
            <div class="w-md-80 w-lg-60 text-center mx-auto">
              <blockquote class="h5 text-white font-weight-normal mb-4">It's beautiful and the coding is done quickly and seamlessly. Keep it up!</blockquote>
              <h2 class="h6 text-white-70">James Austin, Slack</h2>
            </div>
            <!-- End Testimonials -->
          </div>

          <div class="js-slide">
            <!-- Testimonials -->
            <div class="w-md-80 w-lg-60 text-center mx-auto">
              <blockquote class="h5 text-white font-weight-normal mb-4">I love Front! I love the ease of use, I love the fact that I have total creative freedom...</blockquote>
              <h3 class="h6 text-white-70">Charlotte Moore, Amazon</h3>
            </div>
            <!-- End Testimonials -->
          </div>
        </div>
        <!-- End Testimonials Carousel Main -->

        <!-- Testimonials Carousel Pagination -->
        <div id="testimonialsNavPagination" class="js-slick-carousel u-slick u-slick--transform-off u-slick--pagination-modern mx-auto"
             data-infinite="true"
             data-autoplay="true"
             data-speed="5000"
             data-center-mode="true"
             data-slides-show="3"
             data-is-thumbs="true"
             data-focus-on-select="true"
             data-nav-for="#testimonialsNavMain">
          <div class="js-slide">
            <div class="u-avatar mx-auto">
              <img class="img-fluid rounded-circle" src="{{ asset('img/100x100/img1.jpg')}}" alt="">
            </div>
          </div>

          <div class="js-slide">
            <div class="u-avatar mx-auto">
              <img class="img-fluid rounded-circle" src="{{ asset('img/100x100/img3.jpg')}}" alt="">
            </div>
          </div>

          <div class="js-slide">
            <div class="u-avatar mx-auto">
              <img class="img-fluid rounded-circle" src="{{ asset('img/100x100/img2.jpg')}}" alt="">
            </div>
          </div>
        </div>
        <!-- End Testimonials Carousel Pagination -->

        <!-- Clients -->
        <div class="position-absolute right-0 bottom-0 left-0 text-center p-5">
          <h4 class="h6 text-white-70 mb-3">CambiCash partners</h4>
          <div class="d-flex justify-content-center">
            <div class="mx-4">
              <img class="u-clients" src="{{ asset('svg/clients-logo/slack-white.svg')}}" alt="">
            </div>
            <div class="mx-4">
              <img class="u-clients" src="{{ asset('svg/clients-logo/google-white.svg')}}" alt="">
            </div>
            <div class="mx-4">
              <img class="u-clients" src="{{ asset('svg/clients-logo/spotify-white.svg')}}" alt="">
            </div>
          </div>
        </div>
        <!-- End Clients -->
      </div>
    </div>

    <div class="container">
      <div class="row no-gutters">
        <div class="col-md-8 col-lg-7 col-xl-6 offset-md-2 offset-lg-2 offset-xl-3 space-3 space-lg-0">
            <div class="card mt-5">
                <div class="card-header">{{ __('Verifica tu email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Te hemos enviado nuevamente un correo de verificacion a tu email') }}
                        </div>
                    @endif

                    {{ __('Antes de continuar, revise su correo electrónico para obtener un enlace de verificación.') }}
                    {{ __('Si no recibiste el correo electrónico') }}, <a href="{{ route('verification.resend') }}">{{ __('haga clic aquí para solicitar otro') }}</a>.
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Form -->
</main>
<!-- ========== END MAIN ========== -->
@endsection
@section('script')
<script>
  $(document).on('ready', function () {
    // initialization of slick carousel
    $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

    // initialization of form validation
    $.HSCore.components.HSValidation.init('.js-validate');
  });
</script>
@endsection


