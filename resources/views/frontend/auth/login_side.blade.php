<!-- Account Sidebar Navigation -->
<aside id="sidebarContent" class="u-sidebar" aria-labelledby="sidebarNavToggler">
  <div class="u-sidebar__scroller">
    <div class="u-sidebar__container">
      <div class="u-header-sidebar__footer-offset">
        <!-- Toggle Button -->
        <div class="d-flex align-items-center pt-4 px-7">
          <button type="button" class="close ml-auto" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <!-- End Toggle Button -->

        <!-- Content -->
        <div class="js-scrollbar u-sidebar__body">
          <div class="u-sidebar__content u-header-sidebar__content">
          <!-- Login -->
          <form class="js-validate" method="POST" action="{{ route('login') }}">
              @csrf
              <!-- Login -->
              <div id="login" data-target-group="idForm">
                  <!-- Title -->
                  <header class="text-center mb-7">
                      <h2 class="h4 mb-0">{{__('Bienvenido')}}</h2>
                      <p>{{__('Inicia sesión con tu cuenta.')}}</p>
                  </header>
                  <!-- End Title -->
          
                  <!-- Input -->
                  <div class="js-form-message mb-4">
                      <div class="js-focus-state input-group u-form">
                          <div class="input-group-prepend u-form__prepend">
                              <span class="input-group-text u-form__text">
                                  <span class="fa fa-user u-form__text-inner"></span>
                              </span>
                          </div>
                          <input type="email" class="form-control u-form__input" name="email" required @error('email') is-invalid
                              @enderror" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}" aria-label="Email"
                              data-msg="Please enter a valid email address." data-error-class="u-has-error"
                              data-success-class="u-has-success">
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                  </div>
                  <!-- End Input -->
          
                  <!-- Input -->
                  <div class="js-form-message mb-2">
                      <div class="js-focus-state input-group u-form">
                          <div class="input-group-prepend u-form__prepend">
                              <span class="input-group-text u-form__text">
                                  <span class="fa fa-lock u-form__text-inner"></span>
                              </span>
                          </div>
                          <input type="password" class="form-control u-form__input" name="password" required @error('password')
                              is-invalid @enderror" placeholder="{{ __('Password') }}" aria-label="Password"
                              data-msg="Your password is invalid. Please try again." data-error-class="u-has-error"
                              data-success-class="u-has-success">
                          @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                  </div>
                  <!-- End Input -->
          
                  <div class="clearfix mb-4">
                    <a class="float-right small u-link-muted" href="{{url('/password/reset')}}" data-animation-in="slideInUp">{{__('Olvidaste tu contraseña')}}</a>
                  </div>
          
                  <div class="mb-2">
                      <button type="submit" class="btn btn-block btn-primary u-btn-primary transition-3d-hover">{{__('Login')}}</button>
                  </div>
          
                  <div class="text-center mb-4">
                      <span class="small text-muted">{{__('¿No tienes cuenta?')}}</span>
                      <a class="small" href="{{ route('register') }}">{{__('Registrate')}}</a>
                  </div>
          
                  <div class="text-center">
                      <span class="u-divider u-divider--xs u-divider--text mb-4">{{__('o')}}</span>
                  </div>
          
                  <!-- Login Buttons -->
                  <div class="d-flex d-none">
                      <a class="btn btn-block btn-sm u-btn-facebook--air transition-3d-hover mr-1" href="#">
                          <span class="fab fa-facebook-square mr-1"></span>
                          Facebook
                      </a>
                      <a class="btn btn-block btn-sm u-btn-google--air transition-3d-hover ml-1 mt-0" href="#">
                          <span class="fab fa-google mr-1"></span>
                          Google
                      </a>
                  </div>
                  <!-- End Login Buttons -->
              </div>
          </form>
              <!-- End Login -->      
  
              <!-- Register -->
          <form class="js-validate" method="POST" action="{{ route('register') }}">
                @csrf
                <div id="signup" style="display: none; opacity: 0;" data-target-group="idForm">
                  <!-- Title -->
                  <header class="text-center mb-7">
                    <h2 class="h4 mb-0">Bienvenido a Cambicash</h2>
                    <p>{{__('Completa el formulario para empezar')}}</p>
                  </header>
                  <!-- End Title -->
                  <!-- Input -->
                  <div class="js-form-message mb-4">
                    <div class="js-focus-state input-group u-form">
                      <div class="input-group-prepend u-form__prepend">
                        <span class="input-group-text u-form__text">
                          <span class="fa fa-user u-form__text-inner"></span>
                        </span>
                      </div>
                      <input type="text" class="form-control u-form__input" name="name" required @error('nombre') is-invalid @enderror" value="{{ old('nombre') }}" placeholder="Name" aria-label="Name" data-msg="Please enter a valid name address." data-error-class="u-has-error" data-success-class="u-has-success">
                    @error('nombre')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                    </div>
                  </div>
                  <!-- End Input -->
                  <!-- Input -->
                  <div class="js-form-message mb-4">
                    <div class="js-focus-state input-group u-form">
                      <div class="input-group-prepend u-form__prepend">
                        <span class="input-group-text u-form__text">
                          <span class="fa fa-at u-form__text-inner"></span>
                        </span>
                      </div>
                      <input type="email" class="form-control u-form__input" name="email" required @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Email" aria-label="Email" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                    @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                    </div>
                  </div>
                  <!-- End Input -->
                  <!-- Input -->
                  <div class="js-form-message mb-4">
                    <div class="js-focus-state input-group u-form">
                      <div class="input-group-prepend u-form__prepend">
                        <span class="input-group-text u-form__text">
                          <span class="fa fa-lock u-form__text-inner"></span>
                        </span>
                      </div>
                      <input type="password" class="form-control u-form__input" name="password" required @error('password') is-invalid @enderror" placeholder="Password" aria-label="Password" data-msg="Your password is invalid. Please try again." data-error-class="u-has-error" data-success-class="u-has-success">
                    @error('password')
                       <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                       </span>
                    @enderror
                    </div>
                  </div>
                  <!-- End Input -->
                  <!-- Input -->
                  <div class="js-form-message mb-4">
                    <div class="js-focus-state input-group u-form">
                      <div class="input-group-prepend u-form__prepend">
                        <span class="input-group-text u-form__text">
                          <span class="fa fa-key u-form__text-inner"></span>
                        </span>
                      </div>
                      <input type="password" class="form-control u-form__input" name="password_confirmation" required autocomplete="new-password" placeholder="********" aria-label="********" data-msg="Your password is invalid. Please try again.">
                    </div>
                  </div>
                  <!-- End Input -->
                  <div class="mb-2">
                    <button type="submit" class="btn btn-block btn-primary u-btn-primary transition-3d-hover">{{__('Empezar')}}</button>
                  </div>
                  <div class="text-center mb-4">
                    <span class="small text-muted">{{__('¿ya tienes cuenta?')}}</span>
                    <a class="js-animation-link small" href="javascript:;" data-target="#login" data-link-group="idForm" data-animation-in="slideInUp">{{__('Inicia sesión')}}</a>
                  </div>
                  <div class="text-center">
                    <span class="u-divider u-divider--xs u-divider--text mb-4">{{__('OR')}}</span>
                  </div>
                  <!-- Login Buttons -->
                  <div class="d-flex">
                    <a class="btn btn-block btn-sm u-btn-facebook--air transition-3d-hover mr-1" href="#">
                      <span class="fab fa-facebook-square mr-1"></span>
                      Facebook
                    </a>
                    <a class="btn btn-block btn-sm u-btn-google--air transition-3d-hover ml-1 mt-0" href="#">
                      <span class="fab fa-google mr-1"></span>
                      Google
                    </a>
                  </div>
                  <!-- End Login Buttons -->
                </div>
          </form>
              <!-- End Register -->
          </div>
        </div>
        <!-- End Content -->
      </div>
      {{-- @include('frontend.resources.footerlogin') --}}
    </div>
  </div>
</aside>
<!-- End Account Sidebar Navigation -->