@extends('frontend.resources.core')
@section('content')
  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    <!-- Form -->
    <div class="d-flex align-items-center position-relative height-lg-100vh">
      <div class="col-lg-5 col-xl-4 d-none d-lg-flex align-items-center gradient-half-primary-v1 height-lg-100vh px-0">
        <div class="w-100 p-5">
          <!-- SVG Quote -->
          <figure class="text-center mb-5 mx-auto">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px"
               viewBox="0 0 8 8" style="enable-background:new 0 0 8 8;" xml:space="preserve">
              <path class="fill-white" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
                C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
                c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
                C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z"/>
            </svg>
          </figure>
          <!-- End SVG Quote -->

          <!-- Testimonials Carousel Main -->
          <div id="testimonialsNavMain" class="js-slick-carousel u-slick mb-4"
               data-infinite="true"
               data-autoplay="true"
               data-speed="5000"
               data-fade="true"
               data-nav-for="#testimonialsNavPagination">
            <div class="js-slide">
              <!-- Testimonials -->
              <div class="w-md-80 w-lg-60 text-center mx-auto">
                <blockquote class="h5 text-white font-weight-normal mb-4">Contenta de usar este servicio para enviar dinero a mis papas , rapido y seguro. MUCHAS GRACIAS CAMBICASH!</blockquote>
                <h1 class="h6 text-white-70">Maria roldan. </h1>
              </div>
              <!-- End Testimonials -->
            </div>

            <div class="js-slide">
              <!-- Testimonials -->
              <div class="w-md-80 w-lg-60 text-center mx-auto">
                <blockquote class="h5 text-white font-weight-normal mb-4">Comprar bitcoins nunca me pareció tan fácil en  minutos tenia los bitcoin en mi cartera, lo recomiendo!</blockquote>
                <h2 class="h6 text-white-70">James martines, Colombia</h2>
              </div>
              <!-- End Testimonials -->
            </div>

            <div class="js-slide">
              <!-- Testimonials -->
              <div class="w-md-80 w-lg-60 text-center mx-auto">
                <blockquote class="h5 text-white font-weight-normal mb-4">Realmente  una forma simple y segura de enviar dinero, estoy realmente agradecido.</blockquote>
                <h3 class="h6 text-white-70">Charli kan, Lima</h3>
              </div>
              <!-- End Testimonials -->
            </div>
          </div>
          <!-- End Testimonials Carousel Main -->

          <!-- Testimonials Carousel Pagination -->
          <div id="testimonialsNavPagination" class="js-slick-carousel u-slick u-slick--transform-off u-slick--pagination-modern u-slick--transform-off mx-auto"
               data-infinite="true"
               data-autoplay="true"
               data-speed="5000"
               data-center-mode="true"
               data-slides-show="3"
               data-is-thumbs="true"
               data-focus-on-select="true"
               data-nav-for="#testimonialsNavMain">
            <div class="js-slide">
              <div class="u-avatar mx-auto">
                <img class="img-fluid rounded-circle" src="{{asset('img/100x100/img1.jpg')}}" alt="Image Description">
              </div>
            </div>

            <div class="js-slide">
              <div class="u-avatar mx-auto">
                <img class="img-fluid rounded-circle" src="{{asset('img/100x100/img3.jpg')}}" alt="Image Description">
              </div>
            </div>

            <div class="js-slide">
              <div class="u-avatar mx-auto">
                <img class="img-fluid rounded-circle" src="{{asset('img/100x100/img2.jpg')}}" alt="Image Description">
              </div>
            </div>
          </div>
          <!-- End Testimonials Carousel Pagination -->

          <!-- Clients -->
          <div class="position-absolute right-0 bottom-0 left-0 text-center p-5">
            <h4 class="h6 text-white-70 mb-3">Cambicash</h4>
            <div class="d-flex justify-content-center">
              <div class="mx-4">
                <img class="u-clients" src="{{asset('svg/clients-logo/slack-white.svg')}}" alt="Image Description">
              </div>
              <div class="mx-4">
                <img class="u-clients" src="{{asset('svg/clients-logo/google-white.svg')}}" alt="Image Description">
              </div>
              <div class="mx-4">
                <img class="u-clients" src="{{asset('svg/clients-logo/spotify-white.svg')}}" alt="Image Description">
              </div>
            </div>
          </div>
          <!-- End Clients -->
        </div>
      </div>

      {{-- Formulario --}}
      <div class="col-lg-7">
        <div class="row justify-content-center">
          <div class="col-md-10">
              @if(Auth::check())
              <div class="mb-7">
                <h1 class="h3 text-primary font-weight-normal mb-0">Hola <span
                    class="font-weight-semi-bold">{{Auth::user()->name}}</span></h1>
                <p>Porfavor rellena tus datos</p>
                <ul id="tabsselected" class="nav nav-tabs mb-4" role="tablist">
                  <li class="nav-item">
                    <a id="enlace1" class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Persona Natural</a>
                  </li>
                  <li class="nav-item">
                    <a id="enlace2" class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Empresa</a>
                  </li>
                </ul>
                <div class="tab-content">
                  {{-- Persona natural --}}
                  <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <form class="js-validate mt-5" method="POST" action="{{ route('register-complete') }}" enctype="multipart/form-data">
                      {{ csrf_field() }}
                      <div class="form">
                          <input type="hidden" name="type_register" value="persona">
                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                  <label for="nombre">Nombre Completo *</label>
                                  <input id="telefonopersona" name="nombre" type="text" class="form-control" placeholder="Ingrese nombre completo">
                                </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12 col-md-6">
                              <div class="form-group">
                                <label for="nombre">Teléfono *</label>
                                <input id="telefonopersona" name="telefono" type="number" class="form-control" placeholder="Ingrese Teléfono">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12 col-md-6">
                              <div class="form-group">
                                <label for="nombre">Tipo de documento</label>
                                <div class="form-row">
                                  <div class="col">
                                    <select name="tipo_documento" id="tipodedocumento" class="form-control">
                                      <option value="facebook">DNI</option>
                                      <option value="instagram">Carnet de extranjeria</option>
                                      <option value="instagram">PTP</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                            <div class="col-sm-12 col-md-6">
                              <div class="form-group" style="position: relative">
                                <label for="nombre">Nr° Documento</label>
                                <input id="linkfanpagepersona" name="nro_documento" type="text" class="form-control" placeholder="Ingrese número de cocumento">
                              </div>
                            </div>
                          </div>
                          <div class="row align-items-center">
                            <div class="col-md-2">
                              <div class="form-group">
                                <div class="u-lg-avatar mr-3">
                                    <img id="imgProfilePerson" class="img-fluid rounded-circle" src="{{asset('img/160x160/img2.jpg')}}" alt="Image Description">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-10">
                              <div class="media-body">
                                <label for="nombre">Subir documento</label>
                                <div class="form-group">
                                  <label class="btn btn-sm btn-primary transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1" for="fileAttachmentBtn">
                                    Subir Documento de identidad
                                    <input id="inputImgProfilePerson" name="img_avatar" type="file" class="file-attachment-btn__label" accept="image/*" onchange="img_profile('inputImgProfilePerson', 'imgProfilePerson')">
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="buttons">
                        <button type="submit" class="btn btn-soft-primary btn-wide transition-3d-hover">Enviar</button>
                      </div>
                    </form>
                  </div>
                  {{-- End Persona natural --}}
            
                  {{-- Empresa --}}
                  <div class="tab-pane" id="tabs-2" role="tabpanel">
                    <form class="js-validate mt-5" method="POST" action="{{ route('register-complete') }}">
                        @csrf
                        <div class="form">
                          <input type="hidden" name="type_register" value="empresa">
                          <div class="row">
                            <div class="col-sm-12 col-md-12">
                              <div class="form-group">
                                <label for="nombre">Nombre de la empresa *</label>
                                <input id="nombreempresa" name="nombre" type="text" class="form-control" placeholder="Nombre de la empresa">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12 col-md-6">
                              <div class="form-group">
                                <label for="nombre">RUC *</label>
                                <input id="ruc" type="number" name="ruc" class="form-control" placeholder="Ingrese Ruc">
                              </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                              <div class="form-group">
                                <label for="nombre">Nombre del contacto *</label>
                                <input id="nombrecontacto" name="contacto" type="text" class="form-control" placeholder="Ingrese nombre de contacto">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12 col-md-6">
                              <div class="form-group">
                                <label for="nombre">Teléfono *</label>
                                <input id="telefonoempresa" name="telefono" type="tel" class="form-control" placeholder="Ingrese teléfono">
                              </div>
                            </div>
                          </div>
                            <div class="row align-items-center">
                              <div class="col-md-2">
                                <div class="form-group">
                                  <div class="u-lg-avatar mr-3">
                                      <img id="imgProfileEmpresa" class="img-fluid rounded-circle" src="{{asset('img/160x160/img2.jpg')}}" alt="Image Description">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-10">
                                <div class="media-body">
                                  <label for="nombre">Subir Documento</label>
                                  <div class="form-group">
                                    <label class="btn btn-sm btn-primary transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1" for="fileAttachmentBtn">
                                      Subir Documento de Identidad
                                      <input id="inputImgProfileEmpresa" name="img_avatar" type="file" class="file-attachment-btn__label" accept="image/*" onchange="img_profile('inputImgProfileEmpresa', 'imgProfileEmpresa')">
                                    </label>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="buttons">
                          <button type="submit" class="btn btn-soft-primary btn-wide transition-3d-hover">{{__('Enviar')}}</button>
                        </div>
                    </form>
                  </div>
                  {{-- End Empresa --}}
                </div>
              </div>
              @else
                <!-- Form -->
                <form class="js-validate mt-5" method="POST" action="{{ route('register') }}">
                  @csrf
                  <!-- Title -->
                  <div class="mb-7">
                    <h1 class="h3 text-primary font-weight-normal mb-0">{{__('Bienvenido a')}} <span class="font-weight-semi-bold">Cambicash</span></h1>
                    <p>{{__('Complete el formulario para comenzar.')}}</p>
                  </div>
                  <!-- End Title -->

                  <!-- CAMPO NOMBRE -->
                  <div class="js-form-message form-group">
                    <label class="form-label" for="name">{{ __('Nombre de Usuario') }}</label>
                    <input id="name" type="text" class="form-control" @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required placeholder="{{__('Ingrese nombre')}}" autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <!-- End Form Group -->

                  <!-- CAMPO EMAIL -->
                  <div class="js-form-message form-group">
                    <label class="form-label" for="signinSrEmail">{{ __('Correo electrónico') }}</label>
                    <input type="email" class="form-control" @error('email') is-invalid @enderror" name="email" id="signinSrEmail" value="{{ old('email') }}" required autocomplete="email" autofocus data-msg="Please enter a valid email address." placeholder="Correo electrónico" data-error-class="u-has-error" data-success-class="u-has-success">
                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <!-- End Form Group -->

                  <!-- CAMPO PASSWORD -->
                  <div class="form-group row">
                      <div class="col-md-12">
                          <label class="form-label" for="signinSrEmail">{{ __('Contraseña') }}</label>
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Contraseña">
                          @error('password')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                  </div>
                  <!-- End Form Group -->

                  <!-- CAMPO PASSWORD VERIFY -->
                  <div class="form-group row">
                      <div class="col-md-12">
                          <label class="form-label" for="signinSrEmail">{{ __('Confirmar contraseña') }}</label>
                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirmar contraseña">
                      </div>
                  </div>
                  <!-- End Form Group -->

                  <!-- Button -->
                  <div class="row align-items-center mb-5">
                    <div class="col-5 col-sm-6">
                      <span class="small text-muted">{{__('¿Ya tienes una cuenta?')}}</span>
                      <a class="small" href="{{route('login')}}">{{__('Incia sesión')}}</a>
                    </div>

                    <div class="col-7 col-sm-6 text-right">
                      <button type="submit" class="btn btn-primary transition-3d-hover">Registrar</button>
                    </div>
                  </div>
                  <!-- End Button -->
                </form>
                <!-- End Form -->
              @endif
          </div>
        </div>
      </div>
      {{-- End Formulario --}}
    </div>
    <!-- End Form -->
  </main>
  <!-- ========== END MAIN ========== -->
@endsection
@section('script')
    <script>
    $(document).on('ready', function () {
      // initialization of slick carousel
      $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
  
      // initialization of form validation
      $.HSCore.components.HSValidation.init('.js-validate');
    });

    function img_profile(input, imgProfile) {
      var __file = document.getElementById(input).files[0];
      var __render_img = document.getElementById(imgProfile);
      var reader = new FileReader();
      if (__file) {
        reader.readAsDataURL(__file);
        reader.onloadend = function () {
          __render_img.src = reader.result;
        }
      }
    }
  </script>
@endsection