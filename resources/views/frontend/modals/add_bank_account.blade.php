<div class="modal fade" id="modal_add_account" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <form action="{{ route("add_accounts") }}" method="post">
                {{ csrf_field() }}
                    <h4 id="modal-label-3" class="modal-title text-center">Agregar cuenta bancaria</h4>
                    <p class="text-center">Añade la cuenta bancaria de donde envías tu cambio seguro esta</p>
                    <input type="hidden" id="type-account" name="type_account_tran" value="1">
                    <div class="form-row mb-2">
                        <div class="form-group col-md-6">
                            <label for="nombre-cuenta" class="xs-only">Nombre Completo</label>
                            <input type="text" name="nombre_cuenta" class="form-control" id="nombre-cuenta" placeholder="Nombre Completo" required>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="apellido-cuenta" class="xs-only">Apellido Completo</label>
                            <input type="text" name="apellido_cuenta" class="form-control" id="apellido-cuenta" placeholder="Apellido Completo" required>
                        </div>
                    </div>

                    <div class="form-row mb-2">
                        
                        <div class="form-group col-md-6">
                            <label for="nombre">Tipo de documento</label>
                            <div class="form-row">
                              <div class="col">
                                <select name="tipo_documento" id="tipodedocumento" class="form-control">
                                  <option value="DNI">DNI</option>
                                  <option value="Carnet de extranjeria">Carnet de extranjeria</option>
                                  <option value="PTP">PTP</option>
                                </select>
                              </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cedula-cuenta" class="xs-only">Cedula</label>
                            <input type="text" name="cedula" class="form-control" id="cedula-cuenta" placeholder="Cedula" required>
                        </div>
                    </div>

                    <div class="form-row mb-2">
                        <div class="form-group col-md-6">
                            <label for="email-cuenta" class="xs-only">Correo Electronico</label>
                            <input type="text" name="email" class="form-control" id="email-cuenta" placeholder="Correo Electronico" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="casa-cuenta" class="xs-only">Direccion</label>
                            <input type="text" name="casa" class="form-control" id="casa-cuenta" placeholder="Direccion" required>
                        </div>
                    </div>


                    <div class="form-row mb-2">
                        <div class="form-group col-md-6">
                            <label for="nombre-cuenta" class="xs-only">Pais</label>
                            <select id="pais" name="pais_cuenta" class="form-control mb-2">
                                <option value="" selected>Selecciona pais...</option>
                                @foreach ($country as $key => $value)
                                    <option value="{{ $value->id }}" {{ $value->id == 169 ? 'selected': '' }}>{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tipo-moneda" class="xs-only">Tipo de moneda</label>
                            <select id="tipo-moneda" name="tipo_moneda" class="form-control mb-2">
                                <option value="" selected>Tipo de moneda</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row mb-2">
                        <div class="form-group col-md-6">
                            <label for="entidad-bancaria" class="xs-only">Entidad bancaria</label>
                            <select id="entidad-bancaria" name="entidad_bancaria" class="form-control mb-2">
                                <option value="" selected>Selecciona una entidad bancaria...</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tipo-cuenta" class="xs-only">Tipo de cuenta</label>
                            <select id="tipo-cuenta" name="tipo_cuenta" class="form-control mb-2">
                                <option value="" selected>Tipo de cuenta</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row mb-2">
                        <div class="form-group col-md-12">
                            <label for="numero-cuenta" class="xs-only">Numero de cuenta</label>
                            <input type="text" name="numero_cuenta" class="form-control" id="numero-cuenta" placeholder="Numero de cuenta" required>
                        </div>
                    </div>
                    <div class="float-right">
                        <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
                        <button class="btn btn-primary btn-b" type="submit">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>