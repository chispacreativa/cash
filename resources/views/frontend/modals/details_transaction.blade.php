<div class="modal fade" id="modal_details_transaction" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 id="modal-label-3" class="modal-title text-center">Detalle transaccion</h4>
                    {{-- <small class="text-center d-block w-100">...</small> --}}
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Destino</div>
                        <div class="col-md-3">Cambio</div>
                        <div class="col-md-3">Deposito</div>
                        <div class="col-md-3">Estado</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-b" type="button">Cerrar</button>
                </div>
            </div>
        </div>
    </div>