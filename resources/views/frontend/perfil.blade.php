<!DOCTYPE html>
<html lang="es">
<head>
    @include('frontend.resources.head') 
    {{-- <link rel="stylesheet" href="{{ asset('css/plugins.css') }}" type="text/css" />  --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" /> --}}
    <link rel="stylesheet" href="{{ asset('css/text-animation.css') }}" type="text/css" />  
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" type="text/css" />  
</head>
<body class="bg-gray">
    <div id="dashboard-layout" class="">
        @include('frontend.resources.header2') 
        <div class="container mb-3 mt-2 mt-sm-5">
            <div class="row justify-content-md-center">
                <div class="col-lg-2 d-none d-lg-block">
                    <div class="aside">
                        @include('frontend.resources.sider') 
                    </div>
                </div>
                <div class="col-md-12 col-lg-10">
                    <div class="row">
                        <div class="col-4">
                            <div class="panel fixed">
                                <h3 class="panel-title">Perfil</h3>
                                <div class="head-perfil">
                                    <h4 class="">Nombre del usuario</h4>
                                    <h5 class="">Correo@cambicash.com</h5>
                                </div>
                                <div class="content-perfil">
                                    <span>Dato1</span>  
                                    <p>informacion del Datos</p>
                                    <span>Dato2</span> 
                                    <p>informacion del Datos</p>  

                                </div>
                                <div class="panel-content">
                                    <div class="plan-list">
                                       
                                        <div class="list-group list-group-flush" id="list-tab" role="tablist">
                                            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="tab" href="#list-home" role="tab" aria-controls="list-home">Informacion Personal</a>
                                            <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="tab" href="#list-profile" role="tab" aria-controls="list-profile">Información de la Cuenta</a>
                                            <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="tab" href="#list-messages" role="tab" aria-controls="list-messages">Cambiar Contraseña</a>
                                            <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="tab" href="#list-settings" role="tab" aria-controls="list-settings">Config. correo</a>
                                        </div>
                                
                                       
                                    </div>
                                </div>     
                            </div>
                        </div>
                        <div class="col-8 col-md-8">
                            <div class="panel fixed">
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                                        <div class="">
                                            <h3 class="border-bottom border-gray pb-2 mb-0 panel-title">Información  Personal <small>actualice su información personal</small></h3>
                                        </div>
                                        <div class="panel-content">
                                            <form class="user">
                                                <div class="form-group row">
                                                    <label for="inputfirstName" class="col-sm-4 col-form-label texto">Primer Nombre</label>
                                                    <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="inputfirstName" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputlastName" class="col-sm-4 col-form-label texto">Apellidos</label>
                                                    <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="inputlastName" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-4 col-form-label texto">Teléfono de contacto</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text"><i class="fa fa-mobile-alt"></i></div>
                                                            </div>
                                                            <input type="email" class="form-control" id="inputEmail3" placeholder="7889663">
                                                            <small id="passwordHelpBlock" class="form-text text-muted">
                                                                Nunca compartiremos su Numero Telefónico con nadie más.
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-4 col-form-label texto">Dirección de correo electrónico</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text"><i class="fa fa-at"></i></div>
                                                            </div>
                                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                                            <small id="passwordHelpBlock" class="form-text text-muted">
                                                                Nunca compartiremos su correo electrónico con nadie más.
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="line"></div>
                                                <div class="form-group row">
                                                    <div class="col-sm-4"></div>
                                                    <div class="col-sm-8">
                                                    <button type="submit" class="btn btn-success">Guardar</button>
                                                    <button type="submit" class="btn btn-outline-secondary">Cancelar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                                        <div>
                                            <h3 class="border-bottom border-gray pb-2 mb-0 panel-title">Información de la Cuenta <small>cambia la configuración de su cuenta </small></h3>
                                        </div>
                                        
                                        <div class="panel-content">
                                            <form class="user">
                                                <h4 class="mb-3">Cuenta</h4>
                                                <div class="form-group row">
                                                    <label for="inputfirstName" class="col-sm-4 col-form-label texto">Nombre de Usuario</label>
                                                    <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="inputfirstName" placeholder="Nick">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-4 col-form-label texto">Dirección de correo electrónico</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group mb-2">
                                                            <div class="input-group-prepend">
                                                                <div class="input-group-text"><i class="fa fa-at"></i></div>
                                                            </div>
                                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                                            <small id="passwordHelpBlock" class="form-text text-muted">
                                                                Nunca compartiremos su correo electrónico con nadie más.
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="mb-4">
                                                <h4 class="mb-3"> Seguridad</h4>

                                                <div data-toggle="buttons">
                                                <div class="form-group row">
                                                    <label for="inputfirstName" class="col-sm-4 col-form-label texto">verificación de acceso</label>
                                                    <div class="col-sm-8">
                                                        <div class="custom-control custom-radio btn-group-toggle">
                                                            <label class="btn btn btn-check-radio btn-sm">
                                                                <input type="radio" name="option-verificacion" id="option1" autocomplete="off"> Configurar la verificación de inicio de sesión
                                                            </label>
                                                        </div>
                                                        <small id="passwordHelpBlock" class="form-text text-muted">
                                                            Después de iniciar sesión, se le pedirá información adicional para confirmar su identidad y proteger su cuenta de ser comprometida.
                                                        </small>
                                                    </div>
                                                </div>
                                                
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 col-form-label texto"></label>
                                                        <div class="col-sm-8">
                                                            <div class="custom-control custom-radio btn-group-toggle">
                                                                <label class="btn btn-danger btn-bold btn-sm">
                                                                    <input type="radio" name="option-verificacion" id="option2" autocomplete="off"> ¿Desactivar tu cuenta?
                                                                </label>
                                                            </div>
                                                        </div>
                                                </div>

                                            </div>
                                                <div class="form-group row">
                                                    <label for="inputfirstName" class="col-sm-4 col-form-label texto">Verificación de restablecimiento de contraseña</label>
                                                    <div class="col-sm-8">
                                                        <div class="custom-control custom-checkbox pl-4">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                            <label class="custom-control-label" for="customCheck1">Requerir información personal para restablecer su contraseña.</label>
                                                        </div>
                                                        <small id="passwordHelpBlock" class="form-text text-muted">
                                                            Para mayor seguridad, esto requiere que confirme su correo electrónico o número de teléfono cuando restablezca su contraseña.
                                                        </small>
                                                    </div>
                                                </div>



                                            
                                                <div class="line"></div>
                                                <div class="form-group row">
                                                    <div class="col-sm-4"></div>
                                                    <div class="col-sm-8">
                                                        <button type="submit" class="btn btn-success">Guardar</button>
                                                        <button type="submit" class="btn btn-outline-secondary">Cancelar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                                    
                                    </div>
                                                <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                                                        <h3 class="panel-title">Cambiar Contraseña</h3>
                                                        <div class="panel-content">
                                                                <form class="user">
                                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                                        Configure las contraseñas de los usuarios para que caduquen periódicamente. ¡Los usuarios necesitarán advertencias de que sus contraseñas caducarán, o podrían quedar bloqueadas del sistema sin darse cuenta!
                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="closse"><span aria-hidden="false" display="false">X</span></button>
                                                                    </div>
                                                                    
                                                                        <h4 class="mb-3">Cambie o recupere su contraseña:</h4>
                                                                        <div class="form-group row">
                                                                            <label for="inputPassword3" class="col-sm-4 col-form-label texto">contraseña actual</label>
                                                                            <div class="col-sm-8">
                                                                            <input type="password" class="form-control" id="inputPassword3" placeholder="">
                                                                            <a class="text-primary">Se te olvidó tu contraseña ?</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label for="inputPassword4" class="col-sm-4 col-form-label texto">Nueva contraseña</label>
                                                                            <div class="col-sm-8">
                                                                            <input type="password" class="form-control" id="inputPassword4" placeholder="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label for="inputverif" class="col-sm-4 col-form-label texto">Verificar contraseña</label>
                                                                            <div class="col-sm-8">
                                                                            <input type="password" class="form-control" id="inputverif" placeholder="">
                                                                            </div>
                                                                        </div>
                        
                                                                        <div class="line"></div>
                                                                        <div class="form-group row">
                                                                            <div class="col-sm-4"></div>
                                                                            <div class="col-sm-8">
                                                                            <button type="submit" class="btn btn-success">Guardar</button>
                                                                            <button type="submit" class="btn btn-outline-secondary">Cancelar</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                        </div>
                                                    
                                                </div>
                                                <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                                                        <h3 class="panel-title">Configuración del correo</h3>
                                                        <div class="panel-content">
                                                                <p>Irure enim occaecat labore sit qui aliquip reprehenderit amet velit. Deserunt ullamco ex elit nostrud ut dolore nisi officia magna sit occaecat laboris sunt dolor. Nisi eu minim cillum occaecat aute est cupidatat aliqua labore aute occaecat ea aliquip sunt amet. Aute mollit dolor ut exercitation irure commodo non amet consectetur quis amet culpa. Quis ullamco nisi amet qui aute irure eu. Magna labore dolor quis ex labore id nostrud deserunt dolor eiusmod eu pariatur culpa mollit in irure.</p>
                                                        </div>
                                                   
                                                </div>
                                            </div>
                                    
                                </div>
                            </div>

                        </div>

                        <!-- List group -->

                    </div>
                </div>
            </div>
        </div>
        @include('frontend.modals.details_transaction') 
    </div>
    @include('frontend.resources.scripts') 
    <script>
        $('#myList a').on('click', function (e) {
            e.preventDefault()
            $(this).tab('show')
        })

        $('#myList a[href="#profile"]').tab('show') // Select tab by name
        $('#myList a:first-child').tab('show') // Select first tab
        $('#myList a:last-child').tab('show') // Select last tab
        $('#myList a:nth-child(3)').tab('show') // Select third tab
    </script>

    
</body>
</html>