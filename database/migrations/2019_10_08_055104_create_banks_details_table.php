<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bank_id');

            $table->string('holder', 255);
            $table->string('bank_number', 255);
            $table->integer('type_account_id');
            $table->integer('type_currency_id');


            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks_details');
    }
}
