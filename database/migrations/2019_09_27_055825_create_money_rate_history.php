<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoneyRateHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_rate_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('pen', '15', '5')->default('0.01');
            $table->double('btc', '15', '5')->default('0.01');
            $table->double('cop', '15', '5')->default('0.01');
            $table->double('ves', '15', '5')->default('0.01');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('money_rate_histories');
    }
}
