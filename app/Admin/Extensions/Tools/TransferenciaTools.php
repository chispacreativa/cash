<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Illuminate\Support\Facades\Request;

class TransferenciaTools
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT
$('.grid-check-row').on('click', function () {
    $('#myModal').modal();
});

$('.grid-close-row').on('click', function () {
    $('#closeRow').modal();
});
$('#btn-mdlaccept').on('click', function () {
    var __id = $(this).data('id');
    $.ajax({
        method: 'post',
        url: '/admin/api/transferencias',
        data: {
            _token:LA.token,
            id: '{$this->id}',
            action: 'activado'
        },
        success: function () {
            $.pjax.reload('#pjax-container');
            toastr.success('Se actualizó correctamente!');
        }
    });
});
$('.box-body .img').on('click', function (e) {
    var element = $(e.currentTarget);
    $('#imagepreview').attr('src', element.attr('src'));
    $('#myModalImg').modal('show');
});


$('#btn-mdlcancelpayment').on('click', function () {
    var __id = $(this).data('id');
    $.ajax({
        method: 'post',
        url: '/admin/api/cancel/transferencia',
        data: {
            _token:LA.token,
            id: '{$this->id}',
            action: 'cancelado'
        },
        success: function () {
            toastr.success('Se actualizó correctamente!');
            window.location.href = "/admin/transferencias"
        }
    });
});

$('.box-body .img').on('click', function (e) {
    var element = $(e.currentTarget);
    $('#imagepreview').attr('src', element.attr('src'));
    $('#myModalImg').modal('show');
});


SCRIPT;
    }

    public function render() {
        Admin::script($this->script());
        //return "<a class='btn btn-sm btn-info grid-check-row' data-id='{$this->id}'><i class='fa fa-trash'></i>&nbsp;&nbsp;Validar</a>";
        return <<<EOT
        <a class='btn btn-sm btn-info grid-check-row' data-id='{$this->id}'><i class='fa fa-check'></i>&nbsp;&nbsp;Validar</a>
        <a class='btn btn-sm btn-info grid-close-row' data-id='{$this->id}'><i class='fa fa-close'></i>&nbsp;&nbsp;Cancelar</a>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Validación Pago</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Estás seguro?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btn-mdlaccept" data-id='{$this->id}' data-dismiss="modal" class="btn btn-primary">aceptar</button>
                        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="closeRow" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cancelar Pago</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Estás seguro?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btn-mdlcancelpayment" data-id='{$this->id}' data-dismiss="modal" class="btn btn-primary">aceptar</button>
                        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="myModalImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Image preview</h4>
                </div>
                <div class="modal-body">
                    <center>
                        <img src="" id="imagepreview" style="width:100%; max-width: 800px; height: 100%;" >
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
EOT;
    }

    public function __toString()
    {
        return $this->render();
    }
}