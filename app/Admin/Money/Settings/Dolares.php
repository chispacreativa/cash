<?php
namespace App\Admin\Money\Settings;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;

class Dolares extends Form
{
    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'DOLARES';

     /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request)
    {
        //dump($request->all());
        admin_success('Processed successfully.');
        return back();
    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->number('extra_fee', 'Tarifa adicional')->help('Precio Adicional %3')->rules('required');

        $this->table('extra', 'Cuentas Bank', function ($form) {
            $form->text('name');
            $form->email('email');
        });
    }

    /**
     * The data of the form.
     *
     * @return array $data
     */
    public function data()
    {
        return [
            'extra_fee' => 3,
            'extra' => [
                array(
                    'name' => 'a',
                    'email' => 'b'
                )
            ]
        ];
    }
}