<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->resources([
        'section'                  => SectionsController::class,
        'transferencias'           => TransferenciasController::class,
        'usuarios'                 => UsuariosController::class,
        'money-config'             => MoneyController::class,
        'banks'                    => BanksController::class,
        'banksdetails'             => BanksDetailsController::class,
        'promociones'             => PromocionController::class,
    ]);
    $router->post('/api/sendPromotion', 'PromocionController@sendPromotion')->name('admin.sendPromotion');

    $router->post('/api/transferencias', 'TransferenciasController@handle');
    $router->post('/api/cancel/transferencia', 'TransferenciasController@cancel');

    $router->get('/', 'HomeController@index')->name('admin.home');

});
