<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\InfoBox;
use App\User;

class UsuariosController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Usuarios';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->model()->orderBy('id', 'desc');

        $grid->column('id', __('ID'));

        $grid->column('name', __('Nombre'));
        $grid->column('email', __('Correo'));

        $grid->column('created_at', __('Creado Fecha'));


        $grid->actions(function ($actions) {
            //$actions->disableEdit();
            //$actions->disableView();
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('ID'));

        $show->field('name', __('Nombre'));
        $show->field('email', __('Correo'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());

        $form->text('name', __('Nombre'));

        $form->email('email', __('Correo'));

        

        return $form;
    }

}