<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\BanksDetails;
use App\Models\Bank;
use App\Models\TypeBankAccount;
use App\Models\TypeCurrency;
use App\Models\Country;

class BanksDetailsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Banks Details';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BanksDetails);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('holder', __('Titular'));
        $grid->_type()->name(__('Tipo'));
        $grid->_type_currency()->name(__('Tipo Moneda'));
        $grid->column('status', __('Estatus'));


        /*grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));*/

        $grid->actions(function ($actions) {
            
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BanksDetails::findOrFail($id));
        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BanksDetails());
        $form->text('holder', __('Titular'));
        $form->text('bank_number', __('Numero'));
        $form->select('bank_id', __('Bank'))->options(Bank::all()->pluck('name', 'id'));
        $form->select('type_account_id', __('Tipo'))->options(TypeBankAccount::all()->pluck('name', 'id'));
        $form->select('type_currency_id', __('Tipo'))->options(TypeCurrency::all()->pluck('name', 'id'));
        $form->switch('status', __('Status'))->default(1);
        return $form;
    }
}
