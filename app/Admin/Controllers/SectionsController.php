<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Section;

class SectionsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Sections';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Section);

        $grid->column('id', __('ID'))->sortable();

        $grid->column('title', __('Title'));

        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->actions(function ($actions) {
            
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Section::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        $form = new Form(new Section());

        

        $form->tab('Detalles', function($form) {
            $form->text('title', __('Titulo'));

            $form->summernote('details', __('details'));

            $form->switch('status', __('Status'))->default(1);
        });

        $form->tab('SEO', function($form) {
            $form->text('seo_title', __('Titulo SEO'));
            $form->textarea('seo_description', __('Descripcion SEO'));
            $form->tags('seo_keywords', __('Keywords SEO'));
            
        });

        //$tab->add('Pie', $pie);


        return $form;
    }
}