<?php namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Encore\Admin\Layout\Content;
use App\Model\Usuario;
use App\Events\SendPromotion;
use Encore\Admin\Controllers\AdminController;

class PromocionController extends AdminController
{

    public function index(Content $content)
    {
        $__users = Usuario::all();
        return $content
            ->title('Promociones')
            ->description('Panel')
            ->view("admin.promocion.create", [
                'users' => $__users
            ]);
    }

    public function sendPromotion(Request $request)
    {
       
        $request->validate([
            'asunto' => 'required',
            'cuerpo' => 'required'
        ]);


        if ($request->usuario == null) {
            $emails = Usuario::select('email')
                            ->pluck('email');
            event(new SendPromotion($emails, $request->asunto, $request->cuerpo));
        } else {
            $user = Usuario::find($request->usuario);
           
            event(new SendPromotion($user->email, $request->asunto, $request->cuerpo));
        }



        return redirect()->route("promociones.index")->with('success_mail', __('¡La promocion se envio correctamente!'));


    }
}
