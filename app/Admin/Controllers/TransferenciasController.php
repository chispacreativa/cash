<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\InfoBox;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\AttachFile;
use App\Admin\Extensions\Tools\TransferenciaTools;
use App\Mail\NuevoIntercambio;
use Illuminate\Support\Facades\Mail;
use App\Events\IntercambioValidation;
use App\Events\IntercambioValidationBeneficiario;

class TransferenciasController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Transferencias';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Transaction);

        $grid->model()->orderBy('id', 'desc');
        $grid->model()->where('status', '>', 0);

        //$grid->column('id', __('ID'))->sortable();
        
        $grid->column('id', __('Transaccion'))->expand(function ($model) {

            $user = $model->user()->first();

            $bar = view('box.transaccion', [
                'usuario' => $user->name,
                'email' => $user->email
            ]);

            $box = new Box('Detalles', $bar);
            $box->style('info');

            $box->solid();

            return $box;
        });

        $grid->column('type_exchange', __('Tipo Cambio'))->display(function ($type) {
            $__arr = str_split($type, 1);
            $__reverse = '';
            if (count($__arr) > 1) {
                $__exchange = [
                    "1" => "PEN",
                    "2" => "USD",
                    "3" => "COP",
                    "4" => "VES",
                    "5" => "BTC"
                ];
                $__simbolo = [
                    "PEN" => "S/",
                    "USD" => "$",
                    "COP" => "$",
                    "VES" => "$",
                    "BTC" => "₿",
                ];
                $__reverse = $__exchange[$__arr[0]] . " a " . $__exchange[$__arr[1]];
            }
            return "<span class='label label-info'>$__reverse</span>";
        });

        $grid->column('send_amount', __('Recibido'));
        $grid->column('receive_amount', __('Envio'));

        $grid->column('status')->display(function ($type) {
            $__label = '';
            $__value = '';
            switch ($type) {
                case '0':
                    $__label = 'label-danger';
                    $__value = 'SIN CONFIRMAR';
                    break;
                case '1':
                    $__label = 'label-warning';
                    $__value = 'EN PROCESO';
                    break;
                case '2':
                    $__label = 'label-danger';
                    $__value = 'RECHAZADO';
                    break;
                case '3':
                    $__label = 'label-success';
                    $__value = 'ENVIADO';
                    break;
                default:
                    $__label = 'label-danger';
                    $__value = '-';
                    break;
            }
            return "<span class='label {$__label}'>$__value</span>";
        });

        $grid->column('created_at', __('Enviado Fecha'));
        //$grid->column('updated_at', __('Updated at'));


        $grid->actions(function ($actions) {
            
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $this->id = $id;
        $show = new Show(Transaction::findOrFail($id));

        $show->field('id', __('ID'));

        $show->field('send_amount', __('Envió'));
        $show->field('receive_amount', __('Recibirá'));

        $show->field('type_exchange', __('Tipo Intercambio'))->as(function ($type) {
            $__arr = str_split($type, 1);
            $__reverse = '';
            if (count($__arr) > 1) {
                $__exchange = [
                    "1" => "PEN",
                    "2" => "USD",
                    "3" => "COP",
                    "4" => "VES",
                    "5" => "BTC"
                ];
                $__simbolo = [
                    "PEN" => "S/",
                    "USD" => "$",
                    "COP" => "$",
                    "VES" => "$",
                    "BTC" => "₿",
                ];
                $__reverse = $__exchange[$__arr[0]] . " a " . $__exchange[$__arr[1]];
            }
            return $__reverse;
        });

        $show->field('status', __('Estatus'))->as(function ($type) {
            $__label = '';
            $__value = '';
            switch ($type) {
                case '0':
                    $__label = 'label-danger';
                    $__value = 'SIN CONFIRMAR';
                    break;
                case '1':
                    $__label = 'label-warning';
                    $__value = 'EN PROCESO';
                    break;
                case '2':
                    $__label = 'label-danger';
                    $__value = 'RECHAZADO';
                    break;
                case '3':
                    $__label = 'label-success';
                    $__value = 'ENVIADO';
                    break;
                default:
                    $__label = 'label-danger';
                    $__value = '-';
                    break;
            }
            return "$__value";
        });


        $show->field('created_at', __('Enviado Fecha'));
        $show->field('updated_at', __('Actualizado Fecha'));

        $show->field('images', __('Images'))->as(function ($images) {
            $__images = [];
            if ($images != null && $images != 'null') {
                $__data = json_decode($images, true);
                foreach ($__data as $key => $value) {
                    $__attach = AttachFile::where('id' , $value)->first();
                    $__images[] = config('app.url') . '/transferencias/' . $__attach->file;
                }
            }
            return $__images;
        })->image();


        /*$show->field('user', __('Usuario'))->as(function ($user) {
            return $user->name;
        });*/

        $show->_user(__('Usuario'), function ($user) {
            $user->name('Nombre');
            $user->email('Correo');

            $user->panel()->tools(function ($tools) {
                $tools->disableList();
                $tools->disableEdit();
                $tools->disableDelete();
            });
        });

        $show->_bankAccount(__('Cuenta Bank'), function ($bank) {
            $bank->name('Nombre');
            $bank->value('Numero de Cuenta');

            $bank->_bank('Banco', function ($bank2) {
                $bank2->name('Banco');
                $bank2->panel()->tools(function ($tools) {
                    $tools->disableList();
                    $tools->disableEdit();
                    $tools->disableDelete();
                });
            });

            $bank->_currency('Tipo Moneda', function ($currency) {
                $currency->name('Tipo Moneda');
                $currency->panel()->tools(function ($tools) {
                    $tools->disableList();
                    $tools->disableEdit();
                    $tools->disableDelete();
                });
            });

            $bank->_type('Tipo Cuenta', function ($type) {
                $type->name('Tipo Cuenta');
                $type->panel()->tools(function ($tools) {
                    $tools->disableList();
                    $tools->disableEdit();
                    $tools->disableDelete();
                });
            });

            $bank->panel()->tools(function ($tools) {
                $tools->disableList();
                $tools->disableEdit();
                $tools->disableDelete();
            });

            $bank->panel()->tools(function ($tools) {
                $tools->disableList();
                $tools->disableEdit();
                $tools->disableDelete();
            });
        });

        $__tmp_id = $id;


        $show->panel()->tools(function ($tools) {
            $tools->disableEdit();
            $tools->disableList();
            $tools->disableDelete();

            $tools->append(new TransferenciaTools($this->id));

            //$tools->append('<a href="#" class="btn btn-sm btn-info"><i class="fa fa-trash"></i>&nbsp;&nbsp;Validar</a>');
            //$tools->append('<a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Rechazar</a>');
            //$tools->append('<a href="#" class="btn btn-sm btn-success"><i class="fa fa-trash"></i>&nbsp;&nbsp;Confirmar</a>');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        $form = new Form(new Transaction());

        

        /*$form->tab('Detalles', function($form) {
            $form->text('title', __('Titulo'));

            $form->summernote('details', __('details'));

            $form->switch('status', __('Status'))->default(1);
        });

        $form->tab('SEO', function($form) {
            $form->text('seo_title', __('Titulo SEO'));
            $form->textarea('seo_description', __('Descripcion SEO'));
            $form->tags('seo_keywords', __('Keywords SEO'));
        });*/

        //$tab->add('Pie', $pie);


        return $form;
    }

    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request) {
        $__id = $request->get('id');
        $__action = $request->get('action');

        $intercambio = Transaction::findOrFail($__id);
        $intercambio->status = 3;
        $intercambio->save();

        /*Mail::to($intercambio->user()->email)
            ->cc("julitoppapi@gmail.com")->queue(new ValidateIntercambio($intercambio));*/

        event(new IntercambioValidation($intercambio->user()->email, $intercambio));

        event(new IntercambioValidationBeneficiario($intercambio->bankAccountSend(), $intercambio));
    }


    public function cancel(Request $request) {
        $__id = $request->get('id');
        $__action = $request->get('action');

        $intercambio = Transaction::findOrFail($__id);
        $intercambio->status = 2;
        $intercambio->save();

        /*Mail::to($intercambio->user()->email)
            ->cc("julitoppapi@gmail.com")->queue(new ValidateIntercambio($intercambio));*/

       // event(new IntercambioValidation($intercambio->user()->email, $intercambio));
    }
}