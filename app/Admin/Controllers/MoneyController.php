<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\MoneyConfig;

class MoneyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Money Config';


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MoneyConfig);

        $grid->column('id', __('ID'))->sortable();

        $grid->column('name', __('Nombre'));
        $grid->column('code', __('Code'));
        $grid->column('status', __('Estatus'));


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MoneyConfig());

        $form->text('name', __('Nombre'));
        $form->text('code', __('Code'));

        $form->number('fee', __('Couta'));

        $form->switch('status', __('Estatus'));


        return $form;
    }
}
