<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
/* Modelo usuario detalle */
use App\Model\UsuarioDetalle;
use App\Model\Usuario;

class MailRegister extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $usuarioDetalle;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UsuarioDetalle $usuarioDetalle)
    {
        $this->usuarioDetalle = $usuarioDetalle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $__title = "Hola {$this->usuarioDetalle->name} bienvenido a Cambicash";

        return $this->view('emails.register.mail_register')->with([
            'title' => $__title,
            'user_detail' => $this->usuarioDetalle
        ]);
    }
}
