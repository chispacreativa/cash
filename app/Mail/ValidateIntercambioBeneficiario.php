<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Transaction;

class ValidateIntercambioBeneficiario extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $intercambio;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaction $intercambio)
    {
        $this->intercambio = $intercambio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $__exchange_type = $this->intercambio->getTypeExchange();

        $__title = "¡{$this->intercambio->bankAccountSend()->name}! Has recibido {$this->intercambio->receive_amount} {$__exchange_type['after_symbol']} por parte de {$this->intercambio->user()->name} a tu cuenta en {$this->intercambio->bankAccountSend()->bank()->name}";
        
        return $this->view('emails.intercambio.validacion_beneficiario')->with([
            'title' => $__title,
            'intercambio' => $this->intercambio
        ]);
    }
}
