<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Transaction;

class ValidateIntercambio extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $intercambio;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaction $intercambio)
    {
        $this->intercambio = $intercambio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $__title = "¡Recibimos los fondos de tu operación #{$this->intercambio->id}!";
        
        return $this->view('emails.intercambio.validacion')->with([
            'title' => $__title,
            'intercambio' => $this->intercambio
        ]);
    }
}
