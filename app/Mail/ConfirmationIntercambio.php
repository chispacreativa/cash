<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Transaction;

class ConfirmationIntercambio extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $intercambio;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaction $intercambio)
    {
        $this->intercambio = $intercambio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $__title = "¡Operación #{$this->intercambio->id} confirmación recibida!";
        return $this->view('emails.intercambio.confirmation')->with([
            'title' => $__title
        ]);
    }
}
