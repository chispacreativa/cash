<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Listeners\SendPromotionUsers;

class SendPromotionsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $asunto;

    public $cuerpo;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($asunto, $cuerpo)
    {
        $this->asunto = $asunto;
        $this->cuerpo = $cuerpo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $__title = $this->asunto;
        
        return $this->view('emails.promotion')->with([
            'title' => $__title,
            'cuerpo' => $this->cuerpo
        ]);
    }
}
