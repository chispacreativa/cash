<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsuarioDetalle extends Model
{
    protected $table = "user_details";
    protected $fillable = [
        'id_user',
        'name',
        'phone',
        'type_document',
        'ruc',
        'number_document',
        'image',
        'type'
    ];

    protected $primaryKey = "id";
}
