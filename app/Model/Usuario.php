<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "users";
    protected $fillable = [
        'name',
        'lastname',
        'email',
        'password',
    ];
    protected $primaryKey = "id";

    public function usuarioDetail()
    {
        return $this->hasOne("App\Model\UsuarioDetalle", 'id', 'id_user');
    }
}
