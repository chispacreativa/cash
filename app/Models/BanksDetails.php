<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BanksDetails extends Model
{
    //

    public function type() {
        return $this->hasOne('App\Models\TypeBankAccount', 'id', 'type_account_id')->first();
    }

    public function _type() {
        return $this->hasOne('App\Models\TypeBankAccount', 'id', 'type_account_id');
    }

    public function _type_currency() {
        return $this->hasOne('App\Models\TypeCurrency', 'id', 'type_currency_id');
    }
}
