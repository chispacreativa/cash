<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    //

    public function _country() {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function bankdetails() {
        return $this->belongsTo('App\Models\BanksDetails', 'id', 'bank_id');
    }
}
