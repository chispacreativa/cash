<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    //

    public function bank() {
        return $this->hasOne('App\Models\Bank', 'id', 'bank_id')->first();
    }

    public function _bank() {
        return $this->hasOne('App\Models\Bank', 'id', 'bank_id');
    }

    public function currency() {
        return $this->hasOne('App\Models\TypeCurrency', 'id', 'type_currency_id')->first();
    }

    public function _currency() {
        return $this->hasOne('App\Models\TypeCurrency', 'id', 'type_currency_id');
    }

    public function type() {
        return $this->hasOne('App\Models\TypeBankAccount', 'id', 'type_account_id')->first();
    }

    public function _type() {
        return $this->hasOne('App\Models\TypeBankAccount', 'id', 'type_account_id');
    }

    public function _country() {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }
}
