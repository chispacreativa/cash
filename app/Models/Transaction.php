<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //

    /*public function user() {
        return $this->belongsTo('App\User', 'user_id')->first();
    }*/

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    public function _user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /*public function bank() {
        return $this->belongsTo('App\Models\BankAccount', 'user_id')->first();
    }*/

    public function bankAccountSend() {
        return $this->hasOne('App\Models\BankAccount', 'id', 'bank_account_send')->first();
    }

    public function bankAccount() {
        return $this->hasOne('App\Models\BankAccount', 'id', 'bank_account')->first();
    }

    public function _bankAccount() {
        return $this->hasOne('App\Models\BankAccount', 'id', 'bank_account');
    }

    public function money_rate() {
        return $this->hasOne('App\Models\MoneyRateHistory', 'id', 'id_rate');
    }

    public function getMoneyRate() {
        $__arr = str_split($this->type_exchange, 1);
        if (count($__arr) > 1) {
            $__exchange = [
                "1" => "PEN",
                "2" => "USD",
                "3" => "COP",
                "4" => "VES",
                "5" => "BTC"
            ];
            $__simbolo = [
                "PEN" => "S/",
                "USD" => "$",
                "COP" => "$",
                "VES" => "$",
                "BTC" => "₿",
            ];
            $rate = $this->money_rate;

            $rate_primero = $rate->{strtolower($__exchange[$__arr[0]])};
            $rate_segundo = $__exchange[$__arr[1]] == "USD" ? 1 : $rate->{strtolower($__exchange[$__arr[1]])};

            $rate_fin = 0;
            if ($__exchange[$__arr[1]] == "BTC") {
                $rate_fin = number_format(($rate_primero / $rate_segundo), 5);
            } else {
                $rate_fin = number_format(($rate_primero / $rate_segundo), 2);
            }

            return array(
                'rate' => $rate_fin,
                'simbolo' => $__simbolo[$__exchange[$__arr[1]]]
            );
        }
        return null;
    }

    public function getTypeExchange() {
        $__arr = str_split($this->type_exchange, 1);
        if (count($__arr) > 1) {
            $__exchange = [
                "1" => "PEN",
                "2" => "USD",
                "3" => "COP",
                "4" => "VES",
                "5" => "BTC"
            ];
            $__simbolo = [
                "PEN" => "S/",
                "USD" => "$",
                "COP" => "$",
                "VES" => "$",
                "BTC" => "₿",
            ];
            $__reverse = $__exchange[$__arr[0]] . " a " . $__exchange[$__arr[1]];
            return array(
                "before" => $__exchange[$__arr[0]],
                "after" => $__exchange[$__arr[1]],
                "before_symbol" =>  $__simbolo[$__exchange[$__arr[0]]],
                "after_symbol" => $__simbolo[$__exchange[$__arr[1]]]
            );
        } 
        return null;
    }
}
