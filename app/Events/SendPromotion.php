<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendPromotion
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $email;
    public $asunto;
    public $cuerpo;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($email, $asunto, $cuerpo)
    {
        $this->email = $email;
        $this->asunto = $asunto;
        $this->cuerpo = $cuerpo;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
