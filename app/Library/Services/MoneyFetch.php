<?php
namespace App\Library\Services;

use App\Library\Services\Contracts\CustomServiceInterface;
use PHPHtmlParser\Dom;
use App\Models\MoneyRateHistory;

class MoneyFeth implements CustomServiceInterface {
    
    public function __construct() {
        
        
        $__soles = $this->doFetchSoles();
        $__bitcoin = $this->doFetchBitcoin();
        $__pesos = $this->doFetchPesoscop();
        $__bolivares = $this->doFetchBolivareVes();

        $data = array(
            "PEN" => $__soles,
            "BTC" => $__bitcoin,
            "COP" => $__pesos,
            "VES" => $__bolivares
        );

        $this->update($data);
    }

    public function doFetchSoles() {
        $url = "https://usd.mconvert.net/pen";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $dom = new Dom;
        $dom->load($response->getBody());
        $contents = $dom->find('.convert_result');
        $content = $contents[0];
        $__finds = $content->find('.byrate');
        $__cont = str_replace("By rate:", "", $__finds->innerHtml);
        $__precio = trim($__cont);

        $__precio = str_replace(",", "", $__precio);

        return number_format($__precio, 5, ".", "");
    }
    public function doFetchBitcoin() {
        $url = "https://usd.mconvert.net/btc";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $dom = new Dom;
        $dom->load($response->getBody());
        $contents = $dom->find('.convert_result');
        $content = $contents[0];
        $__finds = $content->find('.byrate');
        $__cont = str_replace("By rate:", "", $__finds->innerHtml);
        $__precio = trim($__cont);

        $__precio = str_replace(",", "", $__precio);

        return number_format($__precio, 5, ".", "");
    }
    public function doFetchPesoscop() {
        $url = "https://usd.mconvert.net/cop";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $dom = new Dom;
        $dom->load($response->getBody());
        $contents = $dom->find('.convert_result');
        $content = $contents[0];
        $__finds = $content->find('.byrate');
        $__cont = str_replace("By rate:", "", $__finds->innerHtml);
        $__precio = trim($__cont);

        $__precio = str_replace(",", "", $__precio);

        return number_format($__precio, 5, ".", "");
    }
    public function doFetchBolivareVes() {
        $url = "https://themoneyconverter.com/ES/MoneyConverter?from=USD&to=VES&amount=1";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $dom = new Dom;
        $dom->load($response->getBody());

        //data-value
        $main = $dom->find('main')[0];

        $spl = $main->getAttribute('data-value');
        $contents = $dom->find('#dfm');

        $options = $contents->find('option');

        $__usd = 0;
        $__ves = 0;

        foreach ($options as $key => $value) {
            //var_dump($value->value);
            $__str = explode($spl, $value->value);
            if (count($__str) > 1 && is_numeric($__str[0])) {
                if ($__str[1] == "USD") {
                    $__usd = $__str[0];
                } else if ($__str[1] == "VES") {
                    $__ves = $__str[0];
                }
            } else {
                if ($__str[0] == "USD") {
                    $__usd = $__str[1];
                } else if ($__str[0] == "VES") {
                    $__ves = $__str[1];
                }
            }
        }
        $__precio = $__ves / $__usd;
        $__precio = str_replace(",", "", $__precio);
        return number_format($__precio, 5, ".", "");
    }

    public function update($data) {
        $__insert = new MoneyRateHistory();
        $__insert->pen = $data['PEN'];
        $__insert->btc = $data['BTC'];
        $__insert->cop = $data['COP'];
        $__insert->ves = $data['VES'];
        $__insert->save();
    }
}