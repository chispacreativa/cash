<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NuevoIntercambio;
use App\Models\Transaction;
use PHPHtmlParser\Dom;
use Mail;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$__t = Transaction::first();
        return (new NuevoIntercambio($__t))->build();*/

        $__transaction = Transaction::where(['id' => '140'])->first();

        //var_dump($__transaction->send_amount);

        //var_dump(env('MAIL_FROM_ADDRESS'));

        //dd(config('mail'));

        /*Mail::to("carloscping@gmail.com")
              ->send(new NuevoIntercambio($__transaction));*/
        return (new NuevoIntercambio($__transaction))->build();
    }

}