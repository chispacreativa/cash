<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

/* Modelo usuario detalle */
use App\Model\UsuarioDetalle;
use App\Model\Usuario;

/* Mailing */
use Illuminate\Support\Facades\Mail;
use App\Mail\MailRegister;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (Auth::user()){
            $usuario_detail = new UsuarioDetalle();
            $id = Auth::user()->id;
            $__user = UsuarioDetalle::where('id_user', $id)->first();

            if( isset($__user) ) {
                return redirect('/dashboard/activity');
            }
        }
        return view('frontend.auth.register');
    }
    /** 
     * 
     * Registro de datos complementario
    */
    public function register_complete_data(Request $request) {
        $data = $request->all();
        $id = Auth::user()->id;
        $usuario_detail = new UsuarioDetalle();
        $__user = Usuario::find($id);

        // $request->file('img_avatar')->store('public');

        if( !empty($data) ) {
            $usuario_detail->id_user = $id;
            $usuario_detail->name            = isset($data['nombre']) ? $data['nombre'] : '';
            $usuario_detail->phone           = isset($data['telefono']) ? $data['telefono'] : '';
            $usuario_detail->type_document   = isset($data['tipo_documento']) ? $data['tipo_documento'] : '';
            $usuario_detail->number_document = isset($data['nro_documento']) ? $data['nro_documento'] : '';
            $usuario_detail->image           = isset($data['img_avatar']) ? $data['img_avatar'] : '';
            $usuario_detail->ruc             = isset($data['ruc']) ? $data['ruc'] : '';
            $usuario_detail->type            = isset($data['type_register']) ? $data['type_register'] : '';
            $usuario_detail->save();

            // $usuario_email = $__user['email'];
            // Mail::to(Auth::user()->email)
            //   ->queue(new MailRegister($usuario_detail));

            return redirect('/dashboard/activity');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
