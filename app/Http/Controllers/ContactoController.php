<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactoController extends Controller
{
    public function contacto() {
        return view('frontend.contacto.contacto');
    }
    public function nosotros() {
        return view('frontend.contacto.nosotros');
    }
    public function preguntas() {
        return view('frontend.contacto.preguntas');
    }
    public function terminos() {
        return view('frontend.contacto.terminos');
    }
    public function privacidad() {
        return view('frontend.contacto.privacidad');
    }
}
