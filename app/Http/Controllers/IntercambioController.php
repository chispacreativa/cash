<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use Illuminate\Validation\ValidationException;
use App\Http\Requests\TransactionCreateRequest;
use App\Models\BankAccount;
use App\Models\Bank;
use App\Models\TypeBankAccount;
use App\Models\TypeCurrency;
use App\Models\MoneyConfig;
use App\Models\Transaction;
use App\Models\AttachFile;
use App\Models\MoneyRateHistory;
use App\Models\BanksDetails;
use App\Models\Country;
use App\Events\IntercambioAdd;
use App\Events\IntercambioConfirmation;

use Mail;

class IntercambioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $__user = Auth::user();
        $__bank_account = BankAccount::where(['user_id' => $__user->id, 'type_account_tran' => 1 ])->get();
        $__bank_account_send = BankAccount::where(['user_id' => $__user->id, 'type_account_tran' => 2 ])->get();
        $__country = Country::where(['status' => 1])->get();

        $__bank = Bank::where(['status' => 1])->get();
        $__type_bank_account = TypeBankAccount::get();
        $__type_currency = TypeCurrency::get();

        $__send_data_home = 0;

        $datas = $request->all();
        if( !empty($datas) ) {
            $__send_data_home = $_GET;
        }

        $__money_config = MoneyConfig::where(['status' => 1])->get();

        $__money_rate = MoneyRateHistory::latest('created_at')->first();
        $__tmp_money = array(
            "USD" => 1,
            "PEN" => $__money_rate->pen,
            "BTC" => $__money_rate->btc,
            "COP" => $__money_rate->cop,
            "VES" => $__money_rate->ves
        );

        foreach ($__money_config as $key => $value) {
            if ($value->code != 'USD') {
                $__tmp_money[$value->code] = $__tmp_money[$value->code] * (  $value->fee / 100 + 1 );
            }
        }

        $__money_rate_json = json_encode($__tmp_money);

        return view('frontend.intercambio')->with([
            'bank_account' => $__bank_account,
            'bank_account_send' => $__bank_account_send,
            'country' => $__country,
            'bank' => $__bank,
            'type_bank_account' => $__type_bank_account,
            'type_currency' => $__type_currency,
            'money_rate_json' => $__money_rate_json,
            'data_home' => $__send_data_home
        ]);
    }

    public function store(TransactionCreateRequest $request) {
        $__user = Auth::user();
        $validated = $request->validated();


        $__time = new \DateTime();

        /*if ($__time->format('H') > 18 || $__time->format('H') < 9) {

            //return redirect()->route('intercambio')->with(['error_code', 1]);
            return redirect()->back()->with('error_code', 1);
        }*/
        

        $__transaction = new Transaction();
        $__transaction->send_amount = $validated['cambio01'];
        $__transaction->receive_amount = $validated['cambio02'];
        $__transaction->bank_account = $validated['cuenta-bank'];
        $__transaction->bank_account_send = $validated['cuenta-bank-send'];

        // 1 - SOLES
        // 2 - DOLARES
        // 3 - COLOMBIANO
        // 4 - VENEZOLANO 
        // 5 - BITCOIN

        // SOLES - SOLES           || 1 ~ 1 = 11
        // SOLES - DOLARES         || 1 ~ 2 = 12
        // SOLES - COLOMBIANO      || 1 ~ 3 = 13
        // SOLES - VENEZOLANO      || 1 ~ 4 = 14
        // SOLES - BITCOIN         || 1 ~ 5 = 15
  
        // DOLARES - SOLES         || 2 ~ 1 = 21
        // DOLARES - DOLARES       || 2 ~ 2 = 22
        // DOLARES - COLOMBIANO    || 2 ~ 3 = 23
        // DOLARES - VENEZOLANO    || 2 ~ 4 = 24
        // DOLARES - BITCOIN       || 2 ~ 5 = 25
  
        // COLOMBIANO - SOLES      || 3 ~ 1 = 31
        // COLOMBIANO - DOLARES    || 3 ~ 2 = 32
        // COLOMBIANO - COLOMBIANO || 3 ~ 3 = 33
        // COLOMBIANO - VENEZOLANO || 3 ~ 4 = 34
        // COLOMBIANO - BITCOIN    || 3 ~ 5 = 35

        // BITCOIN - SOLES         || 4 ~ 1 = 41
        // BITCOIN - DOLARES       || 4 ~ 2 = 42
        // BITCOIN - COLOMBIANO    || 4 ~ 3 = 43
        // BITCOIN - VENEZOLANO    || 4 ~ 4 = 44
        // BITCOIN - BITCOIN       || 4 ~ 5 = 45

        $__exchange = [
            "PEN" => "1",
            "USD" => "2",
            "COP" => "3",
            "VES" => "4",
            "BTC" => "5"
        ];

        $type_exchange = $__exchange[$validated['select-envio']] . $__exchange[$validated['select-recibo']];


        $__transaction->type_exchange = $type_exchange;
        $__transaction->status = 0;

        $__money_rate = MoneyRateHistory::latest('created_at')->first();

        $__transaction->id_rate = $__money_rate->id;
        $__transaction->user_id = $__user->id;
        if ( !$__transaction->save() ) {
            return redirect()->back()->withErrors(["error"]);
        } else {
            // CORREO
            if ($__transaction->status == 0)
                event(new IntercambioAdd($__user, $__transaction));
        }

        

        return redirect()->route('intercambio.view', array( 'id' => $__transaction->id ))->with('success', __('¡La transacción se registró correctamente!'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBankAccounts(Request $request)
    {
        $__user = Auth::user();

        $type = $request->query('type');

        $__bank_account = BankAccount::where(['user_id' => $__user->id, 'type_account_tran' => $type])->get();
        return response()->json([
            'count' => $__bank_account->count(),
            'data' => $__bank_account
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function submitBankAccounts(Request $request)
    {
        $__user = Auth::user();

        $response = [];

        $validator = $request->validate([
            'entidad_bancaria' => 'required',
            'tipo_cuenta' => 'required',
            'tipo_moneda' => 'required',
            'tipo_documento' => 'required',
            'nombre_cuenta' => 'required',
            'cedula' => 'required',
            'email' => 'required',
            'casa' => 'required',
            'apellido_cuenta' => 'required',
            'numero_cuenta' => 'required',
            'type_account_tran' => 'required',
            'pais_cuenta' => 'required'
        ]);
       // dd($validator);

        if (!$validator) {
            $response["error"] = true;
        } else {
            $response["error"] = false;

            $__bank_account = new BankAccount();
            $__bank_account->name = $validator['nombre_cuenta'];
            $__bank_account->lastname = $validator['apellido_cuenta'];
            $__bank_account->cedula = $validator['cedula'];
            $__bank_account->email = $validator['email'];
            $__bank_account->casa = $validator['casa'];
            $__bank_account->user_id = $__user->id;
            $__bank_account->tipo_documento = $validator['tipo_documento'];
            $__bank_account->bank_id = $validator['entidad_bancaria'];
            $__bank_account->type_currency_id = $validator['tipo_moneda'];
            $__bank_account->type_account_id = $validator['tipo_cuenta'];
            $__bank_account->value = $validator['numero_cuenta'];
            $__bank_account->type_account_tran = $validator['type_account_tran'];
            $__bank_account->country_id = $validator['pais_cuenta'];
            $__bank_account->save();

            $response["type"] = $validator['type_account_tran'];
        }

        return redirect()->back()->with('success', __('¡La cuenta se registró correctamente!'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTipoMoneda(Request $request)
    {
        $__user = Auth::user();
        $response = [];

        $validator = $request->validate([
            'pais' => 'required'
        ]);

        if (!$validator) {
            $response["error"] = true;
        } else {
            $response["error"] = false;
            $__data = [];
            $__bank = Bank::where(['country_id' => $validator['pais']])->get();
            foreach ($__bank as $key => $value) {
                if ($value->bankdetails()->count()) {
                    foreach ($value->bankdetails()->get() as $key2 => $value2) {
                        $__type_currency = TypeCurrency::where(['id' => $value2->type_currency_id, 'status' => '1'])->first();
                        if ($__type_currency) {
                            $__data[$__type_currency->id] = array(
                                'id' => $__type_currency->id,
                                'name' => $__type_currency->name,
                            );
                        }
                    }
                }
            }
            $__tmpdata = [];
            foreach ($__data as $key => $value) {
                $__tmpdata[] = array(
                    'id' => $value['id'],
                    'name' => $value['name'],
                );
            }
            $response["data"] = $__tmpdata;
        }

        return response()->json($response);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBank(Request $request)
    {
        $__user = Auth::user();
        $response = [];

        $validator = $request->validate([
            'pais' => 'required',
            'type_money' => 'required'
        ]);

        if (!$validator) {
            $response["error"] = true;
        } else {
            $response["error"] = false;

            $__data_type = [];

            $__data = [];
            $__bank = Bank::where(['country_id' => $validator['pais']])->get();
            foreach ($__bank as $key => $value) {
                if ($value->bankdetails()->count()) {
                    foreach ($value->bankdetails()->where(['type_currency_id' => $validator['type_money']])->get() as $key2 => $value2) {
                        $__type_bank_account = TypeBankAccount::where([ 'id' => $value2->type_account_id ])->first();
                        if ($__type_bank_account) {
                            $__data_type[$__type_bank_account->id] = array(
                                'id' => $__type_bank_account->id,
                                'name' => $__type_bank_account->name,
                            );
                        }
                        $__data[$value->id] = array(
                            'id' => $value->id,
                            'name' => $value->name,
                        );
                    }
                }
            }

            $__tmpdata = [];
            foreach ($__data as $key => $value) {
                $__tmpdata[] = array(
                    'id' => $value['id'],
                    'name' => $value['name'],
                );
            }
            $__tmpdata2 = [];
            foreach ($__data_type as $key => $value) {
                $__tmpdata2[] = array(
                    'id' => $value['id'],
                    'name' => $value['name'],
                );
            }
            $response["banks"] = $__tmpdata;
            $response["bank_types"] = $__tmpdata2;
        }


        return response()->json($response);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $__user = Auth::user();

        $__transaction = Transaction::where('id', $id)->first();
        if (!$__transaction) {

        }

        $__bank_account = $__transaction->bankAccount();

        $__bank_details = BanksDetails::where(["bank_id" => $__bank_account->bank_id, "type_account_id" => $__bank_account->type_account_id, "type_currency_id" => $__bank_account->type_currency_id])->first();

        return view('frontend.intercambio.view')->with([
            'id_intercambio' => $id,
            'intercambio' => $__transaction,
            'bank' => $__bank_account->bank(),
            'bank_details' => $__bank_details
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $__user = Auth::user();
        $__transaction = Transaction::where('id', $id)->first();
        $__transaction->images = json_encode($request->input('images'));
        $__transaction->status = 1;
        if ( !$__transaction->save() ) {

        } else {
            if ($__transaction->status == 1)
                event(new IntercambioConfirmation($__user, $__transaction));
        }


        return redirect()->route('dashboard.activity');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function images(Request $request, $id)
    {
        $__user = Auth::user();
        if($request->hasFile('file')) {
            $image = $request->file('file');
            $imageName = rand() . '_'  .  uniqid() . '.' . trim($image->getClientOriginalExtension());
            $image->move(public_path('transferencias'), $imageName);
            $attach = new AttachFile();
            $attach->user_id = $__user->id;
            $attach->file = $imageName;
            $attach->save();
            return response()->json(['success' => $imageName, 'id' => $attach->id], 200);
        }
        return response()->json(['success' => false], 300);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
