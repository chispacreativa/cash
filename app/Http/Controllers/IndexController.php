<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Mail;
use App\Models\MoneyRateHistory;
use App\Models\MoneyConfig;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $__money_config = MoneyConfig::where(['status' => 1])->get();
        $__money_rate = MoneyRateHistory::latest('created_at')->first();
        $__tmp_money = array(
            "USD" => 1,
            "PEN" => $__money_rate->pen,
            "BTC" => $__money_rate->btc,
            "COP" => $__money_rate->cop,
            "VES" => $__money_rate->ves
        );
        foreach ($__money_config as $key => $value) {
            if ($value->code != 'USD') {
                $__tmp_money[$value->code] = $__tmp_money[$value->code] * (  $value->fee / 100 + 1 );
            }
        }

        $__money_rate_json = json_encode($__tmp_money);

        return view('frontend.index')->with([
            'money_rate_json' => $__money_rate_json
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
