<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\NuevoIntercambio;

/* Usuario */
use App\Model\Usuario;
use App\Models\Transaction;
use App\Models\BankAccount;
use App\Models\Country;
use App\Models\Bank;
use App\Models\TypeCurrency;
use App\Models\TypeBankAccount;
use App\Model\UsuarioDetalle;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $__user = Auth::user();
        $__id = Auth::user()->id;
        $__user_detail = Usuario::find($__id);

        $__data = [];

        $__intercambio = Transaction::where([ 
                ['user_id', '=', $__id],
                ['status', '>', '0']
            ])
            ->get();

        foreach ($__intercambio as $key => $value) {
            if (@$__data[$value->type_exchange] == null) $__data[$value->type_exchange] = 0;
            $__data[$value->type_exchange] = $__data[$value->type_exchange] + $value->receive_amount;
        }
        
        return view('frontend.dashboard')->with([
            'intercambio' => $__data
        ]);
    }

    public function perfil() {
        $__user = Auth::user();

        /*return view('frontend.perfil')->with([    
        ]);*/
        return view('frontend.dashboard.perfil')->with([    
        ]);
    }

    public function editprofile(Request $request) {

        /**/
        
        $id = Auth::user()->id;
        $user = Usuario::find($id);
        $details = UsuarioDetalle::where("id_user", $user->id)->first();
        if ($request->isMethod('post')) {

            $this->validate($request, [
                "name" => "required",
                "lastname" => "required",
                "img_avatar" => "required | mimes:jpeg,jpg,png | max:1000",
            ]);
            
            $user->name = $request->input("name");
            $user->lastname = $request->input("lastname");
            
            
            if($request->input("password") != null) {
                $user->password = bcrypt($request->input("password"));
            }

            $details->phone = $request->input("phone");
            $details->type_document = $request->input("type_document");
            $details->number_document = $request->input("number_document");


            if($request->hasFile("img_avatar")) {
                $image = $request->file('img_avatar');
                $imageName = rand() . '_'  .  uniqid() . '.' . trim($image->getClientOriginalExtension());
                $image->move(public_path('img'), $imageName);
                $details->image = $imageName;
            }
            $details->save();
            $user->save();
            return redirect()->back()->with('success', __('¡su perfil se actualizo correctamente!'));
        }
       
        

        return view('frontend.dashboard.editprofile')->with([
            'data' => $user,
            'detalle' => $details
        ]);
    }

    public function activity() {
        $__user = Auth::user();
        $__transaction = Transaction::where(['user_id' => $__user->id])->orderBy('id', 'DESC')->get();
        return view('frontend.dashboard.activity')->with([
            'transactions' => $__transaction
        ]);
    }

    public function cuentas() {

        $__bank_accounts = BankAccount::where(['user_id' => Auth::user()->id, 'type_account_tran' => '1'])->get();

        $__bank_accounts_send = BankAccount::where(['user_id' => Auth::user()->id, 'type_account_tran' => '2'])->get();

        return view('frontend.dashboard.cuentas')->with([
            'bank_accounts' => $__bank_accounts,
            'bank_accounts_send' => $__bank_accounts_send
        ]);
    }

    public function cuentasEdit(Request $request, $id) {
        $__user = Auth::user();

        $__bank_account = BankAccount::where(['id' => $id ,'user_id' => Auth::user()->id])->first();
        $__country = Country::where(['status' => 1])->get();

        $__bank = Bank::where(['country_id' => $__bank_account['country_id']])->get();
            foreach ($__bank as $key => $value) {
                if ($value->bankdetails()->count()) {
                    foreach ($value->bankdetails()->get() as $key2 => $value2) {
                        $__type_currency = TypeCurrency::where(['id' => $value2->type_currency_id, 'status' => '1'])->first();
                        if ($__type_currency) {
                            $__data[$__type_currency->id] = array(
                                'id' => $__type_currency->id,
                                'name' => $__type_currency->name,
                            );
                        }
                    }
                }
            }


            foreach ($__bank as $key => $value) {
                if ($value->bankdetails()->count()) {
                    foreach ($value->bankdetails()->where(['type_currency_id' => $__bank_account['type_currency_id']])->get() as $key2 => $value2) {
                        $__type_bank_account = TypeBankAccount::where([ 'id' => $value2->type_account_id ])->first();
                        if ($__type_bank_account) {
                            $__data_type[$__type_bank_account->id] = array(
                                'id' => $__type_bank_account->id,
                                'name' => $__type_bank_account->name,
                            );
                        }
                        $__data_bank[$value->id] = array(
                            'id' => $value->id,
                            'name' => $value->name,
                        );
                    }
                }
            }
        

        if ($request->isMethod('post')) {
            if ($request->input('action') == "update") {
                if ($request->input('nombre_cuenta') != "" && $request->input('apellido_cuenta') != "") {
                    $__bank_account->name = $request->input('nombre_cuenta');
                    $__bank_account->lastname = $request->input('apellido_cuenta');
                    $__bank_account->tipo_documento = $request->input('tipo_documento');
                    $__bank_account->cedula = $request->input('cedula');
                    $__bank_account->email = $request->input('email');
                    $__bank_account->casa = $request->input('casa');
                    $__bank_account->bank_id = $request->input('entidad_bancaria');
                    $__bank_account->type_currency_id = $request->input('tipo_moneda');
                    $__bank_account->type_account_id = $request->input('tipo_cuenta');
                    $__bank_account->country_id = $request->input('pais_cuenta');
                    $__bank_account->value = $request->input('numero_cuenta');
                    $__bank_account->save();
                    return redirect()->route('dashboard.cuentas');
                }
            } else if ($request->input('action') == "delete") { 
                $__bank_account->delete();
                return redirect()->route('dashboard.cuentas');
            }
        }  

        return view('frontend.dashboard.cuentasedit')->with([
            'bank_account' => $__bank_account,
            'country' => $__country,
            'currencies' => $__data,
            'banks' => $__data_bank,
            'bank_types' => $__data_type
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function submit()
    {
        return view('frontend.submit');
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
