<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionCreateRequest extends FormRequest
{

    //protected $redirectAction = 'IntercambioController@view';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages() {
        return [
            'cambio01.required' => 'El monto es obligatorio',
            'cambio02.required' => 'El monto es obligatorio',
            'select-envio.required' => 'El tipo es obligatorio',
            'select-recibo.required' => 'El tipo es obligatorio',
            'cuenta-bank.required' => 'La cuenta es obligatorio',
            'cuenta-bank-send.required' => 'La cuenta es obligatorio'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cambio01' => 'required',
            'cambio02' => 'required',
            'select-envio' => 'required',
            'select-recibo' => 'required',
            'cuenta-bank' => 'required',
            'cuenta-bank-send' => 'required'
        ];
    }
}
