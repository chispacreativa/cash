<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\NuevoIntercambio;
use App\Models\Transaction;

class SendNuevoIntercambio implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /*Mail::to($this->email)
            ->send(new NuevoIntercambio($this->transaction));*/

        $__transacion = Transaction::where('id', $this->id)->first();
        Mail::to($this->email)
            ->queue(new NuevoIntercambio($__transacion));
    }
}
