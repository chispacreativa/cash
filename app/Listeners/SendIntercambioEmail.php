<?php

namespace App\Listeners;

use App\Events\IntercambioAdd;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Mail\NuevoIntercambio;

class SendIntercambioEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IntercambioAdd  $event
     * @return void
     */
    public function handle(IntercambioAdd $event)
    {
        \Mail::to($event->user->email)
              ->queue(new NuevoIntercambio($event->transaction));
    }
}
