<?php

namespace App\Listeners;

use App\Events\IntercambioValidation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Mail\ValidateIntercambio;
use App\Mail\ValidateIntercambioBeneficiario;
use App\Events\IntercambioValidationBeneficiario;

class SendIntercambioValidationBeneficiario
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IntercambioValidation  $event
     * @return void
     */
    public function handle(IntercambioValidationBeneficiario $event)
    {
        \Mail::to($event->email)
              ->queue(new ValidateIntercambioBeneficiario($event->transaction));
    }
}
