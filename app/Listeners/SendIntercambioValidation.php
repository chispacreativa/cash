<?php

namespace App\Listeners;

use App\Events\IntercambioValidation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Mail\ValidateIntercambio;

class SendIntercambioValidation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IntercambioValidation  $event
     * @return void
     */
    public function handle(IntercambioValidation $event)
    {
        \Mail::to($event->email)
              ->queue(new ValidateIntercambio($event->transaction));
    }
}
