<?php

namespace App\Listeners;

use App\Events\IntercambioConfirmation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Mail\ConfirmationIntercambio;

class SendIntercambioConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IntercambioConfirmation  $event
     * @return void
     */
    public function handle(IntercambioConfirmation $event)
    {
        //
        \Mail::to($event->user->email)
            ->queue(new ConfirmationIntercambio($event->transaction));
    }
}
