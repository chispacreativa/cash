<?php

namespace App\Listeners;

use App\Events\IntercambioTransferido;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendIntercambioTransferido
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IntercambioTransferido  $event
     * @return void
     */
    public function handle(IntercambioTransferido $event)
    {
        //
    }
}
