<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\IntercambioAdd;
use App\Events\IntercambioValidation;
use App\Events\IntercambioConfirmation;
use App\Listeners\SendIntercambioEmail;
use App\Listeners\SendIntercambioValidation;
use App\Listeners\SendIntercambioConfirmation;
use App\Events\SendPromotion;
use App\Listeners\SendPromotionUsers;
use App\Events\IntercambioValidationBeneficiario;
use App\Listeners\SendIntercambioValidationBeneficiario;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        IntercambioAdd::class => [
            SendIntercambioEmail::class
        ],
        IntercambioValidation::class => [
            SendIntercambioValidation::class
        ],
        IntercambioValidationBeneficiario::class => [
            SendIntercambioValidationBeneficiario::class
        ],
        IntercambioConfirmation::class => [
            SendIntercambioConfirmation::class
        ],

        SendPromotion::class => [
            SendPromotionUsers::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //dfd
    }
}
